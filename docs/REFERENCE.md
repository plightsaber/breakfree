# BreakFree Reference Guide
The following is a breakdown of the technical aspects of BreakFree. Users may find this interesting, too, but it's written more for me than anyone else.

## User Settings
- RP Mode - If enabled, the escape game is disabled and all users have full control over the restraints.

## Tetherable attachments
Only certain attachments are tetherable.  If there is an update to the tether script the following attachments need to be updated:
- arm_cuff_backChain
- arm_cuff_frontChain
- arm_rope_back_wrist
- arm_rope_backTight_wrist
- arm_rope_box_harness
- arm_rope_front_wrist
- arm_rope_hammer
- arm_rope_sides
- arm_tape_back_wrist
- arm_tape_backTight_wrist
- arm_tape_front_wrist
- arm_zip_backTight_wrist
- arm_zip_front_wrist
- leg_rope_ankle

## Restraint Properties
### Slots
#### Arm
- wrist
- elbow
- torso
#### Leg
- ankle
- knee
- immobilizer
- crotch
#### Gag
- gag1
- gag2
- gag3
- gag4
#### Blindfold
- blindfold
#### Hand
- hand

### JSON structure
#### User
```json
{
  "key": STRING,
  "name": STRING,
  "gender": STRING,
  "feats": LIST,
  "armBound": INT,
  "handBound": INT,
  "blade": INT
}
```
#### Pose
```json
{
  "name": STRING,
  "idle": STRING,
  "struggle_success": STRING,
  "struggle_failure": STRING,
  "walk": STRING
}
```
#### Restraints
```json
{
  "name": STRING,
  "type": STRING,
  "canCrop": BOOL,
  "canCut": BOOL,
  "canEscape": BOOL,
  "canPick": BOOL,
  "canTether": BOOL,
  "canUseItem": BOOL,
  "attachments": LIST,
  "poses": LIST,
  "complexity": INT,
  "integrity": INT,
  "tightness": INT
}
```
### Gag Properties
- mouthOpen - should an open mouth animation play.
- garble
  - garbled - Garble parameter simulating a full mouth
  - sealed - Garble parameter simulating a sealed mouth
  - muffled - Garble parameter simulating a muffled mouth
