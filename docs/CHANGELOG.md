# Changelog
## Unreleased

## [5.2.0] - 2024-09-26
### Added
- New "server" script to make it easier for external scripts to interface with basic restraints.

## [5.1.2] - 2022-10-16
### Fixed
- Tape mittens gain missing Anubis bonuses

### Changed
- Anubis++ now adds an additional +2 complexity to tape mittens
- Enabled RLV by default
- Enabled feat skipping by default
- Sadist now adds an additional +2 complexity to crotch ties
- Decreased Crotch tie to 1
- Increased Hammer complexity to 4
- Updated security calculations to be more consistent while updating struggle progress

## [5.1.1] - 2022-10-13
### Fixed
- BF-19: Mittens should trigger detach and touchfar RLV locks

## [5.1.0] - 2022-10-01
### Changed
- Allow mittened hands to use the "Pick" action on self
- Separate Mittens from Arm restraints

### Fixed
- BF-16: Fix for thrashing unexpected progress unexpectedly not progressing
- Register blindfold as an actual restraint

## [5.0.4] - 2022-07-21
### Fixed
- BF-11: Fix escape tools not broadcasting proper types
- BF-12: Fix tether particles and movement restrictions not staying in sync with tether status
- BF-13: Fix Athletic+ reverting to base chance for unexpected progress

## [5.0.3] - 2022-06-22
### Fixed
- BF-10: Fix experience not being awarded for struggle progress

## [5.0.2] - 2022-06-15
### Changed
- Simplify API by replacing set_owner_feats with existing set_owner call

### Fixed
- BF-5: Fix recovery timers not recovering stamina
- BF-9: Fix tether chain not descriminating targets

## [5.0.1] - 2022-06-06
### Changed
- BF-8: Change default color of ballgag strap to black

### Fixed
- BF-6: Fix no RLV commands working other than the blindfold
- BF-7: Fix various major gag garbling issues

## [5.0.0] - 2022-06-03
### Added
- New restraint type: Blindfolds
- New rope position: Hammer
- Options to enable/disable each restraint category
- Option to skip Feats requirements, unlocking all positions for villains without enabling RP Mode
- Rest escape option to force stamina recovery

### Changed
- Refactor from LSLForge to fs_preprocessor
- Updated restraint attachment names to match folders

### Fixed
- BF-3: Heroes no longer require victims to have energy to remove restraints
- Multipage menus no longer have empty page
- RealRestraint integration allows touch controller to be disabled so you don't get duplicate menus when wearing other BreakFree items.

## [4.1.3]
### Added
- New feat: Steadfast - recover stamina while you struggle
- Current version is displayed in the owner dialog
- Tie the rainbow!  More color options for all restraints

### Fixed
- Do not recover stamina while you struggle (unless you take the new feat: Steadfast)
- Fix issue where you do not recover stamina after logging in
- Fix issue where dialog control never times out

## [4.1.2]
### Added
- Self-bondage SECURE button to remove bind options when performing self-bondage

### Changed
- New rope mesh for arm back.  Old version added to alternates package
- Swapped knee ropes with their alternates

### Fixed
- Arm tether no longer blocked by leg tether
- Feats now properly apply when being used on another user
- Smooth bound animation transitions when fighting with avi AOs
- Zips and cuffs back pose use incorrect harness

## [4.1.1]
### Fixed
- Fixed bug that made all restraints inescapable o_o

## [4.1.0]
### Added
- Alternate animations for avis with narrow shoulders (Kemono)
- Handcuffs with unique lockable escape path
- Hero dialog now checks feats to give rescue suggestions
- LockGuard and LockMeister scripts added to rope wrist and leg restraints to better furniture interaction
- Owner option to disable lock pick requirement for cuffs
- Pick (Lock pick) and Cropper (bolt cutters) escape item scripts added
- Zip ties for arms and legs.

### Changed
- Action verbs for cutting changed to unique values
- Restraint folders renamed for ease of dynamic identification in code
- Performance improvements for deploying restraint changes
- "Untie" verb changed to "Release" to make more sense with non-rope restraints

### Fixed
- Adding or changing restraints no longer resets current pose if set to a PoseBall pose
- Animations will self-refresh after being interrupted by sitting/standing from PoseBall
- "Tightness" relabeled "Security" in Hero dialog to match Escape dialog terminology
- Escape-item menu entry now defaults to Escape menu when using on self
- Fix garbler sometimes treating avi as gagged after logging back in even if not gagged
- Fix pbBed01 animation not triggering
- Prevent cleave gagging avis who are ball gagged
- Security display now resets when rebound after a successful escape
- Touchers now use feats when using the dialogs of other avis
