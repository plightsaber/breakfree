#include "includes/general_tools.lsl"

integer CHANNEL_ATTACHMENT = -9999277;
string RESTRAINT_ATTACHMENT = "";
string RESTRAINT_COMPONENT = "";
string RESTRAINT_NAME = "";

vector _color = <255.0, 255.0, 255.0>;
string _texture = TEXTURE_BLANK;
key _targetKey;

key _configQueryKey;
integer _listener;

init() {
  _configQueryKey = llGetNotecardLine(".config",0);	// Load config.

  if (_listener) { llListenRemove(_listener); }
  _listener = llListen(CHANNEL_ATTACHMENT, "", NULL_KEY, "");

  llParticleSystem([]);
}

integer is_call_target(string json) {
  string attachment = llJsonGetValue(json, ["attachment"]);
  string component = llJsonGetValue(json, ["component"]);
  string userKey = llJsonGetValue(json, ["userKey"]);

  if (RESTRAINT_ATTACHMENT != attachment) { return FALSE; }
  if (RESTRAINT_COMPONENT != component && component != "ANY") { return FALSE; }
  if (llGetOwner() != userKey) { return FALSE; }

  return TRUE;
}

render_particles() {
  if (!is_set(_targetKey)) {
    llParticleSystem([]);
    return;
  }

  list tetherEffect = [
    PSYS_PART_FLAGS, PSYS_PART_RIBBON_MASK | PSYS_PART_TARGET_POS_MASK | PSYS_PART_TARGET_LINEAR_MASK,
      PSYS_SRC_PATTERN, PSYS_SRC_PATTERN_DROP,

      PSYS_SRC_TARGET_KEY, _targetKey,
      PSYS_SRC_TEXTURE, _texture,
      PSYS_PART_START_COLOR, _color,
      PSYS_PART_END_COLOR, _color,

      PSYS_PART_START_SCALE, <0.05,1.0,1.0>,
      PSYS_PART_END_SCALE, <0.05,1.0,1.0>,

      PSYS_PART_MAX_AGE, 1,
      PSYS_SRC_BURST_RATE, 0.01,
      PSYS_SRC_BURST_PART_COUNT, 1
  ];

  llParticleSystem(tetherEffect);
}

execute_function(string prmFunction, string params) {
  if (!is_call_target(params)) {
    return;
  }

  if ("tether_to" == prmFunction) {
    _targetKey = llJsonGetValue(params, ["targetID"]);
    render_particles();
  } else if ("set_color" == prmFunction) {
    _color = (vector)llJsonGetValue(params, ["color"]);
    render_particles();
  } else if ("set_texture" == prmFunction ) {
    _texture = llJsonGetValue(params, ["texture"]);
    if (!is_set(_texture) || "blank" == _texture) {
      _texture = TEXTURE_BLANK;
    }
    render_particles();
  }
}

default
{
  on_rez(integer prmStart) {
    init();
  }

  state_entry() {
    init();
  }

  dataserver(key queryID, string configData) {
    if (queryID == _configQueryKey) {
      RESTRAINT_ATTACHMENT = llJsonGetValue(configData, ["attachment"]);
      RESTRAINT_COMPONENT = llJsonGetValue(configData, ["component"]);
      RESTRAINT_NAME = llJsonGetValue(configData, ["type"]);

      string request = "";
      request = llJsonSetValue(request, ["attachment"], RESTRAINT_ATTACHMENT);
      request = llJsonSetValue(request, ["component"], RESTRAINT_COMPONENT);
      request = llJsonSetValue(request, ["name"], RESTRAINT_NAME);
      api_request(llGetOwner(), llGetOwner(), "request_tether", request);
    }
  }

  listen(integer prmChannel, string prmName, key prmID, string prmText) {
    if (prmChannel == CHANNEL_ATTACHMENT) {
      string function = llJsonGetValue(prmText, ["function"]);
      string value = llJsonGetValue(prmText, ["value"]);

      if (!is_set(function)) {
        debug(prmText);
        return;
      }

      execute_function(function, value);
      value = llJsonGetValue(prmText, ["value"]);
    }
  }
}
