#include "includes/general_tools.lsl"

integer CHANNEL_ATTACHMENT = -9999277;

list FACE_NORMALS;
integer FACE_TARGET = ALL_SIDES;
integer LINK_TARGET = LINK_SET;
string RESTRAINT_ATTACHMENT = "";
string RESTRAINT_COMPONENT = "";
string RESTRAINT_NAME = "";

vector _color = <0.0, 0.0, 0.0>;
string _texture = TEXTURE_BLANK;

key _configQueryKey;
integer _listener;

init() {
  _configQueryKey = llGetNotecardLine(".config",0); // Load config.

  if (_listener) { llListenRemove(_listener); }
  _listener = llListen(CHANNEL_ATTACHMENT, "", NULL_KEY, "");
}

set_color(string prmInfo) {
  if (validate_target(prmInfo) == FALSE) { return; }
  _color = (vector)llJsonGetValue(prmInfo, ["color"]);

  llSetLinkColor(LINK_TARGET, _color, FACE_TARGET); // LINK_SET to _color all, LINK_THIS to _color prim script is located in.
  simple_request("set_color", (string)_color);
}

set_texture(string prmInfo) {
 if (validate_target(prmInfo) == FALSE) { return; }
    _texture = llJsonGetValue(prmInfo, ["texture"]);
    if (llGetInventoryType(_texture) == 0 || _texture == TEXTURE_BLANK) {
      llSetLinkTexture(LINK_TARGET, _texture, FACE_TARGET);
    }

    // Update normals if available
    string normal = TEXTURE_BLANK;
    if (_texture != TEXTURE_BLANK) { normal = _texture+"Normal"; }

    if (normal == TEXTURE_BLANK || llGetInventoryType(normal) == 0) {
        integer faceIndex = 0;
        list primitiveParams = llGetPrimitiveParams([PRIM_NORMAL, faceIndex]);
        while (llGetListLength(primitiveParams) > 0) {
          if (llGetListLength(FACE_NORMALS) == 0 || llListFindList(FACE_NORMALS, [faceIndex]) != -1) {
            llSetLinkPrimitiveParamsFast(LINK_TARGET, [PRIM_NORMAL, faceIndex, normal, llList2Vector(primitiveParams, 1), llList2Vector(primitiveParams, 2), llList2Float(primitiveParams, 3)]);
          }
          faceIndex ++;
          primitiveParams = llGetPrimitiveParams([PRIM_NORMAL, faceIndex]);
        }
    }

    simple_request("set_texture", _texture);
}

integer validate_target(string prmInfo) {
  if (llJsonGetValue(prmInfo, ["attachment"]) != RESTRAINT_ATTACHMENT) { return FALSE; }
  if (llJsonGetValue(prmInfo, ["component"]) != RESTRAINT_COMPONENT) { return FALSE; }
  if (llJsonGetValue(prmInfo, ["userKey"]) != (string)llGetOwner()) { return FALSE; }
  return TRUE;
}

default
{
  on_rez(integer prmStart) {
    init();
  }

  state_entry() {
    init();
  }

  dataserver(key queryID, string configData) {
    if (queryID == _configQueryKey) {
      RESTRAINT_ATTACHMENT = llJsonGetValue(configData, ["attachment"]);
      RESTRAINT_COMPONENT = llJsonGetValue(configData, ["component"]);
      RESTRAINT_NAME = llJsonGetValue(configData, ["name"]);

      // Allow configuration to override material targets
      if (JSON_INVALID != llJsonGetValue(configData, ["faceTarget"])) {
        FACE_TARGET = (integer)llJsonGetValue(configData, ["faceTarget"]);
      }

      if (JSON_INVALID != llJsonGetValue(configData, ["linkTarget"])) {
        LINK_TARGET = (integer)llJsonGetValue(configData, ["linkTarget"]);
      }

      if (is_set(llJsonGetValue(configData, ["faceNormals"]))) {
        FACE_NORMALS = llJson2List(llJsonGetValue(configData, ["faceNormals"]));
      }

      string request = "";
      request = llJsonSetValue(request, ["attachment"], RESTRAINT_ATTACHMENT);
      request = llJsonSetValue(request, ["component"], RESTRAINT_COMPONENT);
      request = llJsonSetValue(request, ["name"], RESTRAINT_NAME);
      api_request(llGetOwner(), llGetOwner(), "request_style", request);
    }
  }

  listen(integer prmChannel, string prmName, key prmID, string prmText) {
    if (prmChannel == CHANNEL_ATTACHMENT) {
      string function;
      string value;

      if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
        debug(prmText);
        return;
      }
      value = llJsonGetValue(prmText, ["value"]);

      if (function == "set_color") {
        set_color(value);
      } else if (function == "set_texture") {
        set_texture(value);
      }
    }
  }
}
