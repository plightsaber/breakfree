#include "includes/general_tools.lsl"
#include "includes/gui_tools.lsl"
#include "includes/pose_lib.lsl"
#include "includes/restraint_tools.lsl"
#include "includes/user_lib.lsl"

string _self; // JSON object
string _villain;

// Status
integer _armsBound = FALSE;
integer _legsBound = FALSE;
integer _gagBound = FALSE;

// Tether variables
integer _armsTetherable;
integer _legsTetherable;

list _legPoses;

// GUI screens
integer GUI_HOME = 0;
integer GUI_ARM = 10;
integer GUI_LEG = 20;
integer GUI_GAG = 30;
integer GUI_BLINDFOLD = 40;
integer GUI_POSE = 70;

// Restaint lists
list _armRestraints;
list _legRestraints;
list _gagRestraints;
list _blindfoldRestraints;

// Other
integer _allowArm = TRUE;
integer _allowLeg = TRUE;
integer _allowGag = TRUE;
integer _allowMitten = TRUE;
integer _allowBlindfold = TRUE;
integer _armBoundExternal = FALSE;
integer _featSkip = FALSE;
integer _rpMode = FALSE;
key _configQueryID;
string _JsonSettings;

string _resumeFunction;

init() {
  _armRestraints = ["Unbound"];
  _legRestraints = ["Unbound"];
  _gagRestraints = ["Unbound"];
  _blindfoldRestraints = ["Unbound"];

  _configQueryID = llGetNotecardLine(".config",0); // Load config.

  simple_request("get_available_restraints", "all");
}

init_gui(key prmID, integer prmScreen) {
  _guiUserKey = prmID;

  if (_guiListener) { llListenRemove(_guiListener); }
  _guiChannel = (integer)llFrand(-9998) - 1;
  _guiListener = llListen(_guiChannel, "", _guiUserKey, "");
  gui(prmScreen);
}

gui(integer prmScreen) {
  // Reset Busy Clock
  llSetTimerEvent(GUI_TIMEOUT);

  string btn10 = " "; string btn11 = " ";   string btn12 = " ";
  string btn7 = " "; string btn8 = " ";   string btn9 = " ";
  string btn4 = " "; string btn5 = " ";   string btn6 = " ";
  string btn1 = " "; string btn2 = "<<Done>>"; string btn3 = " ";

  if (_guiUserKey == llGetOwner()) { btn1 = "<<Back>>"; } // Allow users to bind selves and return to own menu

  list mpButtons;

  _guiText = " ";


  if (prmScreen != _guiScreen) { _guiScreenLast = _guiScreen; }
  if (btn1 == " " && (prmScreen != 0)) { btn1 = "<<Back>>"; };

  // GUI: Main
  if (prmScreen == GUI_HOME) {
    // Reset previous screen
    _guiScreenLast = GUI_HOME;

    list homeButtons;

    if (_legsBound) { btn3 = "Pose"; }

    if (can_bind_arms()) { btn4 = "Bind Arms"; }
    if (can_bind_legs()) { btn5 = "Bind Legs"; }
    if (can_gag()) { btn6 = "Gag"; }
    if (can_mitten()) { btn7 = "Mitten"; }
    if (can_blindfold()) { btn9 = "Blindfold"; }

    if (_armsTetherable) { btn10 = "Tether Arms"; }
    if (_legsTetherable) { btn11 = "Tether Legs"; }
    if (is_bound() && llJsonGetValue(_villain, ["key"]) == llGetOwner()) { btn12 = "Secure"; }
  }

  // GUI: Bind Arms
  if (prmScreen == GUI_ARM) {
    _guiText = "What do you want to bind " + get_name() + "'s arms with?";
    mpButtons = multipage_gui(_armRestraints, 3, _guiPage);
  }

  // GUI: Bind Legs
  if (prmScreen == GUI_LEG) {
    _guiText = "What do you want to bind " + get_name() + "'s legs with?";
    mpButtons = multipage_gui(_legRestraints, 3, _guiPage);
  }

  // GUI: Bind Gag
  if (prmScreen == GUI_GAG) {
    _guiText = "What do you want to gag " + get_name() + " with?";
    mpButtons = multipage_gui(_gagRestraints, 3, _guiPage);
  }

  if (prmScreen == GUI_BLINDFOLD) {
    _guiText = "What do you want to blindfold " + get_name() + "with?";
    mpButtons = multipage_gui(_blindfoldRestraints, 3, _guiPage);
  }

  // GUI: Pose
  if (prmScreen == GUI_POSE) {
    _guiText = "How do you want to pose " + get_name() + "?";
    mpButtons = multipage_gui(_legPoses, 3, _guiPage);
  }

  _guiScreen = prmScreen;
  _guiButtons = [btn1, btn2, btn3];

  if (btn4+btn5+btn6 != "   ") { _guiButtons += [btn4, btn5, btn6]; }
  if (btn7+btn8+btn9 != "   ") { _guiButtons += [btn7, btn8, btn9]; }
  if (btn10+btn11+btn12 != "   ") { _guiButtons += [btn10, btn11, btn12]; }

  // Load MP Buttons - hopefully the lengths were configured correctly!
  if (llGetListLength(mpButtons)) { _guiButtons += mpButtons; }

  llDialog(_guiUserKey, _guiText, _guiButtons, _guiChannel);
}

// ===== Main Functions =====
add_available_restraint(string prmInfo) {
  string tmpName = llJsonGetValue(prmInfo, ["name"]);
  string tmpPart = llJsonGetValue(prmInfo, ["part"]);
  integer hasColor = llJsonGetValue(prmInfo, ["hasColor"]) == "1";

  if (tmpPart == "arm") {
    _armRestraints += tmpName;
  } else if (tmpPart == "leg") {
    _legRestraints += tmpName;
  } else if (tmpPart == "gag") {
    _gagRestraints += tmpName;
  } else if (tmpPart == "blindfold") {
    _blindfoldRestraints += tmpName;
  }
}

integer can_bind_arms() {
  return !_armBoundExternal && _allowArm;
}

integer can_bind_legs() {
  return _allowLeg;
}

integer can_blindfold() {
  return _allowBlindfold;
}

integer can_mitten() {
  return _allowMitten && (has_feat(_villain, "Anubis++") || _rpMode || _featSkip);
}

integer can_gag() {
  return _allowGag;
}

set_available_poses(string prmPoses) {
  _legPoses = llJson2List(prmPoses);
  _legPoses += get_poseball_pose_list();
}

set_restraints(string prmJson) {
  _restraints = prmJson;
  _armsBound = (integer)llJsonGetValue(prmJson, ["armBound"]);
  _armsTetherable = (integer)llJsonGetValue(prmJson, ["armTetherable"]);
  _armBoundExternal = (integer)llJsonGetValue(prmJson, ["armBoundExternal"]);
  _legsBound = (integer)llJsonGetValue(prmJson, ["legBound"]);
  _legsTetherable = (integer)llJsonGetValue(prmJson, ["legTetherable"]);
}

// ===== Event Controls =====
execute_function(string prmFunction, string prmJson) {
  string value = llJsonGetValue(prmJson, ["value"]);
  if (JSON_INVALID == value) {
    //return;  // TODO: Rewrite all linked calls to send in JSON
  }
  if (prmFunction == "set_gender") { set_gender(value); }
  else if (prmFunction == "set_restraints") { set_restraints(value); }
  else if (prmFunction == "set_poses") { set_available_poses(llJsonGetValue(value, ["leg"])); }
  else if (prmFunction == "add_available_restraint") { add_available_restraint(value); }
  else if (prmFunction == "set_villain") { _villain = value; }
  else if (prmFunction == "set_allowArm") { _allowArm = (integer)value; }
  else if (prmFunction == "set_allowLeg") { _allowLeg = (integer)value; }
  else if (prmFunction == "set_allowGag") { _allowGag = (integer)value; }
  else if (prmFunction == "set_allowMitten") { _allowMitten = (integer)value; }
  else if (prmFunction == "set_allowBlindfold") { _allowBlindfold = (integer)value; }
  else if (prmFunction == "set_featSkip") { _featSkip = (integer)value; }
  else if (prmFunction == "set_rpMode") { _rpMode = (integer)value; }
  else if (prmFunction == "gui_bind") {
    key userkey = (key)llJsonGetValue(prmJson, ["userkey"]);
    integer screen = GUI_HOME;
    if ((integer)llJsonGetValue(prmJson, ["restorescreen"]) && _guiScreen) { screen = _guiScreen;}
    init_gui(userkey, screen);
  } else if (prmFunction == "reset_gui") {
    exit("");
  }
}

default
{
  state_entry() {
    init();
  }

  on_rez(integer prmStart) {
    init();
  }

  dataserver(key queryID, string configData) {
    if (queryID == _configQueryID) {
      _JsonSettings = configData;
    }
  }

  listen(integer prmChannel, string prmName, key prmID, string prmText) {
    if (prmChannel = _guiChannel) {
      if (prmText == "<<Done>>") { exit("done"); return; }
      else if (prmText == " ") { gui(_guiScreen); }
      else if (_guiScreen != GUI_HOME && prmText == "<<Back>>") { gui(_guiScreenLast); return; }

      else if (prmText == "Next >>") { _guiPage ++; gui(_guiScreen); return; }
      else if (prmText == "<< Previous") { _guiPage --; gui(_guiScreen); return; }

      if (_guiScreen == GUI_HOME) {
        if (prmText == "Bind Arms") { gui(GUI_ARM); }
        else if (prmText == "Bind Legs") { gui(GUI_LEG); }
        else if (prmText == "Gag") { gui(GUI_GAG); }
        else if (prmText == "Mitten") {
          gui_request("gui_mitten", FALSE, _guiUserKey, 0);
          return;
        }
        else if (prmText == "Blindfold") { gui(GUI_BLINDFOLD); }
        else if (prmText == "Pose") { gui(GUI_POSE); }
        else if (prmText == "Secure") {
          _villain = NULL_KEY;
          simple_request("set_villain", NULL_KEY);
          exit("");
        }
        else if (prmText == "Tether Arms") { gui_request("gui_tether_arm", FALSE, _guiUserKey, 0); return; }
        else if (prmText == "Tether Legs") { gui_request("gui_tether_leg", FALSE, _guiUserKey, 0); return; }
        else if (prmText == "<<Back>>") {
          gui_request("gui_owner", FALSE, _guiUserKey, 0);
          return;
        }
      }
      else if (_guiScreen == GUI_ARM) {
        if (prmText == "Unbound") {
          simple_request("release_restraint", "arm");
          gui(_guiScreen);
          return;
        }
        gui_request("gui_arm_" + llToLower(prmText), FALSE, _guiUserKey, 0);
        return;
      } else if (_guiScreen == GUI_LEG) {
        if (prmText == "Unbound") {
          simple_request("release_restraint", "leg");
          gui(_guiScreen);
          return;
        }
        gui_request("gui_leg_" + llToLower(prmText), FALSE, _guiUserKey, 0);
        return;
      } else if (_guiScreen == GUI_GAG) {
        if (prmText == "Unbound") {
          simple_request("release_restraint", "gag");
          gui(_guiScreen);
          return;
        }
        gui_request("gui_gag_" + llToLower(prmText), FALSE, _guiUserKey, 0);
        return;
      } else if (_guiScreen == GUI_BLINDFOLD) {
        if (prmText == "Unbound") {
          simple_request("release_restraint", "blindfold");
          gui(_guiScreen);
          return;
        }
        gui_request("gui_blindfold_" + llToLower(prmText), FALSE, _guiUserKey, 0);
        return;
      } else if (_guiScreen == GUI_POSE) {
        simple_request("set_leg_pose", prmText);
        gui(_guiScreen);
        return;
      }
    }
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function = llJsonGetValue(prmText, ["function"]);
    if (JSON_INVALID == function) {
      debug(prmText);
      return;
    }

    execute_function(function, prmText);

    if (function == _resumeFunction) {
      _resumeFunction = "";
      init_gui(_guiUserKey, _guiScreen);
    }
  }

  timer() {
    exit("timeout");
  }

}
