#include "includes/arm_tools.lsl"
#include "includes/color_lib.lsl"
#include "includes/general_tools.lsl"
#include "includes/gui_tools.lsl"
#include "includes/restraint_tools.lsl"
#include "includes/texture_lib.lsl"

integer GUI_HOME = 0;
integer GUI_STYLE = 100;
integer GUI_COLOR = 100; // Only style option is color

init() {
  if (!is_set(_currentColors)) {
    set_color(COLOR_SILVER, "cuff");
  }
  if (!is_set(_currentTextures)) {
    set_texture(TEXTURE_BLANK, "cuff");
  }
}

init_gui(key prmID, integer prmScreen) {
  _guiUserKey = prmID;

  if (_guiListener) { llListenRemove(_guiListener); }
  _guiChannel = (integer)llFrand(-9998) - 1;
  _guiListener = llListen(_guiChannel, "", _guiUserKey, "");
  gui(prmScreen);
}

string get_self() {
  if (_self != "") return _self;

  _self = llJsonSetValue(_self, ["name"], "Cuff");
  _self = llJsonSetValue(_self, ["part"], "arm");
  _self = llJsonSetValue(_self, ["type"], "cuff");
  return _self;
}

string define_restraint(string name) {
  string restraint;

  // Type-specific values
  restraint = llJsonSetValue(restraint, ["name"], name);
  restraint = llJsonSetValue(restraint, ["canCrop"], "1");
  restraint = llJsonSetValue(restraint, ["canCut"], "0");
  restraint = llJsonSetValue(restraint, ["canPick"], "1");
  restraint = llJsonSetValue(restraint, ["canEscape"], "0");
  restraint = llJsonSetValue(restraint, ["canTether"], "0");
  restraint = llJsonSetValue(restraint, ["canUseItem"], "1");
  restraint = llJsonSetValue(restraint, ["type"], "cuff");

  integer complexity = 1;
  integer integrity = 15;
  integer tightness;

  if ("Front" == name) {
    tightness = 3;

    restraint = llJsonSetValue(restraint, ["uid"], "front_cuff");
    restraint = llJsonSetValue(restraint, ["slot"], "wrist");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, ["frontLoose"]));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, ["arm_cuff_wristRight", "arm_cuff_wristLeft", "arm_cuff_frontChain"]));
  } else if ("Back" == name) {
    tightness = 4;

    restraint = llJsonSetValue(restraint, ["uid"], "back_cuff");
    restraint = llJsonSetValue(restraint, ["slot"], "wrist");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, ["backLoose"]));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, ["arm_cuff_wristRight", "arm_cuff_wristLeft", "arm_cuff_backChain"]));
  }

  restraint = llJsonSetValue(restraint, ["complexity"], (string)complexity);
  restraint = llJsonSetValue(restraint, ["integrity"], (string)integrity);
  restraint = llJsonSetValue(restraint, ["tightness"], (string)tightness);

  return restraint;
}

gui(integer screen) {
  // Reset busy clock
  llSetTimerEvent(GUI_TIMEOUT);

  string btn10 = " "; string btn11 = " "; string btn12 = " ";
  string btn7 = " "; string btn8 = " "; string btn9 = " ";
  string btn4 = " "; string btn5 = " "; string btn6 = " ";
  string btn1 = "<<Back>>"; string btn2 = "<<Done>>"; string btn3 = " ";

  list mpButtons;
  _guiText = " ";

  // GUI: Main
  if (screen == GUI_HOME) {
     btn3 = "<<Style>>"; // Cuffs have no style at the moment

    if (is_set(llJsonGetValue(_restraints, ["slots", "wrist"]))
      || is_set(llJsonGetValue(_restraints, ["slots", "elbow"]))
      || is_set(llJsonGetValue(_restraints, ["slots", "torso"]))
    ) {
      mpButtons += "Release";
    }

    if (is_set(llJsonGetValue(_restraints, ["slots", "hand"]))) { mpButtons += "Free Hands"; }

    if (!is_set(llJsonGetValue(_restraints, ["slots", "wrist"])) && !is_set(llJsonGetValue(_restraints, ["slots", "torso"]))) {
      if (!is_set(llJsonGetValue(_restraints, ["slots", "elbow"]))) {
        mpButtons += "Front";
      }
      mpButtons += "Back";
    }

    mpButtons = multipage_gui(mpButtons, 2, _guiPage);
  }
  else if (screen == GUI_COLOR) {
    _guiText = "Choose a color for the arm cuffs.";
    mpButtons = multipage_gui(_colors, 3, _guiPage);
  }

  if (screen != _guiScreen) { _guiScreenLast = _guiScreen; }
  _guiScreen = screen;

  _guiButtons = [btn1, btn2, btn3];
  if (btn4+btn5+btn6 != "   ") { _guiButtons += [btn4, btn5, btn6]; }
  if (btn7+btn8+btn9 != "   ") { _guiButtons += [btn7, btn8, btn9]; }
  if (btn10+btn11+btn12 != "   ") { _guiButtons += [btn10, btn11, btn12]; }

  // Load MP Buttons - hopefully the lengths were configured correctly!
  if (llGetListLength(mpButtons)) { _guiButtons += mpButtons; }

  llDialog(_guiUserKey, _guiText, _guiButtons, _guiChannel);
}

execute_function(string prmFunction, string prmJson) {
  string value = llJsonGetValue(prmJson, ["value"]);

  if ("get_available_restraints" == prmFunction) {
    simple_request("add_available_restraint", get_self());
    return;
  }
  else if ("request_style" == prmFunction) {
    if (llJsonGetValue(value, ["attachment"]) != llJsonGetValue(get_self(), ["part"])) { return; }
    if (llJsonGetValue(value, ["name"]) != "cuff") { return; }
    string component = llJsonGetValue(value, ["component"]);
    if ("" == component) { component = "cuff"; }

    set_color((vector)llJsonGetValue(_currentColors, [component]), component);
    set_texture(llJsonGetValue(_currentTextures, [component]), component);
  }
  else if ("set_restraints" == prmFunction) {
    _restraints = value;
    set_restraints(value);
    return;
  }
  else if ("gui_arm_cuff" == prmFunction) {
    key userkey = (key)llJsonGetValue(prmJson, ["userkey"]);
    integer screen = 0;
    if ((integer)llJsonGetValue(prmJson, ["restorescreen"]) && _guiScreenLast) { screen = _guiScreenLast;}
    init_gui(userkey, screen);
  }
  else if ("reset_gui" == prmFunction) {
    exit("");
  }
  else if (prmFunction == "add_restraint_by_type") {
    if (llJsonGetValue(value, ["type"]) != llJsonGetValue(get_self(), ["type"])) {
      return; // Incorrect type
    }

    if (llJsonGetValue(value, ["part"]) != llJsonGetValue(get_self(), ["part"])) {
      return; // Incorrect type
    }

    string restraintSet;
    restraintSet = llJsonSetValue(restraintSet, ["type"], llJsonGetValue(get_self(), ["part"]));
    restraintSet = llJsonSetValue(restraintSet, ["restraint"], define_restraint(llJsonGetValue(value, ["restraint"])));
    simple_request("add_restraint", restraintSet);
    return;
  }
}

default
{
  state_entry() {
    init();
  }

  link_message(integer sender_num, integer num, string str, key id) {
    string function;
    string value;

    if ((function = llJsonGetValue(str, ["function"])) == JSON_INVALID) {
      debug(str);
      return;
    }

    execute_function(function, str);

    if (function == _resumeFunction) {
      _resumeFunction = "";
      init_gui(_guiUserKey, _guiScreen);
    }
  }

  listen(integer channel, string name, key id, string message) {
    if (channel = _guiChannel) {
      if (message == "<<Done>>") { exit("done"); return; }
      else if (message == " ") { gui(_guiScreen); return; }
      else if (message == "<<Back>>") {
        if (_guiScreen != GUI_HOME) { gui(_guiScreenLast); return;}
        gui_request("gui_bind", TRUE, _guiUserKey, 0);
        return;
      }
      else if (message == "Next >>") { _guiPage ++; gui(_guiScreen); return; }
      else if (message == "<< Previous") { _guiPage --; gui(_guiScreen); return; }

      if (message == "Release") {
        simple_request("remove_restraint", llJsonGetValue(get_self(), ["part"]));
        _resumeFunction = "set_restraints";
        return;
      } else if (message == "Free Hands") {
        simple_request("remove_slot", "hand");
        _resumeFunction = "set_restraints";
        return;
      }

      if (_guiScreen == GUI_HOME) {
        if (message == "<<Style>>") {
          gui(GUI_STYLE);
          return;
        } else {
          string restraintSet;
          restraintSet = llJsonSetValue(restraintSet, ["type"], llJsonGetValue(get_self(), ["part"]));
          restraintSet = llJsonSetValue(restraintSet, ["restraint"], define_restraint(message));
          simple_request("add_restraint", restraintSet);
          _resumeFunction = "set_restraints";
          return;
        }
      } else if (_guiScreen == GUI_COLOR) {
        set_color_by_name(message, "cuff");
        gui(_guiScreen);
        return;
      }
    }
  }

  timer() {
    exit("timeout");
  }
}
