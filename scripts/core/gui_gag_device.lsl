#include "includes/color_lib.lsl"
#include "includes/general_tools.lsl"
#include "includes/gag_tools.lsl"
#include "includes/gui_tools.lsl"
#include "includes/user_lib.lsl"

integer GUI_HOME = 0;
integer GUI_STYLE = 100;
integer GUI_COLOR_BALL = 101;
integer GUI_COLOR_STRAP = 102;

string get_self() {
  if (_self != "") return _self;

  _self = llJsonSetValue(_self, ["name"], "Device");
  _self = llJsonSetValue(_self, ["part"], "gag");
  _self = llJsonSetValue(_self, ["type"], "device");
  _self = llJsonSetValue(_self, ["hasColor"], "1");
  return _self;
}

init() {
  if (!is_set(_currentColors)) {
    set_color(COLOR_RED, "ball");
    set_color(COLOR_BLACK, "strap");
  }
}

init_gui(key prmID, integer prmScreen) {
  _guiUserKey = prmID;

  if (_guiListener) { llListenRemove(_guiListener); }
  _guiChannel = (integer)llFrand(-9998) - 1;
  _guiListener = llListen(_guiChannel, "", _guiUserKey, "");
  gui(prmScreen);
}

send_availability_info() {
  simple_request("add_available_restraint", get_self());
}

gui(integer prmScreen) {
  // Reset Busy Clock
  llSetTimerEvent(GUI_TIMEOUT);

  string btn10 = " ";   string btn11 = " ";   string btn12 = " ";
  string btn7 = " ";   string btn8 = " ";   string btn9 = " ";
  string btn4 = " ";   string btn5 = " ";   string btn6 = " ";
  string btn1 = "<<Back>>"; string btn2 = "<<Done>>"; string btn3 = " ";

  list mpButtons;
  _guiText = " ";

  // GUI: Main
  if (prmScreen == GUI_HOME) {
    get_current_restraints();
    btn3 = "<<Style>>";

    if (llJsonGetValue(_currentRestraints, ["gag1"]) != JSON_NULL
      || llJsonGetValue(_currentRestraints, ["gag2"]) != JSON_NULL
      || llJsonGetValue(_currentRestraints, ["gag3"]) != JSON_NULL
      || llJsonGetValue(_currentRestraints, ["gag4"]) != JSON_NULL
    ) {
      mpButtons += "Ungag";
    } else {
      mpButtons += "Ballgag";
    }

    mpButtons = multipage_gui(mpButtons, 2, _guiPage);
  }
  // GUI: Colorize
  else if (prmScreen == GUI_STYLE) {
    _guiText = "Choose what you want to style.";
    mpButtons = multipage_gui(["Strap", "Ball"], 2, _guiPage);
  }
  else if (prmScreen == GUI_COLOR_STRAP) {
    _guiText = "Choose a color for the straps.";
    mpButtons = multipage_gui(_colors, 3, _guiPage);
  }
  else if (prmScreen == GUI_COLOR_BALL) {
    _guiText = "Choose a color for the ball.";
    mpButtons = multipage_gui(_colors, 3, _guiPage);
  }
  /*
  else if (prmScreen == 111) {
    _guiText = "Choose a texture for the gag.";
    mpButtons = multipage_gui(textures, 3, _guiPage);
  }
  */

  if (prmScreen != _guiScreen) { _guiScreenLast = _guiScreen; }
  _guiScreen = prmScreen;

  _guiButtons = [btn1, btn2, btn3];
  if (btn4+btn5+btn6 != "   ") { _guiButtons += [btn4, btn5, btn6]; }
  if (btn7+btn8+btn9 != "   ") { _guiButtons += [btn7, btn8, btn9]; }
  if (btn10+btn11+btn12 != "   ") { _guiButtons += [btn10, btn11, btn12]; }

  // Load MP Buttons - hopefully the lengths were configured correctly!
  if (llGetListLength(mpButtons)) { _guiButtons += mpButtons; }

  llDialog(_guiUserKey, _guiText, _guiButtons, _guiChannel);
}

// ===== Main Functions =====
string define_restraint(string prmName) {
  string gag;

  gag = llJsonSetValue(gag, ["name"], prmName);
  gag = llJsonSetValue(gag, ["type"], llJsonGetValue(get_self(), ["type"]));

  if (prmName == "Ballgag") {
    gag = llJsonSetValue(gag, ["uid"], "ball_device");
    gag = llJsonSetValue(gag, ["speechGarbled"], "1");
    gag = llJsonSetValue(gag, ["slot"], "gag1");
    gag = llJsonSetValue(gag, ["canCut"], "0");
    gag = llJsonSetValue(gag, ["canEscape"], "0");
    gag = llJsonSetValue(gag, ["mouthOpen"], "1");
    gag = llJsonSetValue(gag, ["type"], "strap");
    gag = llJsonSetValue(gag, ["complexity"], "2");
    gag = llJsonSetValue(gag, ["integrity"], "10");
    gag = llJsonSetValue(gag, ["tightness"], "3");
    gag = llJsonSetValue(gag, ["attachments", JSON_APPEND], "gag_device_ball");
  }

  return gag;
}

// ===== Event Controls =====
execute_function(string prmFunction, string prmJson) {
  string value = llJsonGetValue(prmJson, ["value"]);
  if (JSON_INVALID == value) {
    //return;  // TODO: Rewrite all linked calls to send in JSON
  }


  if (prmFunction == "set_gender") { set_gender(value); }
  else if (prmFunction == "set_restraints") {
    _currentRestraints = llJsonGetValue(value, ["slots"]);
    _mouthOpen = llJsonGetValue(value, ["mouthOpen"]) == "1";
  }
  else if (prmFunction == "get_available_restraints") { send_availability_info(); }
  else if (prmFunction == "request_style") {
    if (llJsonGetValue(value, ["attachment"]) != "gag") { return; }
    if (llJsonGetValue(value, ["name"]) != "device") { return; }
    string component = llJsonGetValue(value, ["component"]);
    if ("" == component) { component = "ball"; }

    set_color((vector)llJsonGetValue(_currentColors, [component]), component);
  }
  else if (prmFunction == "gui_gag_device") {
    key userkey = (key)llJsonGetValue(prmJson, ["userkey"]);
    integer screen = 0;
    if ((integer)llJsonGetValue(prmJson, ["restorescreen"]) && _guiScreenLast) { screen = _guiScreenLast;}
    init_gui(userkey, screen);
  } else if (prmFunction == "reset_gui") {
    exit("");
  }
  else if (prmFunction == "add_restraint_by_type") {
    if (llJsonGetValue(value, ["type"]) != llJsonGetValue(get_self(), ["type"])) {
      return; // Incorrect type
    }

    if (llJsonGetValue(value, ["part"]) != llJsonGetValue(get_self(), ["part"])) {
      return; // Incorrect type
    }

    string restraintSet;
    restraintSet = llJsonSetValue(restraintSet, ["type"], llJsonGetValue(get_self(), ["part"]));
    restraintSet = llJsonSetValue(restraintSet, ["restraint"], define_restraint(llJsonGetValue(value, ["restraint"])));
    simple_request("add_restraint", restraintSet);
    return;
  }
}

default
{
  state_entry() {
    init();
  }

  listen(integer prmChannel, string prmName, key prmID, string prmText) {
    if (prmChannel = _guiChannel) {
      if (prmText == "<<Done>>") { exit("done"); return; }
      else if (prmText == " ") { gui(_guiScreen); return; }
      else if (prmText == "<<Back>>") {
        if (_guiScreen == GUI_STYLE) { gui(GUI_HOME); return; }
        else if (_guiScreen != GUI_HOME) { gui(_guiScreenLast); return;}
        gui_request("gui_bind", TRUE, _guiUserKey, 0);
        return;
      }
      else if (prmText == "Next >>") { _guiPage ++; gui(_guiScreen); return; }
      else if (prmText == "<< Previous") { _guiPage --; gui(_guiScreen); return; }

      if (prmText == "Ungag") {
        simple_request("remove_restraint", "gag");
        _resumeFunction = "set_restraints";
        return;
      }

      if (_guiScreen == GUI_HOME) {
        if (prmText == "<<Style>>") {
          gui(GUI_STYLE);
        } else {
          string restraint;
          restraint = llJsonSetValue(restraint, ["type"], llJsonGetValue(get_self(), ["part"]));
          restraint = llJsonSetValue(restraint, ["restraint"], define_restraint(prmText));
          simple_request("add_restraint", restraint);
          _resumeFunction = "set_restraints";
        }
        return;
      } else if (_guiScreen == GUI_STYLE) {
        if ("Strap" == prmText) { gui(GUI_COLOR_STRAP); }
        else if ("Ball" == prmText) { gui(GUI_COLOR_BALL); }
        //else if ("Texture" == prmText) { gui(111); }
        return;
      } else if (_guiScreen == GUI_COLOR_STRAP) {
        set_color_by_name(prmText, "strap");
      } else if (_guiScreen == GUI_COLOR_BALL) {
        set_color_by_name(prmText, "ball");
      }

      gui(_guiScreen);
      return;
    }
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function;
    string value;

    if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
      debug(prmText);
      return;
    }

    execute_function(function, prmText);

    if (function == _resumeFunction) {
      _resumeFunction = "";
      init_gui(_guiUserKey, _guiScreen);
    }
  }

  timer() {
    exit("timeout");
  }
}
