#include "includes/contrib_lib.lsl"
#include "includes/general_tools.lsl"

list _attachedFolders = [];

set_attachments(string prmAttachments) {
  list bindFolders = [];
  list preventFolders = [];
  string restraint;
  integer index;

  list liAttachments = llJson2List(prmAttachments);
  list addFolders = ListXnotY(liAttachments, _attachedFolders);
  list remFolders = ListXnotY(_attachedFolders, liAttachments);

  // Add folders
  for (index = 0; index < llGetListLength(addFolders); index++) {
    llOwnerSay("@attachover:~BreakFree/" + llList2String(addFolders, index) + "=force");
  }

  // Detatch folders
  for (index = 0; index < llGetListLength(remFolders); index++) {
    llOwnerSay("@detachall:~BreakFree/" + llList2String(remFolders, index) + "=force");
  }

  // Save setting
  _attachedFolders = liAttachments;
}

default
{
  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function;
    string value;

    if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
      debug(prmText);
      return;
    }
    value = llJsonGetValue(prmText, ["value"]);

    if ("set_attachments" == function) { set_attachments(value); }
  }

}
