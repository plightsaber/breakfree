#include "includes/general_tools.lsl"
#include "includes/restraint_tools.lsl"
#include "includes/user_lib.lsl"

float REST_TIMER = 60.0;

string  _owner;
integer _active;
integer _distraction;
integer _maxStamina;
integer _stamina;

string _progress;
string _puzzles;

init() {
  _active = FALSE;
  start_recovery_timer();
  simple_request("get_owner", "");
}

set_owner(string user) {
  _owner = user;
  _maxStamina = 100;

  if (has_feat(_owner, "Endurant+")) { _maxStamina = 150; }
  else if (has_feat(_owner, "Endurant")) { _maxStamina = 125; }
}

execute_function(string function, string json) {
  string value = llJsonGetValue(json, ["value"]);

  if ("set_owner" == function) {
    set_owner(value);
  } else if ("set_escape_distraction" == function) {
    _distraction = (integer)value;
  } else if ("set_escape_stamina" == function) {
    _stamina = (integer)value;
  } else if ("set_progress" == function) {
    _progress = llJsonGetValue(value, ["progress"]);
  } else if ("set_puzzles" == function) {
    _puzzles = llJsonGetValue(value, ["puzzles"]);
  } else if ("set_restraints" == function) {
    _restraints = value;
    start_recovery_timer();
  } else if ("start_recovery_timer" == function) {
    start_recovery_timer();
  } else if ("stop_recovery_timer" == function) {
    stop_recovery_timer();
  }
}

start_recovery_timer() {
  if (_active) {
    return; // Do not reset timer if timer is already running.
  }
  _active = TRUE;
  llSetTimerEvent(REST_TIMER);
}

stop_recovery_timer() {
  _active = FALSE;
  llSetTimerEvent(0.0);
}

default
{
  on_rez(integer prmStart) {
    init();
  }

  state_entry() {
    init();
  }

  link_message(integer sender_num, integer num, string str, key id) {
    string function;
    string value;

    if ((function = llJsonGetValue(str, ["function"])) == JSON_INVALID) {
      debug(str);
      return;
    }
    execute_function(function, str);
  }

  timer() {
    if (!is_bound()) {
      // Oops, shouldn't be here.  Just clean up
      _distraction = 0;
      simple_request("set_escape_distraction", (string)_distraction);

      _stamina = _maxStamina;
      simple_request("set_escape_stamina", (string)_stamina);

      stop_recovery_timer();
      return;
    }

    // Decrease distraction
    if (_distraction > 0) {
      _distraction -= 20/4;

      if (_distraction <= 0) {
        _distraction = 0;
        llOwnerSay("You are feeling less distracted.");
      }
      simple_request("set_escape_distraction", (string)_distraction);
    }

    if (_stamina == _maxStamina) {
      return;
    }

    // Refresh stamina
    integer denominator = 4;
    if (has_feat(_owner, "Resolute+")) {
      denominator = 1;
    } else if (has_feat(_owner, "Resolute")) {
      denominator = 2;
    }

    _stamina += _maxStamina/denominator;
    if (_stamina >= _maxStamina) {
      _stamina = _maxStamina;
      llOwnerSay("You are feeling fully rested.");
    }

    simple_request("set_escape_stamina", (string)_stamina);
  }
}
