#include "includes/blindfold_tools.lsl"
#include "includes/color_lib.lsl"
#include "includes/general_tools.lsl"
#include "includes/gui_tools.lsl"
#include "includes/texture_lib.lsl"
#include "includes/user_lib.lsl"
#include "includes/dictionary/texture_tape_dictionary.lsl"

integer _rpMode = FALSE;
integer _featSkip = FALSE;

integer GUI_HOME = 0;
integer GUI_STYLE = 100;
integer GUI_COLOR = 102;
integer GUI_TEXTURE = 111;

string _restraintLib;
string _villain;

string get_self() {
  if (_self != "") return _self;

  _self = llJsonSetValue(_self, ["name"], "Tape");
  _self = llJsonSetValue(_self, ["part"], "blindfold");
  _self = llJsonSetValue(_self, ["type"], "tape");
  _self = llJsonSetValue(_self, ["hasColor"], "1");
  return _self;
}

init() {
  if (!is_set(_currentColors)) {
    set_color(COLOR_SILVER, "tape");
  }
  if (!is_set(_currentTextures)) {
    set_texture(TEXTURE_DUCT, "tape");
  }
}

init_gui(key prmID, integer prmScreen) {
  _guiUserKey = prmID;

  if (_guiListener) { llListenRemove(_guiListener); }
  _guiChannel = (integer)llFrand(-9998) - 1;
  _guiListener = llListen(_guiChannel, "", _guiUserKey, "");
  gui(prmScreen);
}

send_availability_info() {
  simple_request("add_available_restraint", get_self());
}

gui(integer prmScreen) {
  // Reset Busy Clock
  llSetTimerEvent(GUI_TIMEOUT);

  string btn10 = " "; string btn11 = " ";   string btn12 = " ";
  string btn7 = " "; string btn8 = " ";   string btn9 = " ";
  string btn4 = " "; string btn5 = " ";   string btn6 = " ";
  string btn1 = "<<Back>>"; string btn2 = "<<Done>>";  string btn3 = " ";

  list mpButtons;
  _guiText = " ";

  // GUI: Main
  if (prmScreen == GUI_HOME) {
    get_current_restraints();
    btn3 = "<<Style>>";
    if (is_set(llJsonGetValue(_currentRestraints, ["blindfold"]))) {
      mpButtons += "Remove";
    } else {
      mpButtons += "Blindfold";
    }

    mpButtons = multipage_gui(mpButtons, 2, _guiPage);
  }
  // GUI: Colorize
  else if (prmScreen == GUI_STYLE) {
    _guiText = "Choose what you want to style.";
    mpButtons = multipage_gui(["Color"], 2, _guiPage);
  }
  else if (prmScreen == GUI_COLOR) {
    _guiText = "Choose a color for the blindfold.";
    mpButtons = multipage_gui(_colors, 3, _guiPage);
  }
  /*
  else if (prmScreen == GUI_TEXTURE) {
    _guiText = "Choose a texture for the blindfold.";
    mpButtons = multipage_gui(_textures, 3, _guiPage);
  }
  */

  if (prmScreen != _guiScreen) { _guiScreenLast = _guiScreen; }
  _guiScreen = prmScreen;

  _guiButtons = [btn1, btn2, btn3];
  if (btn4+btn5+btn6 != "   ") { _guiButtons += [btn4, btn5, btn6]; }
  if (btn7+btn8+btn9 != "   ") { _guiButtons += [btn7, btn8, btn9]; }
  if (btn10+btn11+btn12 != "   ") { _guiButtons += [btn10, btn11, btn12]; }

  // Load MP Buttons - hopefully the lengths were configured correctly!
  if (llGetListLength(mpButtons)) { _guiButtons += mpButtons; }

  llDialog(_guiUserKey, _guiText, _guiButtons, _guiChannel);
}

// ===== Main Functions =====
string define_restraint(string prmName) {
  string blindfold;

  // Type-specific values
  blindfold = llJsonSetValue(blindfold, ["name"], prmName);
  blindfold = llJsonSetValue(blindfold, ["type"], llJsonGetValue(get_self(), ["type"]));
  blindfold = llJsonSetValue(blindfold, ["canEscape"], "1");

  integer complexity;
  integer integrity;
  integer tightness;

  if (prmName == "Blindfold") {
    complexity = 1;
    integrity = 10;
    tightness = 5;

    blindfold = llJsonSetValue(blindfold, ["uid"], "blindfold_tape");
    blindfold = llJsonSetValue(blindfold, ["slot"], "blindfold");
    blindfold = llJsonSetValue(blindfold, ["canCut"], "1");
    blindfold = llJsonSetValue(blindfold, ["attachments", JSON_APPEND], "blindfold_tape");
  }

  blindfold = llJsonSetValue(blindfold, ["complexity"], (string)complexity);
  blindfold = llJsonSetValue(blindfold, ["integrity"], (string)integrity);
  blindfold = llJsonSetValue(blindfold, ["tightness"], (string)tightness);

  return blindfold;
}

// ===== Event Controls =====
execute_function(string prmFunction, string prmJson) {
  string value = llJsonGetValue(prmJson, ["value"]);
  if (JSON_INVALID == value) {
    //return;  // TODO: Rewrite all linked calls to send value in Json
  }

  if (prmFunction == "set_gender") { set_gender(value); }
  else if (prmFunction == "set_restraints") {
    _currentRestraints = llJsonGetValue(value, ["slots"]);
  }
  else if (prmFunction == "get_available_restraints") { send_availability_info(); }
  else if (prmFunction == "set_rpMode") { _rpMode = (integer)value; }
  else if (prmFunction == "set_featSkip") { _featSkip = (integer)value; }
  else if (prmFunction == "set_villain") { _villain = value; }
  else if (prmFunction == "set_color") {
    if (llJsonGetValue(value, ["attachment"]) != "blindfold") { return; }
    if (!is_set(llJsonGetValue(value, ["component"]))) { return; }
    _currentColors = llJsonSetValue(_currentColors, [llJsonGetValue(value, ["component"])], llJsonGetValue(value, ["color"]));
  }
  else if (prmFunction == "set_texture") {
    if (llJsonGetValue(value, ["attachment"]) != "blindfold") { return; }
    if (!is_set(llJsonGetValue(value, ["component"]))) { return; }
    _currentTextures = llJsonSetValue(_currentTextures, [llJsonGetValue(value, ["component"])], llJsonGetValue(value, ["texture"]));
  }
  else if (prmFunction == "request_style") {
    if (llJsonGetValue(value, ["attachment"]) != llJsonGetValue(get_self(), ["part"])) { return; }
    if (llJsonGetValue(value, ["name"]) != "tape") { return; }
    string component = llJsonGetValue(value, ["component"]);
    if ("" == component) { component = "tape"; }

    set_color((vector)llJsonGetValue(_currentColors, [component]), component);
    set_texture(llJsonGetValue(_currentTextures, [component]), component);
  }
  else if (prmFunction == "gui_blindfold_tape") {
    key userkey = (key)llJsonGetValue(prmJson, ["userkey"]);
    integer screen = 0;
    if ((integer)llJsonGetValue(prmJson, ["restorescreen"]) && _guiScreenLast) { screen = _guiScreenLast;}
    init_gui(userkey, screen);
  } else if (prmFunction == "reset_gui") {
    exit("");
  }
  else if (prmFunction == "add_restraint_by_type") {
    if (llJsonGetValue(value, ["type"]) != llJsonGetValue(get_self(), ["type"])) {
      return; // Incorrect type
    }

    if (llJsonGetValue(value, ["part"]) != llJsonGetValue(get_self(), ["part"])) {
      return; // Incorrect type
    }

    string restraintSet;
    restraintSet = llJsonSetValue(restraintSet, ["type"], llJsonGetValue(get_self(), ["part"]));
    restraintSet = llJsonSetValue(restraintSet, ["restraint"], define_restraint(llJsonGetValue(value, ["restraint"])));
    simple_request("add_restraint", restraintSet);
    return;
  }
}

default
{
  state_entry() {
    init();
  }

  listen(integer prmChannel, string prmName, key prmID, string prmText) {
    if (prmChannel = _guiChannel) {
      if (prmText == "<<Done>>") { exit("done"); return; }
      else if (prmText == " ") { gui(_guiScreen); return; }
      else if (prmText == "<<Back>>") {
        if (_guiScreen == 100) { gui(GUI_HOME); return; }
        else if (_guiScreen != 0) { gui(_guiScreenLast); return;}
        gui_request("gui_bind", TRUE, _guiUserKey, 0);
        return;
      }
      else if (prmText == "Next >>") { _guiPage ++; gui(_guiScreen); return; }
      else if (prmText == "<< Previous") { _guiPage --; gui(_guiScreen); return; }

      if (prmText == "Remove") {
        simple_request("remove_restraint", "blindfold");
        _resumeFunction = "set_restraints";
        return;
      }

      if (_guiScreen == GUI_HOME) {
        if (prmText == "<<Style>>") {
          gui(GUI_STYLE);
        } else {
          string restraint;
          restraint = llJsonSetValue(restraint, ["type"], llJsonGetValue(get_self(), ["part"]));
          restraint = llJsonSetValue(restraint, ["restraint"], define_restraint(prmText));
          simple_request("add_restraint", restraint);
          _resumeFunction = "set_restraints";
        }
        return;
      } else if (_guiScreen == GUI_STYLE) {
        if ("Color" == prmText) { gui(GUI_COLOR); }
        else if ("Texture" == prmText) { gui(GUI_TEXTURE); }
      } else if (_guiScreen == GUI_COLOR) {
        set_color_by_name(prmText, "tape");
      } else if (_guiScreen == GUI_TEXTURE) {
        set_texture_by_name(prmText, "tape");
      }

      gui(_guiScreen);
      return;
    }
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function;
    string value;

    if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
      debug(prmText);
      return;
    }

    execute_function(function, prmText);

    if (function == _resumeFunction) {
      _resumeFunction = "";
      init_gui(_guiUserKey, _guiScreen);
    }
  }

  timer() {
    exit("timeout");
  }
}
