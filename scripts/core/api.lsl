#include "includes/general_tools.lsl"
#include "includes/restraint_tools.lsl"
#include "includes/user_lib.lsl"

integer _apiListener;
string _owner;

init() {
  // Reset owner if mismatched.
  if (llJsonGetValue(_owner, ["uid"]) != llGetOwner()) {
    _owner = "";
  }

  if (_apiListener) { llListenRemove(_apiListener); }
  llListen(CHANNEL_API, "", NULL_KEY, "");
  if (_owner == "") { _owner = get_default_user(llGetOwner()); }
}

api(string prmJson) {
  string function = llJsonGetValue(prmJson, ["function"]);
  string value = llJsonGetValue(prmJson, ["value"]);
  if (!is_set(function)) {
    debug("ERROR: API call missing function: " + prmJson);
  }

  // Check for general requests that don't require validation
  if ("ping_bound" == function) {
    if (is_bound()) {
      string response = "";
      response = llJsonSetValue(response, ["function"], "pong_bound");
      response = llJsonSetValue(response, ["bound"], "1");
      response = llJsonSetValue(response, ["userKey"], llGetOwner());
      llRegionSayTo(llJsonGetValue(prmJson, ["key"]), CHANNEL_API, response);
    }
    return;
  }

  // Validate
  if (llJsonGetValue(prmJson, ["toKey"]) != llGetOwner()) { return; }

  // Execute API Call
  if (function == "get_touch_info") {
    api_request(llJsonGetValue(prmJson, ["fromKey"]), llGetOwner(), "touch_user", _owner);
  } else {
    if (value == JSON_INVALID) {
      debug("WARNING: Legacy api call detected: " + prmJson);
      value = prmJson;
    }

    simple_request(function, value);
  }
}

default
{
  on_rez(integer prmStart) {
    init();
  }

  state_entry() {
    init();
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function;
    string value;

    if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
      debug(prmText);
      return;
    }
    value = llJsonGetValue(prmText, ["value"]);

    if (function == "set_restraints") {
      _restraints = value;
      _owner = llJsonSetValue(_owner, ["armBound"], llJsonGetValue(value, ["armBound"]));
      _owner = llJsonSetValue(_owner, ["handBound"], (string)is_set(llJsonGetValue(value, ["slots", "hand"])));
    } else if (function == "set_owner") {
      _owner = llJsonSetValue(_owner, ["feats"], llJsonGetValue(value, ["feats"]));
    }
  }

  listen(integer prmChannel, string prmName, key prmID, string prmText) {
    api(prmText);
  }

}
