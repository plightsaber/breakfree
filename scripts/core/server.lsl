#include "includes/general_tools.lsl"
#include "includes/restraint_tools.lsl"
#include "includes/user_lib.lsl"

integer CHANNEL_CLIENT = -23254368;
integer CHANNEL_SERVER = -23773783;

integer _serverHandler;

integer _allowArm = TRUE;
integer _allowLeg = TRUE;
integer _allowGag = TRUE;
integer _allowBlindfold = TRUE;
integer _allowMitten = TRUE;


init() {
    if (_serverHandler) { llListenRemove(_serverHandler); }
    _serverHandler = llListen(CHANNEL_SERVER, "", NULL_KEY, "");
}

handle_request(string json) {
    // Ensure this server is the intended target.
    if (llJsonGetValue(json, ["serverAvi"]) != llGetOwner()) {
        return;
    }

    string restraintType = llJsonGetValue(json, ["restraintType"]);
    string clientKey = llJsonGetValue(json, ["clientKey"]);
    string request = llJsonGetValue(json, ["request"]);
    string jsonResponse = create_response(request, clientKey);

    // Filter any restraint types that are disabled.
    if (restraintType == "arm" && !_allowArm) { return; }
    else if (restraintType == "leg" && !_allowLeg) { return; }
    else if (restraintType == "gag" && !_allowGag) { return; }
    else if (restraintType == "blindfold" && !_allowBlindfold) { return; }
    else if (restraintType == "hand" && !_allowMitten) { return; }

    if ("change_owner" == request) {
        simple_request("set_villain", get_default_user(clientKey));
    }

    if ("is_locked" == request) {
        integer isLocked = FALSE;
        if (restraintType == "arm") { isLocked = is_set(llJsonGetValue(_restraints, ["armBound"])); }
        else if (restraintType == "leg") { isLocked = is_set(llJsonGetValue(_restraints, ["legBound"])); }
        else if (restraintType == "gag") { isLocked = is_set(llJsonGetValue(_restraints, ["gagged"])); }
        else if (restraintType == "blindfold") { isLocked = is_set(llJsonGetValue(_restraints, ["slots", "blindfold"])); }
        else if (restraintType == "hand") { isLocked = is_set(llJsonGetValue(_restraints, ["slots", "hand"])); }
        
        llRegionSayTo(clientKey, CHANNEL_CLIENT, llJsonSetValue(jsonResponse, ["data", "locked"], (string)isLocked));
        return;
    }
    
    if ("release" == request) {
        release_restraint(restraintType);
    }

    if ("bf_set_lock" == request) {
        string restraint = llJsonGetValue(json, ["data", "restraint"]);
        string type = llJsonGetValue(json, ["data", "material"]);

        string jsonRequest;
        jsonRequest = llJsonSetValue(jsonRequest, ["part"], restraintType);
        jsonRequest = llJsonSetValue(jsonRequest, ["type"], type);
        simple_request("add_restraint_by_type", llJsonSetValue(jsonRequest, ["restraint"], restraint));
    }
}

release_restraint(string restraintType) {
    if (restraintType == "arm") {
        simple_request("remove_restraint", "arm");  llSleep(1.0);
        simple_request("remove_restraint", "arm");  llSleep(1.0);
        simple_request("remove_restraint", "arm");  llSleep(1.0);
    }
    else if (restraintType == "leg") {
        simple_request("remove_restraint", "leg"); llSleep(1.0);
        simple_request("remove_restraint", "leg"); llSleep(1.0);
        simple_request("remove_restraint", "leg"); llSleep(1.0);
    }
    else if (restraintType == "gag") {
        simple_request("remove_restraint", "gag"); llSleep(1.0);
        simple_request("remove_restraint", "gag"); llSleep(1.0);
        simple_request("remove_restraint", "gag"); llSleep(1.0);
        simple_request("remove_restraint", "gag"); llSleep(1.0);
    }
    else if (restraintType == "blindfold") {
        simple_request("remove_restraint", "blindfold"); llSleep(1.0);
    }
    else if (restraintType == "hand") {
        simple_request("remove_restraint", "hand"); llSleep(1.0);
    }
}

string create_response(string request, key clientKey) {
    string jsonResponse;
    jsonResponse = llJsonSetValue(jsonResponse, ["serverAvi"], llGetOwner());
    jsonResponse = llJsonSetValue(jsonResponse, ["clientKey"], clientKey);
    jsonResponse = llJsonSetValue(jsonResponse, ["response"], request);
    jsonResponse = llJsonSetValue(jsonResponse, ["data", "restraintType"], "any");
    return jsonResponse;
}

default {
    state_entry() {
        init();
    }

    on_rez(integer start_param) {
        init();
    }

    link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
        string function;
        string value;

        if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
            debug(prmText);
            return;
        }
        value = llJsonGetValue(prmText, ["value"]);

        if (function == "set_restraints") {
            _restraints = value;
        }
        else if (function == "set_allowArm") { _allowArm = (integer)value; }
        else if (function == "set_allowLeg") { _allowLeg = (integer)value; }
        else if (function == "set_allowGag") { _allowGag = (integer)value; }
        else if (function == "set_allowBlindfold") { _allowBlindfold = (integer)value; }
        else if (function == "set_allowMitten") { _allowMitten = (integer)value; }
    }

    listen(integer channel, string name, key id, string message) {
        if (CHANNEL_SERVER == channel) {
            handle_request(message);
            return;
        }
    }
}