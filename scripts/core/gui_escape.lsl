#include "includes/contrib_lib.lsl"
#include "includes/general_tools.lsl"
#include "includes/gui_tools.lsl"
#include "includes/restraint_tools.lsl"
#include "includes/user_lib.lsl"

string CONFIG;
integer REST_TIMER = 60;

integer GUI_HOME = 0;
integer GUI_ESCAPE = 100;
integer GUI_RESCUE = 101;

key _configQueryID;

integer _lockable = TRUE;

integer _distraction;
integer _stamina;
integer _maxStamina = 100;

string _activePart;
string _escapeProgress;
string _owner;
string _guiUser;
string _puzzles;
string _security;

string _actionMsg;
string _resumeFunction;

init() {
  _configQueryID = llGetNotecardLine(".config", 0);
  _stamina = _maxStamina;
}

init_gui(key userKey, integer screen) {
  _actionMsg = "";
  _guiUserKey = userKey;
  if (_guiListener) { llListenRemove(_guiListener); }

  _guiChannel = (integer)llFrand(-9998) - 1;
  _guiListener = llListen(_guiChannel, "", _guiUserKey, "");
  gui(screen);
}

gui(integer prmScreen) {
  // Exit menu if exhausted
  if (!is_assisted() && _stamina <= 0) {
    simple_request("start_recovery_timer", (string)REST_TIMER);
    llOwnerSay("You are too tired to struggle anymore.");
    exit("Exhausted");
    return;
  }

  string btn10 = " "; string btn11 = " ";   string btn12 = " ";
  string btn7 = " "; string btn8 = " ";   string btn9 = " ";
  string btn4 = " "; string btn5 = " ";   string btn6 = " ";
  string btn1 = " "; string btn2 = "<<Done>>"; string btn3 = " ";

  _guiText = " ";

  if (prmScreen != _guiScreen) { _guiScreenLast = _guiScreen; }
  if (btn1 == " " && (prmScreen != 0)) { btn1 = "<<Back>>"; };

  if (prmScreen == GUI_HOME) {
    if (_guiUserKey == llGetOwner()) { btn1 = "<<Back>>"; }

    // Reset previous screen
    _guiScreenLast = 0;

    if (is_set(llJsonGetValue(_security, ["arm", "tightness"]))) { btn4 = "Free Arms"; }
    if (is_set(llJsonGetValue(_security, ["leg", "tightness"]))) { btn5 = "Free Legs"; }
    if (is_set(llJsonGetValue(_security, ["gag", "tightness"]))) { btn6 = "Free Gag"; }
    if (is_set(llJsonGetValue(_restraints, ["slots", "hand"]))) { btn7 = "Free Hands"; }
    if (is_set(llJsonGetValue(_restraints, ["slots", "crotch"]))) { btn8 = "Free Crotch"; }
    if (is_set(llJsonGetValue(_restraints, ["slots", "blindfold"]))) { btn9 = "Free Blindfold"; }

  } else if (prmScreen == GUI_ESCAPE) {
    if (!has_feat(_owner, "Steadfast")) { simple_request("stop_recovery_timer", ""); }

    btn3 = "Rest";

    btn4 = "Twist";
    btn5 = "Struggle";
    btn6 = "Thrash";

    if (_activePart == "hand" || !is_set(llJsonGetValue(_restraints, ["slots", "hand"]))) { btn7 = "Pick"; }
    btn8 = "Tug";
    btn9 = "Yank";

    // Lockpick Verb overrides
    if (is_picking() && !is_set(llJsonGetValue(_restraints, ["slots", "hand"]))) {
      btn7 = "Poke";
      btn8 = "Sweep";
      btn9 = "Turn";
    }

    // Blade / Cropper Verb overrides
    if (is_cutting() && !is_set(llJsonGetValue(_restraints, ["slots", "hand"]))) {
      btn7 = "Slice";
      btn8 = "Dice";
      btn9 = "Rend";
    }

    _guiText = "Restraint: " + ToTitle(_activePart); // TODO: Get full name of restraint
    _guiText += "\nSecurity: " + display_security(_activePart);
    if (_actionMsg) { _guiText += "\n" + _actionMsg; }
    _guiText += "\n" + get_suggested_action();
  } else if (prmScreen == GUI_RESCUE) {
    if (!is_set(llJsonGetValue(_guiUser, ["handBound"]))) { btn4 = "Pick"; }
    btn5 = "Tug";
    btn6 = "Yank";

    // Lockpick Verb overrides
    if (is_picking() && !is_set(llJsonGetValue(_restraints, ["slots", "hand"]))) {
      btn4 = "Poke";
      btn5 = "Sweep";
      btn6 = "Turn";
    }

    // Blade / Cropper Verb overrides
    if (is_cutting() && !is_set(llJsonGetValue(_restraints, ["slots", "hand"]))) {
      btn7 = "Slice";
      btn8 = "Dice";
      btn9 = "Rend";
    }

    _guiText = "Restraint: " + ToTitle(_activePart); // TODO: Get full name of restraint
    _guiText += "\nSecurity: " + display_security(_activePart);
    _guiText += "\n" + get_suggested_action();
    // TODO: Suggested action for feats?
    if (_actionMsg) { _guiText += "\n" + _actionMsg; }
  }

  _guiScreen = prmScreen;
  _guiButtons = [btn1, btn2, btn3];

  if (btn4+btn5+btn6 != "   ") { _guiButtons += [btn4, btn5, btn6]; }
  if (btn7+btn8+btn9 != "   ") { _guiButtons += [btn7, btn8, btn9]; }
  if (btn10+btn11+btn12 != "   ") { _guiButtons += [btn10, btn11, btn12]; }
  llDialog(_guiUserKey, _guiText, _guiButtons, _guiChannel);
}

string display_security(string restraint) {
  integer index;

  integer tightness = (integer)llJsonGetValue(_security, [restraint, "tightness"]);
  integer integrity = (integer)llJsonGetValue(_security, [restraint, "integrity"]);
  integer complexity = (integer)llJsonGetValue(_security, [restraint, "complexity"]);

  integer integrityProgress = (integer)llJsonGetValue(_escapeProgress, ["integrity", "progress"]);
  integer complexityProgress = (integer)llJsonGetValue(_escapeProgress, ["complexity", "progress"]);

  integer totalSecurity = (integrity + (tightness * complexity)) / 2;
  integer totalProgress = (integrityProgress + (tightness * complexityProgress + 1)) / 2;

  integer security = totalSecurity - totalProgress;

  string output;
  string char = "_";

  if (security > 70) {
    return "[  SECURE  ]";
  } else if (security > 60) { // 60 is Hammerlock without any adjustment feats.
    char = "#";
    security -= 60;
    for (index = 0; index < security; index ++) { output += "!"; }
  } else if (security > 50) {
    char = "=";
    security -= 50;
    for (index = 0; index < security; index ++) { output += "#"; }
  } else if (security > 40) {
    char = "+";
    security -= 40;
    for (index = 0; index < security; index ++) { output += "="; }
  } else if (security > 30) {
    char = ":";
    security -= 30;
    for (index = 0; index < security; index ++) { output += "+"; }
  } else if (security > 20) {
    char = "-";
    security -= 20;
    for (index = 0; index < security; index ++) { output += ":"; }
  } else if (security > 10) {
    char = ".";
    security -= 10;
    for (index = 0; index < security; index ++) { output += "-"; }
  } else {
    for (index = 0; index < security; index ++) { output += "."; }
  }

  while (index < 10) {
    output += char;
    index++;
  }
  return "[" + output + "]";
}

string get_suggested_action() {
  integer eidetic = has_feat(_guiUser, "Eidetic");
  integer intuitive = has_feat(_guiUser, "Intuitive");

  // Check tightness vs integrity suggestion
  string puzzleType = "tightness";
  integer progress = (integer)llJsonGetValue(_escapeProgress, ["tightness", "progress"]);
  integer tightness = (integer)llJsonGetValue(_security, [_activePart, "tightness"]);
  integer maxProgress = (integer)llJsonGetValue(_escapeProgress, ["tightness", "maxProgress"]);

  if (ignore_tightness() || progress >= tightness) {
    puzzleType = "integrity";
    progress = (integer)llJsonGetValue(_escapeProgress, ["integrity", "progress"]);
    tightness = (integer)llJsonGetValue(_security, [_activePart, "integrity"]);
    maxProgress = (integer)llJsonGetValue(_escapeProgress, ["integrity", "maxProgress"]);
  }

  if ("integrity" == puzzleType && !intuitive && !is_picking()) {
    return "You think it is time to break free!";
  } else if (is_picking() && "integrity" == puzzleType && !intuitive) {
    return "You think it is time to pick the lock!";
  } else if (maxProgress > progress && eidetic) {
    return "You think you should try to " + action_to_string((integer)llJsonGetValue(_puzzles, [puzzleType, progress]), puzzleType);
  } else if (intuitive) {
    integer action1 = (integer)llJsonGetValue(_puzzles, [puzzleType, progress]);
    integer action2;
    do {
      action2 = roll(1,3);
    } while (action2 == action1);
    list actions = [action1, action2];
    actions = llListSort(actions, 1, TRUE);

    return "You think should " + action_to_string(llList2Integer(actions, 0), puzzleType) + " or " + action_to_string(llList2Integer(actions, 1), puzzleType) + ".";
  }
  return "You are not sure how to escape.";
}

string action_to_string(integer action, string puzzleType) {

  // Cause a distraction ...
  if (roll(_distraction, 5) >= 20) { return "???"; }

  if (puzzleType == "tightness") {
    if (action == 1) { return "Twist"; }
    else if (action == 2) { return "Struggle"; }
    else if (action == 3) { return "Thrash"; }
  } else if (puzzleType == "integrity") {
    if (is_picking()) {
      if (action == 1) { return "Poke"; }
      else if (action == 2) { return "Sweep"; }
      else if (action == 3) { return "Turn"; }
    }

    if (is_cutting()) {
      if (action == 1) { return "Slice"; }
      else if (action == 2) { return "Dice"; }
      else if (action == 3) { return "Rend"; }
    }

    if (action == 3) { return "Pick"; }
    else if (action == 2) { return "Tug"; }
    else if (action == 1) { return "Yank"; }
  }

  return "ERROR: UNKNOWN ACTION:" + (string)(action);
}

integer can_escape() {
  return !_lockable
    || is_set(llJsonGetValue(_security, [_activePart, "canEscape"]))
    || (is_picking() && is_set(llJsonGetValue(_security, [_activePart, "canPick"])));
}

integer check_complexity() {
  integer complexityProgress = (integer)llJsonGetValue(_escapeProgress, ["complexity", "progress"]);
  integer complexity = (integer)llJsonGetValue(_security, [_activePart, "complexity"]);

  if (complexityProgress >= complexity) {
    _escapeProgress = JSON_NULL;
    _puzzles = JSON_NULL;

    llWhisper(0, get_name() + " is freed from " + get_owner_pronoun("her") + " " + _activePart + " restraints.");
    simple_request("remove_restraint", _activePart);
    _guiScreen = GUI_HOME; // Exit the current screen when restraint level is removed
    _resumeFunction = "set_security";
    return TRUE;
  }

  return FALSE;
}

integer check_integrity() {
  integer integrityProgress = (integer)llJsonGetValue(_escapeProgress, ["integrity", "progress"]);
  integer integrity = (integer)llJsonGetValue(_security, [_activePart, "integrity"]);
  integer tightness = (integer)llJsonGetValue(_security, [_activePart, "tightness"]);

  if (integrityProgress >= integrity) {
    _actionMsg = "Your restraint suddenly feels looser!";
    float adjustment = 1;
    if (ignore_tightness() && ignore_integrity()) {
      adjustment = 0.3;
    } else if (ignore_tightness() || ignore_integrity()) {
      adjustment = 0.75;
    }
    integer expEarned = (integer)(tightness*adjustment);

    if (is_assisted()) {
      api_request(llJsonGetValue(_guiUser, ["key"]), llGetOwner(), "add_exp", (string)expEarned);
    } else {
      // Earn experience in escapology!
      simple_request("add_exp", (string)expEarned);
    }

    // Reset puzzles
    refresh_puzzle("tightness");
    refresh_puzzle("integrity");

    // Update complexity.
    integer cProgress = (integer)llJsonGetValue(_escapeProgress, ["complexity", "progress"]) + 1;
    update_progress("complexity", cProgress);
    return TRUE;
  }

  return FALSE;
}

integer check_thrash() {
  integer dieSides = 100;
  if (has_feat(_owner, "Athletic")) {
    dieSides = 75;
  } else if (has_feat(_owner, "Athletic+")) {
    dieSides = 50;
  }

  return roll(1, dieSides) == dieSides;
}

escape_action(string prmVerb) {
  string action;
  integer stamina;
  integer exertion;
  string puzzleType;

  integer is_cutting = 0;
  integer is_picking = 0;

  // TODO: Central definition?
  if (prmVerb == "Twist") { action = "1"; exertion = 3; puzzleType = "tightness"; }
  else if (prmVerb == "Struggle") { action = "2"; exertion = 4; puzzleType = "tightness"; }
  else if (prmVerb == "Thrash") { action = "3"; exertion = 7; puzzleType = "tightness"; }

  else if (prmVerb == "Pick") { action = "3"; exertion = 2; puzzleType = "integrity";}
  else if (prmVerb == "Tug") { action = "2"; exertion = 5; puzzleType = "integrity";}
  else if (prmVerb == "Yank") { action = "1"; exertion = 5; puzzleType = "integrity";}

  else if ("Poke" == prmVerb) { action = "1"; exertion = 2; puzzleType = "integrity"; is_picking = TRUE;}
  else if ("Sweep" == prmVerb) { action = "2"; exertion = 2; puzzleType = "integrity"; is_picking = TRUE;}
  else if ("Turn" == prmVerb) { action = "3"; exertion = 2; puzzleType = "integrity"; is_picking = TRUE;}

  else if ("Slice" == prmVerb) { action = "1"; exertion = 2; puzzleType = "integrity"; is_cutting = TRUE;}
  else if ("Dice" == prmVerb) { action = "2"; exertion = 2; puzzleType = "integrity"; is_cutting = TRUE;}
  else if ("Rend" == prmVerb) { action = "3"; exertion = 2; puzzleType = "integrity"; is_cutting = TRUE;}

  integer maxProgress = (integer)llJsonGetValue(_escapeProgress, [puzzleType, "maxProgress"]);
  integer progress = (integer)llJsonGetValue(_escapeProgress, [puzzleType, "progress"]);
  integer tightness = (integer)llJsonGetValue(_security, [_activePart, "tightness"]);

  if (!is_assisted() && is_arm_bound()) {
    stamina = _stamina - exertion;
    update_stamina(stamina);
  }

  // Execute action
  //debug("CORRECT ACTION: " + llJsonGetValue(_puzzles, [puzzleType, progress]));
  integer success = (action == llJsonGetValue(_puzzles, [puzzleType, progress]));
  if (success && puzzleType == "integrity" && !ignore_tightness()) {
    // Tightness failure
    integer tightnessProgress = (integer)llJsonGetValue(_escapeProgress, ["tightness", "progress"]);
    if (llFrand(1.0) > ((float)(tightnessProgress-(tightness-tightnessProgress))/(float)tightness)) {
      success = FALSE;
    }

    // Escapability
    if (!can_escape() && !is_picking && !is_cutting) {
      success = FALSE;
    }
  }

  // Set escape progress
  if (prmVerb == "Thrash"
    && can_escape()
    && check_thrash()
  ) {
    _actionMsg = "You think you've made some unexpected progress.";
    puzzleType = "integrity";
    maxProgress = (integer)llJsonGetValue(_escapeProgress, [puzzleType, "maxProgress"]);
    progress = (integer)llJsonGetValue(_escapeProgress, [puzzleType, "progress"]);
    progress++;
    if (maxProgress < progress) {
      maxProgress = progress;
      _escapeProgress = llJsonSetValue(_escapeProgress, [puzzleType, "maxProgress"], (string)maxProgress);
    }
  } else if (success) {
    _actionMsg = "You think you've made some progress.";
    progress++;
    if (maxProgress < progress) {
      maxProgress = progress;
      _escapeProgress = llJsonSetValue(_escapeProgress, [puzzleType, "maxProgress"], (string)maxProgress);
    }
  } else {
    _actionMsg = "Your " + prmVerb + " didn't help.";
    if (!is_assisted() && progress > 0 && puzzleType == "tightness") {
      progress = lose_progress(progress);
    }

    if ("integrity" == puzzleType && is_picking) {
      progress = 0;
    }
  }
  update_progress(puzzleType, progress);

  // Animate
  if (!is_assisted() && puzzleType == "tightness") {
    if (success) { simple_request("animate", "animation_" + _activePart + "_success"); }
    else { simple_request("animate", "animation_" + _activePart + "_failure"); }
  }

  // Adjust distraction
  if (!is_assisted() && !success && is_set(llJsonGetValue(_restraints, ["slots", "crotch"]))) {
    update_distraction(_distraction + 1);
  }

  // Check if anything should be escaped from
  if (success && puzzleType == "integrity") {
    // If being rescued by an unbound avi, every success reduces complexity
    if (ignore_integrity()) {
      _escapeProgress = llJsonSetValue(_escapeProgress, ["integrity", "progress"], "9999");
    }

    if (check_integrity()) {
      if (check_complexity()) {
        return; // GUI will resume after getting restraints call
      }
    }
  }
  gui(_guiScreen);
}

get_gui(string part) {
  string tmpType;
  _activePart = part;
  _actionMsg = "";

  _guiScreen = GUI_RESCUE;
  if (_guiUserKey == llGetOwner()) {
    _guiScreen = GUI_ESCAPE;
  }

  _resumeFunction = "set_active_escape_data";
  simple_request("get_escape_data", _activePart);
}

integer ignore_integrity() {
  return (is_assisted() && !(integer)llJsonGetValue(_guiUser, ["armBound"])) && !is_set(llJsonGetValue(_guiUser, ["handBound"]))
    || (!is_assisted() && !is_arm_bound())
    || (is_set(llJsonGetValue(_guiUser, ["blade"])) && is_set(llJsonGetValue(_security, [_activePart, "canCut"])))
    || (is_set(llJsonGetValue(_guiUser, ["cropper"])) && is_set(llJsonGetValue(_security, [_activePart, "canCrop"])))
  ;
}

integer ignore_tightness() {
  return is_assisted() || !is_arm_bound();
}

integer is_assisted() {
  return _guiUserKey != llGetOwner();
}

integer is_cutting() {
  if (is_set(llJsonGetValue(_security, [_activePart, "canCut"])) && is_set(llJsonGetValue(_guiUser, ["blade"]))) {
    return TRUE;
  }

  if (is_set(llJsonGetValue(_security, [_activePart, "canCrop"])) && is_set(llJsonGetValue(_guiUser, ["cropper"]))) {
    return TRUE;
  }
  return FALSE;
}

integer is_picking() {
  return is_set(llJsonGetValue(_security, [_activePart, "canPick"]))
    && is_set(llJsonGetValue(_guiUser, ["pick"]));
}

integer lose_progress(integer progress) {
  if (has_feat(_owner, "Flexible+")) {
    return progress;
  } else if (has_feat(_owner, "Flexible")) {
    progress--;
  } else {
    progress = progress - 2;
  }


  if (progress < 0) {
    return 0;
  }

  return progress;
}

set_active_escape_data(string json) {
  _puzzles = llJsonGetValue(json, ["puzzles"]);
  _escapeProgress = llJsonGetValue(json, ["progress"]);

  if (!is_set(_puzzles) || !is_set(_escapeProgress)) {
    refresh_puzzle("tightness");
    refresh_puzzle("integrity");
    refresh_puzzle("complexity");
  }
}

set_owner(string user) {
  _owner = user;
  _maxStamina = 100;

  if (has_feat(_owner, "Endurant+")) { _maxStamina = 150; }
  else if (has_feat(_owner, "Endurant")) { _maxStamina = 125; }
}

set_restraints(string restraints) {
  _restraints = restraints;
}

set_security(string security) {
  _security = security;
}

update_distraction(integer distraction) {
  _distraction = distraction;
  if (_distraction > 20) {
    _distraction = 20;
  }
  simple_request("set_escape_distraction", (string)_distraction);
}

update_progress(string type, integer progress) {
  _escapeProgress = llJsonSetValue(_escapeProgress, [type, "progress"], (string)progress);

  string request;
  request = llJsonSetValue(request, ["restraint"], _activePart);
  request = llJsonSetValue(request, ["progress"], _escapeProgress);
  simple_request("set_progress", request);
}

update_stamina(integer stamina) {
  _stamina = stamina;
  if (_stamina < 0) {
    _stamina = 0;
  }
  simple_request("set_escape_stamina", (string)_stamina);
}

// ===== Puzzle Methods =====
list generate_puzzle(integer length) {
  list puzzle = [];
  integer index;
  for (index = 0; index < length; index++) {
    integer result = roll(1, 20);
    if (result <= 7) { puzzle += "1"; }   // 35%
    else if (result <= 16) { puzzle += "2"; } // 45%
    else { puzzle += "3"; }      // 20%
  }

  return puzzle;
}

refresh_puzzle(string puzzleType) {
  string restraint = _activePart;

  _escapeProgress = llJsonSetValue(_escapeProgress, [puzzleType, "maxProgress"], "0");
  update_progress(puzzleType, 0);

  string securityLevel = llJsonGetValue(_security, [restraint, puzzleType]);
  if (!is_set(securityLevel)) {
    _puzzles = llJsonSetValue(_puzzles, [puzzleType], JSON_NULL);
    return;
  }

  list puzzle = generate_puzzle((integer)securityLevel);
  _puzzles = llJsonSetValue(_puzzles, [puzzleType], llList2Json(JSON_ARRAY, puzzle));

  string request;
  request = llJsonSetValue(request, ["restraint"], _activePart);
  request = llJsonSetValue(request, ["puzzles"], _puzzles);
  simple_request("set_puzzles", request);
}

// ===== Events =====
execute_function(string function, string json) {
  string value = llJsonGetValue(json, ["value"]);

  if (function == "set_owner") { set_owner(value); }
  else if (function  == "set_active_escape_data") {
    set_active_escape_data(value);
    return;
  }
  else if (function == "set_escape_distraction") { _distraction = (integer)value; }
  else if (function == "set_escape_stamina") { _stamina = (integer)value; }
  else if (function == "set_lockable") { _lockable = (integer)value; }
  else if (function == "set_restraints") { set_restraints(value); }
  else if (function == "set_security") { set_security(value); }
  else if (function == "set_toucher") { _guiUser = value; }
  else if (function == "gui_escape") {
    integer screen = 0;
    key userkey = (key)llJsonGetValue(json, ["userkey"]);

    if ((integer)llJsonGetValue(json, ["restorescreen"]) && _guiScreen) { screen = _guiScreen;}
    init_gui(userkey, screen);
  } else if (function == "reset_gui") {
    exit("");
  }
}

default
{
  state_entry() {
    init();
  }

  dataserver(key queryID, string configData) {
    if (queryID == _configQueryID) {
      CONFIG = configData;
    }
  }

  link_message(integer sender_num, integer num, string str, key id) {
    string function;
    string value;

    if ((function = llJsonGetValue(str, ["function"])) == JSON_INVALID) {
      debug(str);
      return;
    }
    execute_function(function, str);

    if (function == _resumeFunction) {
      _resumeFunction = "";
      init_gui(_guiUserKey, _guiScreen);
    }
  }

  listen(integer channel, string name, key id, string message) {
    if (channel != _guiChannel) { return; }

    if (message == "<<Done>>") { exit("done"); return; }
    else if (message == " ") { gui(_guiScreen); return; }
    else if (_guiScreen != GUI_HOME && message == "<<Back>>") { gui(_guiScreenLast); return; }

    if (_guiScreen == GUI_HOME) {
      if (message == "Free Arms") { get_gui("arm"); return; }
      else if (message == "Free Legs") { get_gui("leg"); return; }
      else if (message == "Free Gag") { get_gui("gag"); return; }
      else if (message == "Free Blindfold") { get_gui("blindfold"); return; }
      else if (message == "Free Hands") { get_gui("hand"); return; }
      else if (message == "Free Crotch") { get_gui("crotch"); return; }
      else if (message == "<<Back>>") {
        gui_request("gui_owner", TRUE, _guiUserKey, 0);
        return;
      }
    } else if (_guiScreen == GUI_ESCAPE || _guiScreen == GUI_RESCUE) {
      if ("Rest" == message) {
        simple_request("start_recovery_timer", (string)REST_TIMER);
      } else {
        escape_action(message);
      }
    }
  }

  timer() {
    exit("timeout");
  }
}
