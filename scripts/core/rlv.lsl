#include "includes/general_tools.lsl"
#include "includes/dictionary/chat_channels.lsl"

integer _armBound;
integer _legBound;
integer _gagBound;
integer _blindfolded;
integer _mittened;

integer _RLV; // Toggle if RLV should be activated or not
string _restraints;

init() {
  set_restraints(_restraints);
}

restrict_detach() {
  if (_RLV && (_armBound || _legBound || _gagBound || _blindfolded || _mittened)) {
    llOwnerSay("@detach=n");
  } else {
    llOwnerSay("@detach=y");
  }
}

restrict_speech() {
  // Note:  Ignores _RLV setting, we always want to "Mmph!"
  if (_gagBound) {
    llOwnerSay("@redirchat:" + (string)CHANNEL_GAGCHAT + "=add");
    llOwnerSay("@rediremote:" + (string)CHANNEL_GAGEMOTE + "=add");
    return;
  }

  // Disable garbling
  llOwnerSay("@redirchat:" + (string)CHANNEL_GAGCHAT + "=rem");
  llOwnerSay("@rediremote:" + (string)CHANNEL_GAGEMOTE + "=rem");
}

restrict_touch() {
  if (_RLV && (_armBound || _mittened)) {
    llOwnerSay("@touchfar=n");
  } else {
    llOwnerSay("@touchfar=y");
  }
}

restrict_vision() {
  if (_RLV && _blindfolded) {
    llOwnerSay("@clear,setsphere=n,setsphere_distmin:1=force,setsphere_valuemin:0.2=force,setsphere_distmax:2.0=force,setsphere_valuemax:0.99=force");
  } else {
    llOwnerSay("@clear");
  }
}

reset_restrictions() {
  restrict_vision();  // Always set vision first as it clears the other locks.
  restrict_detach();
  restrict_speech();
  restrict_touch();
}

set_restraints(string prmJson) {
  _restraints = prmJson;

  _armBound = (integer)llJsonGetValue(prmJson, ["armBound"]);
  _legBound = (integer)llJsonGetValue(prmJson, ["legBound"]);
  _gagBound = (integer)llJsonGetValue(prmJson, ["gagged"]);
  _blindfolded = is_set(llJsonGetValue(prmJson, ["slots", "blindfold"]));
  _mittened = is_set(llJsonGetValue(_restraints, ["slots", "hand"]));

  reset_restrictions();
}

default
{
  on_rez(integer prmStart) {
    init();
  }

  state_entry() {
    init();
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function;
    string value;

    if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
      debug(prmText);
      return;
    }
    value = llJsonGetValue(prmText, ["value"]);

    if (function == "set_restraints") { set_restraints(value); }
    else if (function == "set_rlv") {
      _RLV = (integer)value;
      reset_restrictions();
    }
  }
}
