#include "includes/contrib_lib.lsl"
#include "includes/gui_tools.lsl"
#include "includes/pose_lib.lsl"
#include "includes/user_lib.lsl"

// Quick keys
key _ownerID;
key _villainKey;

// Settings
integer _rpMode = FALSE;
integer _lockable = TRUE;
integer _RLV = TRUE;
integer _featSkip = TRUE;

integer _allowArm = TRUE;
integer _allowLeg = TRUE;
integer _allowGag = TRUE;
integer _allowMitten = TRUE;
integer _allowBlindfold = TRUE;

// Stats
string _owner; // User object
integer _ownerExp = 0;
list _ownerFeats = [];

key _statsQueryID;

// Status variables
integer _isArmsBound = FALSE;
integer _isLegsBound = FALSE;
integer _isGagged = FALSE;
integer _isBlindfolded = FALSE;
integer _isHandBound = FALSE;
integer _isOtherBound = FALSE;

list _legPoses;

// GUI screens
integer GUI_HOME = 0;
integer GUI_STATS = 10;
integer GUI_OPTIONS = 20;
integer GUI_POSE = 70;

string _resumeFunction;

list _feats = [
  "Athletic",
  "Athletic+",
  "Eidetic",
  "Intuitive",
  "Endurant",
  "Endurant+",
  "Flexible",
  "Flexible+",
  "Resolute",
  "Resolute+",
  "Steadfast",
  "Anubis",
  "Anubis+",
  "Anubis++",
  "Rigger",
  "Rigger+",
  "Gag Snob",
  "Gag Snob+",
  "Sadist"
];

init() {
  _ownerID = llGetOwner();

  // Reset owner if mismatched.
  if (llJsonGetValue(_owner, ["key"]) != _ownerID) {
    _owner = "";
  }

  if (_owner == "" && llGetInventoryKey(".stats") != NULL_KEY) {
    _statsQueryID = llGetNotecardLine(".stats",0); // Load config.
  }

  _owner = llJsonSetValue(_owner, ["key"], llGetOwner());
  _owner = llJsonSetValue(_owner, ["name"], llGetDisplayName(llGetOwner()));
  _owner = llJsonSetValue(_owner, ["gender"], get_gender_from_key(llGetOwner()));

  _owner = llJsonSetValue(_owner, ["feats"], llList2Json(JSON_ARRAY, _ownerFeats));
  _owner = llJsonSetValue(_owner, ["exp"], (string)_ownerExp);
  _owner = llJsonSetValue(_owner, ["armBound"], (string)_isArmsBound);
  _owner = llJsonSetValue(_owner, ["handBound"], (string)_isHandBound);
  _owner = llJsonSetValue(_owner, ["blade"], JSON_NULL);
}

init_gui(key prmID, integer prmScreen) {
  _guiUserKey = prmID;

  if (_guiListener) { llListenRemove(_guiListener); }
  _guiChannel = (integer)llFrand(-9998) - 1;
  _guiListener = llListen(_guiChannel, "", _guiUserKey, "");
  gui(prmScreen);
}

// ===== GUI =====
gui(integer prmScreen) {
  // Reset Busy Clock
  llSetTimerEvent(GUI_TIMEOUT);

  string btn10 = " "; string btn11 = " ";   string btn12 = " ";
  string btn7 = " "; string btn8 = " ";   string btn9 = " ";
  string btn4 = " "; string btn5 = " ";   string btn6 = " ";
  string btn1 = " "; string btn2 = "<<Done>>"; string btn3 = " ";

  _guiText = " ";
  list mpButtons;

  // GUI: Main
  if (prmScreen == GUI_HOME) {
    _guiText = "v5.2.0";
    if (_rpMode || (!_isArmsBound && !_isLegsBound && !_isGagged) || _villainKey == _ownerID) {
      btn1 = "Options";
      btn4 = "Bind";
    }

    if (_isLegsBound) {
      btn6 = "Pose";
    }

    if (_isArmsBound || _isLegsBound || _isGagged || _isBlindfolded || _isOtherBound) { btn5 = "Escape"; }
    btn3 = "Stats";
  }
  // GUI: Stats
  else if (prmScreen == GUI_STATS) {
    _guiText = "Level: " + (string)get_user_level() + "\n";
    _guiText += "Experience: " + (string)_ownerExp + "/" + (string)get_next_level_exp() + "\n";
    _guiText += "Feats: " + llDumpList2String(_ownerFeats, ", ");

    if (can_level_up()) {
      mpButtons = get_available_feats();
    }
    mpButtons = multipage_gui(mpButtons, 3, _guiPage);

    btn1 = "<<Back>>";
    btn3 = "Export";
  }
  else if (prmScreen == GUI_OPTIONS) {
    _guiText = "User Settings" + "\n";
    _guiText += "Please reference the included README for details.";

    if (_rpMode) { mpButtons += "☑ RP Mode"; }
    else { mpButtons += "☒ RP Mode"; }

    if (_RLV) { mpButtons += "☑ RLV"; }
    else { mpButtons += "☒ RLV"; }

    if (_lockable) { mpButtons += "☑ Lockable"; }
    else { mpButtons += "☒ Lockable"; }

    if (_allowArm) { mpButtons += "☑ Arms"; }
    else { mpButtons += "☒ Arms"; }

    if (_allowLeg) { mpButtons += "☑ Legs"; }
    else { mpButtons += "☒ Legs"; }

    if (_allowGag) { mpButtons += "☑ Gags"; }
    else { mpButtons += "☒ Gags"; }

    if (_allowMitten) { mpButtons += "☑ Mittens"; }
    else { mpButtons += "☒ Mittens"; }

    if (_allowBlindfold) { mpButtons += "☑ Blindfolds"; }
    else { mpButtons += "☒ Blindfolds"; }

    if (_featSkip) { mpButtons += "☑ Feat Skip"; }
    else { mpButtons += "☒ Feat Skip"; }

    btn1 = "<<Back>>";
  }
  else if (prmScreen == GUI_POSE) {
    _guiText = "How do you want to pose?";
    mpButtons = multipage_gui(_legPoses, 3, _guiPage);
  }

  if (prmScreen != _guiScreen) { _guiScreenLast = _guiScreen; }
  _guiScreen = prmScreen;

  _guiButtons = [btn1, btn2, btn3];
  if (btn4+btn5+btn6 != "   ") { _guiButtons += [btn4, btn5, btn6]; }
  if (btn7+btn8+btn9 != "   ") { _guiButtons += [btn7, btn8, btn9]; }
  if (btn10+btn11+btn12 != "   ") { _guiButtons += [btn10, btn11, btn12]; }

  if (llGetListLength(mpButtons)) { _guiButtons += mpButtons; }
  llDialog(_guiUserKey, _guiText, _guiButtons, _guiChannel);
}

// ===== Gets & Sets =====
list get_available_feats() {

  // Remove any feats we already have
  list feats = ListXnotY(_feats, _ownerFeats);

  // Remove any feats that have unmet prerequisites
  feats = require_prerequisite_feat(feats, "Athletic+", "Athletic");
  feats = require_prerequisite_feat(feats, "Endurant+", "Endurant");
  feats = require_prerequisite_feat(feats, "Flexible+", "Flexible");
  feats = require_prerequisite_feat(feats, "Resolute+", "Resolute");
  feats = require_prerequisite_feat(feats, "Sadist", "Rigger+");
  feats = require_prerequisite_feat(feats, "Rigger+", "Rigger");
  feats = require_prerequisite_feat(feats, "Anubis++", "Anubis+");
  feats = require_prerequisite_feat(feats, "Anubis+", "Anubis");
  feats = require_prerequisite_feat(feats, "Gag Snob+", "Gag Snob");

  return feats;
}

set_restraints(string prmJson) {
  _isArmsBound = (integer)llJsonGetValue(prmJson, ["armBound"])
    && !(integer)llJsonGetValue(prmJson, ["armBoundExternal"]);
  _isLegsBound = (integer)llJsonGetValue(prmJson, ["legBound"]);
  _isGagged = (integer)llJsonGetValue(prmJson, ["gagged"]);
  _isBlindfolded = is_set(llJsonGetValue(prmJson, ["slots", "blindfold"]));
  _isHandBound = is_set(llJsonGetValue(prmJson, ["slots", "hand"]));
  _isOtherBound = is_set(llJsonGetValue(prmJson, ["slots", "crotch"])) || is_set(llJsonGetValue(prmJson, ["slots", "hand"]));

  _owner = llJsonSetValue(_owner, ["armBound"], (string)_isArmsBound);
  _owner = llJsonSetValue(_owner, ["handBound"], (string)_isHandBound);
}

integer get_next_level_exp() {
  integer tmpLevel = get_user_level();
  if (tmpLevel == 0) {
    return 0;
  }

  return (integer)(tmpLevel * 50 + llPow(tmpLevel,3));
}

integer get_user_level() {
  return llGetListLength(_ownerFeats);
}

set_available_poses(string prmPoses) {
  _legPoses = llJson2List(prmPoses);
  _legPoses += get_poseball_pose_list();
}

set_exp(integer exp) {
  _ownerExp = exp;
  _owner = llJsonSetValue(_owner, ["exp"], (string)exp);
}

set_feats(list feats) {
  _ownerFeats = llListSort(feats, 1, TRUE);
  _owner = llJsonSetValue(_owner, ["feats"], llList2Json(JSON_ARRAY, feats));
  simple_request("set_owner", _owner);
}

add_exp(string prmValue) {
  debug("Earned Experience! " + prmValue);
  integer experience = _ownerExp;
  integer addValue = (integer)prmValue;
  if (addValue > 0) { experience += addValue; }
  set_exp(experience);
}

add_feat(string feat) {
  _ownerFeats += feat;
  set_feats(_ownerFeats);
}

integer can_level_up() {
  return get_next_level_exp() <= _ownerExp
    && get_user_level() < llGetListLength(_feats);
}

list require_prerequisite_feat(list feats, string feat, string prerequisite) {
  integer featIndex = llListFindList(feats, [feat]);
  integer preIndex = llListFindList(feats, [prerequisite]);
  if (preIndex != -1 && featIndex != -1) {
    feats = llDeleteSubList(feats, featIndex, featIndex);
  }
  return feats;
}

// ===== Event Controls =====
execute_function(string prmFunction, string prmJson) {
  string value = llJsonGetValue(prmJson, ["value"]);
  if (JSON_INVALID == value) {
    debug("Invalid value in JSON: " + prmJson);
    return;
  }

  if (prmFunction == "gui_owner") {
    key userkey = (key)llJsonGetValue(prmJson, ["userkey"]);
    init_gui(userkey, (integer)value);
  }
  else if (prmFunction == "get_owner") { simple_request("set_owner", _owner); }
  else if (prmFunction == "set_villain") { _villainKey = (key)llJsonGetValue(value, ["key"]); }
  else if (prmFunction == "set_poses") { set_available_poses(llJsonGetValue(value, ["leg"])); }
  else if (prmFunction == "add_exp") { add_exp(value); }
  else if (prmFunction == "set_restraints") { set_restraints(value); }
  else if (prmFunction == "reset_gui") {
    exit("");
  }
}

default
{
  state_entry() {
    init();
  }

  on_rez(integer start_param) {
    init();
  }

  dataserver(key queryID, string data) {
    if (queryID == _statsQueryID) {
      string exp = llJsonGetValue(data, ["exp"]);
      string feats = llJsonGetValue(data, ["feats"]);
      if (is_set(exp)) { set_exp((integer)exp); }
      if (is_set(feats)) { set_feats(llJson2List(feats)); }
    }
  }

  listen(integer prmChannel, string prmName, key prmID, string prmText) {
    if (prmChannel = _guiChannel) {
      if (prmText == "<<Done>>") { exit("done"); return; }
      else if (prmText == "<<Back>>") { _guiPage = 0; gui(_guiScreenLast); return;}
      else if (prmText == "Next >>") { _guiPage ++; gui(_guiScreen); return; }
      else if (prmText == "<< Previous") { _guiPage --; gui(_guiScreen); return; }
      else if (prmText == " ") { gui(_guiScreen); }

      if (_guiScreen == GUI_HOME) {
        if (prmText == "Bind") {
          simple_request("set_villain", _owner);
          gui_request("gui_bind", FALSE, _guiUserKey, 0);
          return;
        }
        else if (prmText == "Escape") { gui_request("gui_escape", FALSE, _guiUserKey, 0); return; }
        else if (prmText == "Stats") { gui(GUI_STATS); }
        else if (prmText == "Options") { gui(GUI_OPTIONS); }
        else if (prmText == "Pose") { gui(GUI_POSE); }
      } else if (_guiScreen == GUI_STATS) {
        if (prmText == "Export") {
          string export;
          export = llJsonSetValue(export, ["exp"], (string)_ownerExp);
          export = llJsonSetValue(export, ["feats"], llList2Json(JSON_ARRAY, _ownerFeats));
          llOwnerSay(export);
        } else {
          add_feat(prmText);
        }
        gui(_guiScreen);
      } else if (_guiScreen == GUI_OPTIONS) {
        if (prmText == "☒ RP Mode") { _rpMode = TRUE; simple_request("set_rpMode", "1"); }
        else if (prmText == "☑ RP Mode") { _rpMode = FALSE; simple_request("set_rpMode", "0"); }
        else if (prmText == "☒ RLV") { _RLV = TRUE; simple_request("set_rlv", "1"); }
        else if (prmText == "☑ RLV") { _RLV = FALSE; simple_request("set_rlv", "0"); }
        else if (prmText == "☒ Lockable") { _lockable = TRUE; simple_request("set_lockable", "1"); }
        else if (prmText == "☑ Lockable") { _lockable = FALSE; simple_request("set_lockable", "0"); }
        else if (prmText == "☒ Arms") { _allowArm = TRUE; simple_request("set_allowArm", "1"); }
        else if (prmText == "☑ Arms") { _allowArm = FALSE; simple_request("set_allowArm", "0"); }
        else if (prmText == "☒ Legs") { _allowLeg = TRUE; simple_request("set_allowLeg", "1"); }
        else if (prmText == "☑ Legs") { _allowLeg = FALSE; simple_request("set_allowLeg", "0"); }
        else if (prmText == "☒ Gags") { _allowGag = TRUE; simple_request("set_allowGag", "1"); }
        else if (prmText == "☑ Gags") { _allowGag = FALSE; simple_request("set_allowGag", "0"); }
        else if (prmText == "☒ Mittens") { _allowMitten = TRUE; simple_request("set_allowMitten", "1"); }
        else if (prmText == "☑ Mittens") { _allowMitten = FALSE; simple_request("set_allowMitten", "0"); }
        else if (prmText == "☒ Blindfolds") { _allowBlindfold = TRUE; simple_request("set_allowBlindfold", "1"); }
        else if (prmText == "☑ Blindfolds") { _allowBlindfold = FALSE; simple_request("set_allowBlindfold", "0"); }
        else if (prmText == "☒ Feat Skip") { _featSkip = TRUE; simple_request("set_featSkip", "1"); }
        else if (prmText == "☑ Feat Skip") { _featSkip = FALSE; simple_request("set_featSkip", "0"); }
        gui(_guiScreen);
      } else if (_guiScreen == GUI_POSE) {
        simple_request("set_leg_pose", prmText);
        gui(_guiScreen);
      }
    }
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function;
    string value;

    if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
      debug(prmText);
      return;
    }
    execute_function(function, prmText);

    if (function == _resumeFunction) {
      _resumeFunction = "";
      init_gui(_guiUserKey, _guiScreen);
    }
  }

  timer() {
    exit("timeout");
  }
}
