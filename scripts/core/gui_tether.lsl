#include "includes/gui_tools.lsl"
#include "includes/restraint_tools.lsl"

integer CHANNEL_LOCKMEISTER = -8888;
vector COLOR_WHITE = <1.00, 1.00, 1.00>;

integer _lockmeisterListener;
key _armTargetID;
key _legTargetID;

integer GUI_ARM = 0;
integer GUI_LEG = 10;

// Status
integer _armTetherLength = 1;
integer _legTetherLength = 1;

string requestingAttachment;

init() {
  GUI_TIMEOUT = 60;  // Extend timeout for leash

  if (_lockmeisterListener) { llListenRemove(_lockmeisterListener); }
  _lockmeisterListener = llListen(CHANNEL_LOCKMEISTER, "", NULL_KEY, "");
}

init_gui(key prmID, integer prmScreen) {
  _guiUserKey = prmID;

  if (_guiListener) { llListenRemove(_guiListener); }
  _guiChannel = (integer)llFrand(-9998) - 1;
  _guiListener = llListen(_guiChannel, "", _guiUserKey, "");
  gui(prmScreen);
}

refresh_tether(string prmAttachment) {
  if ("arm" == prmAttachment) {
    if (is_set(_armTargetID) && is_arm_bound()) {
      tether_to(_armTargetID, "arm");
      return;
    }
    tether_to(NULL_KEY, "arm");
  } else if ("leg" == prmAttachment) {
    if (is_set(_legTargetID) && is_leg_bound()) {
      tether_to(_legTargetID, "leg");
      return;
    }
    tether_to(NULL_KEY, "leg");
  }
}

request_hitch(string prmAttachment) {
  requestingAttachment = prmAttachment;
  llRegionSayTo(_guiUserKey, 0, "Please select a hitching post.");
  llListenControl(_lockmeisterListener, TRUE);
}

tether_to(key prmTargetID, string prmAttachment) {
  string request = "";
  request = llJsonSetValue(request, ["targetID"], prmTargetID);
  request = llJsonSetValue(request, ["attachment"], prmAttachment);
  request = llJsonSetValue(request, ["component"], "ANY");
  request = llJsonSetValue(request, ["userKey"], (string)llGetOwner());
  if (prmAttachment == "arm") {
    request = llJsonSetValue(request, ["length"], (string)_armTetherLength);
    _armTargetID = prmTargetID;
  } else if (prmAttachment == "leg") {
    request = llJsonSetValue(request, ["length"], (string)_legTetherLength);
    _legTargetID = prmTargetID;
  }

  simple_attached_request("tether_to", request);
  simple_request("tether_to", request);
}

set_arm_tether_length(string prmLength) {
  _armTetherLength = (integer)llDeleteSubString(prmLength, -1, 2);
  tether_to(_armTargetID, "arm");
}

set_leg_tether_length(string prmLength) {
  _legTetherLength = (integer)llDeleteSubString(prmLength, -1, 2);
  tether_to(_legTargetID, "leg");
}

gui(integer prmScreen) {
  // Reset Busy Clock
  llSetTimerEvent(GUI_TIMEOUT);

  string btn10 = " ";     string btn11 = " ";     string btn12 = " ";
  string btn7 = " ";      string btn8 = " ";      string btn9 = " ";
  string btn4 = " ";      string btn5 = " ";      string btn6 = " ";
  string btn1 = "<<Back>>";   string btn2 = "<<Done>>";   string btn3 = " ";

  _guiText = " ";

  // Arm Tether
  if (prmScreen == GUI_ARM) {
    _guiText = "How do you want to tether " + llGetDisplayName(llGetOwner()) + "'s arms?\nCurrent length: " + (string)_armTetherLength + "m";
  } else if (prmScreen == GUI_LEG) {
    _guiText = "How do you want to tether " + llGetDisplayName(llGetOwner()) + "'s legs?\nCurrent length: " + (string)_legTetherLength + "m";
  }

  btn7 = "1m";
  btn8 = "2m";
  btn9 = "3m";
  btn10 = "5m";
  btn11 = "8m";
  btn12 = "10m";

  btn4 = "Release";
  btn5 = "Grab";
  btn6 = "Hitch";
  btn3 = "Pull";

  _guiScreen = prmScreen;
  _guiButtons = [btn1, btn2, btn3];

  if (btn4+btn5+btn6 != "   ") { _guiButtons += [btn4, btn5, btn6]; }
  if (btn7+btn8+btn9 != "   ") { _guiButtons += [btn7, btn8, btn9]; }
  if (btn10+btn11+btn12 != "   ") { _guiButtons += [btn10, btn11, btn12]; }
  llDialog(_guiUserKey, _guiText, _guiButtons, _guiChannel);
}

execute_function(string function, string prmJson) {
  string value = llJsonGetValue(prmJson, ["value"]);

  if (function == "gui_tether_arm" || function == "gui_tether_leg") {
    key userkey = (key)llJsonGetValue(prmJson, ["userkey"]);
    integer screen = 0;
    if (function == "gui_tether_leg") {
      screen = 10;
    }
    init_gui(userkey, screen);
  } else if (function == "request_tether") {
    refresh_tether(llJsonGetValue(value, ["attachment"]));
  } else if (function == "set_restraints") {
    _restraints = value;
  } else if (function == "reset_gui") {
    exit("");
  }
}

default
{
  state_entry() {
    init();
  }

  listen(integer prmChannel, string prmName, key prmID, string prmText) {
    if (prmChannel == CHANNEL_LOCKMEISTER) {

      // Check if post is okay
      if (llGetSubString(prmText,-2,-1) != "ok") { return; }
      if (llGetSubString(prmText, 0, 35) != (string)_guiUserKey ) { return; }

      llListenControl(_lockmeisterListener, FALSE);
      tether_to(prmID, requestingAttachment);

      gui(_guiScreen);
    } else if (prmChannel == _guiChannel) {
      if (prmText == "<<Done>>") { exit("done"); return; }
      else if (prmText == " ") { gui(_guiScreen); return; }
      else if (prmText == "<<Back>>") { gui_request("gui_bind", TRUE, _guiUserKey, 0); return; }

      if (_guiScreen == GUI_ARM || _guiScreen == GUI_LEG) {
        string restraintType = "arm"; // Default
        if (_guiScreen == GUI_LEG) { restraintType = "leg"; }

        if (prmText == "Grab") { tether_to(_guiUserKey, restraintType); }
        else if (prmText == "Release") { tether_to(NULL_KEY, restraintType); }
        else if (prmText == "Hitch") { request_hitch(restraintType); return; }
        else if (prmText == "Pull") { simple_request("tetherPull", _guiUserKey); }
        else {
          if (GUI_ARM == _guiScreen) { set_arm_tether_length(prmText); }
          else if (GUI_LEG == _guiScreen) { set_leg_tether_length(prmText); }
        }
        gui(_guiScreen);
      }
    }
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function;

    if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
      debug(prmText);
      return;
    }
    execute_function(function, prmText);
  }

  timer() {
    llListenControl(_lockmeisterListener, FALSE);
    exit("timeout");
  }
}
