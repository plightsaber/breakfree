#include "includes/general_tools.lsl"
#include "includes/gui_tools.lsl"
#include "includes/restraint_tools.lsl"
#include "includes/user_lib.lsl"

float TOUCH_BOUND_MAX_DISTANCE = 1.5;
float TOUCH_MAX_DISTANCE = 3.0;
float TOUCH_TIMEOUT = 3.0;

// Quick Keys
key _activeKey = NULL_KEY;
key _toucherKey;
key _villainKey;

string _owner;

// Settings
integer _rpMode = FALSE;

// ==== Initializer =====
init() {
  _activeKey = NULL_KEY;
}

init_gui_request(string prmGUI,integer prmRestore, key prmUser, integer prmScreen) {
  key userKey = (key)llJsonGetValue(prmUser, ["key"]);

  simple_request("reset_gui", "init");
  simple_request("set_toucher", prmUser);

  if (is_set(_activeKey) && (_activeKey != userKey)) {
    llRegionSayTo(_activeKey, 0, get_name() + "'s menu has been claimed by " + (string)llJsonGetValue(prmUser, ["name"]) + ".");
  }

  if ((("gui_bind" == prmGUI) && (_activeKey != userKey))) {
    llOwnerSay((llJsonGetValue(prmUser,["name"]) + " is eyeing you suspiciously."));
    simple_request("set_villain", prmUser);
    _villainKey = (key)llJsonGetValue(prmUser, ["key"]);
  }

  _activeKey = userKey;
  gui_request(prmGUI, prmRestore, userKey, prmScreen);
}

init_touch(key prmUserKey) {
  if (!can_touch(prmUserKey)) {
    if (prmUserKey == llGetOwner()) {
      llRegionSayTo(prmUserKey, 0, "You cannot do anything because you are being controlled by " + llGetDisplayName(_activeKey) + ".");
    } else {
      llRegionSayTo(prmUserKey, 0, "You cannot do anything because " + llGetDisplayName(llGetOwner()) + " is being controlled by " + llGetDisplayName(_activeKey) + ".");
    }

    return;
  }

  if (prmUserKey == llGetOwner()) {
    init_gui_request("gui_owner", 0, _owner, 0);
    return;
  }

  _toucherKey = prmUserKey;
  api_request(_toucherKey, llGetOwner(), "get_touch_info", "");
  llSetTimerEvent(TOUCH_TIMEOUT);
}

integer can_bind(string prmUser) {
  key userKey = (key)llJsonGetValue(prmUser, ["key"]);

  if (_rpMode) {
    return TRUE;  // Always can bind in RP mode.
  }

  if ((integer)llJsonGetValue(prmUser, ["armBound"])) {
    return FALSE; // Can't bind if toucher is bound.
  }

  if (userKey == llGetOwner()) {
    return FALSE; // Owner never goes directly to bound menu.
  }

  if (userKey != _villainKey && _villainKey != NULL_KEY) {
    return FALSE; // Only original villain can access at this point - or if there was no villain involved.
  }

  return TRUE;
}

integer can_touch(key prmUserKey) {
  if (!is_set(_activeKey)) {
    return TRUE;
  }

  if (llGetOwner() == _activeKey) {
    return TRUE;
  }

  if (prmUserKey == _activeKey) {
    return TRUE;
  }

  if (_villainKey == _activeKey) {
    return FALSE;
  }

  return TRUE;
}

touch_user(string prmUser) {
  //debug("touch_user Event");
  llSetTimerEvent(0.0);  // Stop Timer

  // Is the toucher in a reasonable range?
  key userKey = (key)llJsonGetValue(prmUser, ["key"]);
  vector toucherPos = llList2Vector(llGetObjectDetails(userKey, [OBJECT_POS]), 0);

  integer toucherDistance = llAbs(llFloor(llVecDist(toucherPos, llGetPos())));
  integer toucherBound = (integer)llJsonGetValue(prmUser, ["armBound"]);

  float checkDistance;
  if (toucherBound) { checkDistance = TOUCH_BOUND_MAX_DISTANCE; }
  else { checkDistance = TOUCH_MAX_DISTANCE; }

  if (toucherDistance > TOUCH_MAX_DISTANCE) {
    // TODO: Get preferred username
    llRegionSayTo(userKey, 0, llGetDisplayName(llGetOwner()) + " is too far away.");
    return;
  }

  if (toucherBound && !is_bound()) {
    llRegionSayTo(userKey, 0, "You can't do that while bound.");
    return;
  }

  simple_request("reset_gui", "override");

  if (is_bound()) {
    if (can_bind(prmUser)) {
      init_gui_request("gui_bind", FALSE, prmUser, 0);
      return;
    }

    init_gui_request("gui_escape", FALSE, prmUser, 0);
    return;
  }

  init_gui_request("gui_bind", FALSE, prmUser, 0);
}

default {
  state_entry() {
    init();
  }

  touch_start(integer prmCount) {
    key toucherKey = llDetectedKey(0);
    init_touch(toucherKey);
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function;
    string value;

    if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
      debug(prmText);
      return;
    }
    value = llJsonGetValue(prmText, ["value"]);

    if (function == "touch") { init_touch(value); }
    else if (function == "touch_user") { touch_user(value); }
    else if (function == "set_owner") { _owner = value; }
    else if (function == "set_restraints") { _restraints = value; }
    else if (function == "set_villain") { _villainKey = (key)llJsonGetValue(value, ["key"]); }
    else if (function == "set_rpMode") { _rpMode = (integer)value; }
    else if (function == "reset_gui") {
      if (value == "timeout") {
        llRegionSayTo(_activeKey, 0, llGetDisplayName(llGetOwner()) + "'s menu has timed out.");
        _activeKey = NULL_KEY;
      } else if (value != "override") {
        _activeKey = NULL_KEY;
      }

      if (!is_bound()) { _villainKey = NULL_KEY; }
    }
  }

  timer() {
    touch_user(get_default_user(_toucherKey));
  }
}
