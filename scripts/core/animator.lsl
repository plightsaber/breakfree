#include "includes/contrib_lib.lsl"
#include "includes/general_tools.lsl"
#include "includes/restraint_tools.lsl"
#include "includes/pose_lib.lsl"

string _self;  // JSON object

float ANIM_REFRESH_INTERVAL = 0.2;

list _animations; // Other animations set by restraints. (ie. mitten hands)
string _animationArmBase;
string _animationArmSuccess;
string _animationArmFailure;

string _animationLegBase;
string _animationLegSuccess;
string _animationLegFailure;
string _animationLegWalk;

string _animationMoverCurrent; // The mover should only be able to use one animation at a time.  The previous animation is always stopped first.

integer _mouthOpen = FALSE;

list _armPoses;  // A list of valid poses for the current restraint
string _armPose; // The currently set arm pose

list _legPoses;
string _legPose;

init() {
  refresh_animations();
  llSetTimerEvent(ANIM_REFRESH_INTERVAL);
}

set_arm_pose(string prmUID) {
   llRequestPermissions(llGetOwner(), PERMISSION_TRIGGER_ANIMATION);
  if (!is_set(prmUID)) {
    return; // Just keep on truckin'
  }

  if (is_set(_animationArmBase)) { llStopAnimation(_animationArmBase); }

  if (prmUID == "free" || prmUID == "external") {
    _armPose = JSON_NULL;
    _animationArmBase = JSON_NULL;
    return;
  }

  _armPose = prmUID;
  _animationArmBase = llJsonGetValue(get_poses(), ["arm", prmUID, "base"]);
  //_animationArmSuccess = llJsonGetValue(get_poses(), ["arm", "prmUID", "struggleSuccess"]);
  //_animationArmFailure = llJsonGetValue(get_poses(), ["arm", "prmUID", "struggleFailure"]);
  _animationArmSuccess = "animArm_struggle";
  _animationArmFailure = "animArm_struggle";

  refresh_animations();
}

set_leg_pose(string prmUID) {
   llRequestPermissions(llGetOwner(), PERMISSION_TRIGGER_ANIMATION | PERMISSION_OVERRIDE_ANIMATIONS);
  if (!is_set(prmUID)) {
    return; // Just keep on truckin'
  }

  string pose = llJsonGetValue(_poseLib, ["leg", _legPose]);

  if (is_set(_animationLegBase)) { llStopAnimation(_animationLegBase); }

  if (prmUID == "free" || prmUID == "external") {
    _legPose = JSON_NULL;
    _animationLegBase = JSON_NULL;
    llResetAnimationOverride("ALL");
    return;
  }

  _legPose = prmUID;
  _animationLegBase = llJsonGetValue(get_poses(), ["leg", prmUID, "base"]);
  _animationLegSuccess = llJsonGetValue(get_poses(), ["leg", prmUID, "success"]);
  _animationLegFailure = llJsonGetValue(get_poses(), ["leg", prmUID, "failure"]);
  _animationLegWalk = llJsonGetValue(get_poses(), ["leg", prmUID, "walk"]);

  // Override walking animation
  if (is_set(_animationLegWalk) && llGetAnimationOverride("Walking") != _animationLegWalk) {
    llSetAnimationOverride("Walking", _animationLegWalk);
  }

  refresh_animations();
}


set_arm_poses(string prmPoses) {
  _armPoses = llJson2List(prmPoses);
  if (llListFindList(_armPoses, [_armPose]) == -1) {
    set_arm_pose(llList2String(_armPoses, 0));
  }
}

set_leg_poses(string prmPoses) {
  _legPoses = llJson2List(prmPoses);

  // Add poseBall overrides as valid poses unless freed
  if (llListFindList(_legPoses, ["free"]) == -1) {
    _legPoses += get_poseball_pose_list();
  }

  // Remove current pose and set to default if current pose no longer valid
  if (llListFindList(_legPoses, [_legPose]) == -1) {
    set_leg_pose(llList2String(_legPoses, 0));
  }
}

set_restraints(string prmJson) {
  _mouthOpen = llJsonGetValue(prmJson, ["mouthOpen"]) == "1";

  list newAnimations = llJson2List(llJsonGetValue(prmJson,["animations"]));
  list stopAnimations = ListXnotY(_animations,newAnimations);

  integer index;
  for (index = 0; index < llGetListLength(stopAnimations); index++) {
    llStopAnimation(llList2String(stopAnimations, index));
  }

  _animations = newAnimations;
  refresh_animations();

}

// ===== Main Functions =====
animate(string prmAnimation) {
  llRequestPermissions(llGetOwner(), PERMISSION_TRIGGER_ANIMATION);
  string animation;

  if (prmAnimation == "animation_arm_success") { animation = _animationArmSuccess; }
  else if (prmAnimation == "animation_arm_failure") { animation = _animationArmFailure; }
  else if (prmAnimation == "animation_leg_success") { animation = _animationLegSuccess; }
  else if (prmAnimation == "animation_leg_failure") { animation = _animationLegFailure; }

  if (is_set(animation)) { llStartAnimation(animation); }
}

animate_loop(string animation)
{
  if (llGetInventoryKey(animation) == NULL_KEY) {
    debug("Could not find animation: " + animation);
    return;
  }

  llRequestPermissions(llGetOwner(), PERMISSION_TRIGGER_ANIMATION);
  llStartAnimation(animation);
  llSetTimerEvent(2.0);
}

animate_mover(string prmAnimation) {
  llRequestPermissions(llGetOwner(), PERMISSION_TRIGGER_ANIMATION | PERMISSION_OVERRIDE_ANIMATIONS);

  if (is_set(_animationMoverCurrent)) { llStopAnimation(_animationMoverCurrent); }

  if (prmAnimation == "animation_walk_forward") {
    llResetAnimationOverride("ALL");
    _animationMoverCurrent = _animationLegWalk;
  }
  else {
    // Set override ONLY when not already walking.  Needed for tether animation, but does bad things for slow movement speeds
    if (is_set(_animationLegWalk) && llGetAnimationOverride("Walking") != _animationLegWalk) {
      llSetAnimationOverride("Walking", _animationLegWalk);
    }
    _animationMoverCurrent = JSON_NULL;
  }

  if (is_set(_animationMoverCurrent)) { llStartAnimation(_animationMoverCurrent); }
}

refresh_animations() {
  llRequestPermissions(llGetOwner(), PERMISSION_TRIGGER_ANIMATION);

  if (is_set(_animationArmBase)) { animate_loop(_animationArmBase); }
  if (is_set(_animationLegBase)) { animate_loop(_animationLegBase); }

  // Mouth
  if (_mouthOpen) {
    llStartAnimation("express_open_mouth");
    animate_loop("animOpenMouthBento");
  } else {
    llStopAnimation("express_open_mouth");
    llStopAnimation("animOpenMouthBento");
  }

  // Other animations
  integer index;
  for (index = 0; index < llGetListLength(_animations); index++) {
    animate_loop(llList2String(_animations, index));
  }
}

// ===== Event Controls =====
default
{
  state_entry() {
    init();
  }

  on_rez(integer prmStart) {
    init();
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function;
    string value;

    if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
        debug(prmText);
        return;
    }
    value = llJsonGetValue(prmText, ["value"]);

    if (function == "set_arm_pose") { set_arm_pose(value); }
    else if (function == "animate") { animate(value); }
    else if (function == "animate_mover") { animate_mover(value); }
    else if (function == "set_poses") {
      set_arm_poses(llJsonGetValue(value, ["arm"]));
      set_leg_poses(llJsonGetValue(value, ["leg"]));
    }
    else if (function == "set_leg_pose") { set_leg_pose(value); }
    else if (function == "set_restraints") { set_restraints(value); }
  }

  timer() {
    llSetTimerEvent(ANIM_REFRESH_INTERVAL);
    refresh_animations();
  }

}
