#include "includes/general_tools.lsl"
#include "includes/dictionary/chat_channels.lsl"

integer _gagChatListener;
integer _emoteChatListener;

integer _gagged;
integer _mouthOpen;
integer _mouthGarbled;
integer _mouthStuffed;
integer _mouthSealed;

string _currentRestraints;

init() {
  // Define listeners
  if (_gagChatListener) { llListenRemove(_gagChatListener); }
  _gagChatListener = llListen(CHANNEL_GAGCHAT, "", llGetOwner(), "");

  if (_emoteChatListener) { llListenRemove(_emoteChatListener); }
  _emoteChatListener = llListen(CHANNEL_GAGEMOTE, "", llGetOwner(), "");

  // Enable garbling if required
  if (_gagged) {
    enable_garbling(TRUE);
  }
}

enable_garbling(integer toggle) {
  llListenControl(_gagChatListener, toggle);
  llListenControl(_emoteChatListener, toggle);
}

set_restraints(string prmJson) {
  _currentRestraints = prmJson;
  _gagged = is_set(llJsonGetValue(_currentRestraints, ["gagged"]));

  // Set garbling variables
  _mouthOpen = (is_set(llJsonGetValue(_currentRestraints, ["mouthOpen"])));
  _mouthGarbled = (is_set(llJsonGetValue(_currentRestraints, ["speechGarbled"])));
  _mouthStuffed = (is_set(llJsonGetValue(_currentRestraints, ["speechMuffled"])));
  _mouthSealed = (is_set(llJsonGetValue(_currentRestraints, ["speechSealed"])));

  if (!_gagged) {
    if (_mouthOpen || _mouthGarbled || _mouthStuffed || _mouthSealed) {
      debug("WARNING: No gag detected, garbling rules still enabled.");
    }

    enable_garbling(FALSE);
    return;
  }

  if (!_mouthOpen && !_mouthGarbled && !_mouthStuffed && !_mouthSealed) {
    debug("WARNING: Gag detected, no garbling rules.");
  }

  enable_garbling(TRUE);
}

convert_emote(string prmEmote) {
  string strNew = "";
  integer intMessageLength = llStringLength(prmEmote);
  integer intChar;
  string char;
  integer isUpper;

  integer activeGarble = FALSE;

  for (intChar = 0; intChar < intMessageLength; intChar++) {
    char = llGetSubString(prmEmote, intChar, intChar);
    if (char == "\"") {
      activeGarble = !activeGarble;
    } else if (activeGarble) {
      char = garble_char(char);
    }
    strNew = strNew + char;
  }

  // Set name to speaker
  string object_name = llGetObjectName();
  llSetObjectName(llGetDisplayName(llGetOwner())); // TODO: Get Name
  llWhisper(0, strNew);
  llSetObjectName(object_name);
}

string garble_char(string char) {
  integer isUpper;

  if (!_mouthStuffed) { isUpper = llToLower(char) != char; }
  else { isUpper = FALSE; }
  char = llToLower(char);

  if (_mouthOpen) {
    if (char == "b") char = "";
    else if (char == "d") char = "e";
    else if (char == "f") char = "h";
    else if (char == "j") char = "y";
    else if (char == "l") char = "h";
    else if (char == "p") char = "h";
    else if (char == "q") char = "k";
    else if (char == "s") char = "h";
    else if (char == "t") char = "h";
    else if (char == "v") char = "w";
    else if (char == "x") char = "k";
    else if (char == "z") char = "";
  }
  if (_mouthGarbled) {
    if (char == "c") char = "h";
    else if (char == "r") char = "h";
    else if (char == "g") char = "n";
    else if (char == "k") char = "ng";
    else if (char == "n") char = "n";
  }

  if (_mouthSealed) {
    if (char == "a") char = "m";
    else if (char == "e") char = "m";
    else if (char == "i") char = "n";
    else if (char == "o") char = "m";
    else if (char == "u") char = "m";
    else if (char == "y") char = "n";
  }

  if (_mouthOpen && _mouthGarbled && _mouthSealed && _mouthStuffed) {
    if (llRound(llFrand(2)) == 0) {
      char = "";
    } else if (char == "!") { char = "m"; }
  }

  if (isUpper) { char = llToUpper(char); }

  return char;
}

convert_speech(string prmChat) {
  string strNew = "";
  integer intMessageLength = llStringLength(prmChat);
  integer intChar;
  string char;

  for (intChar = 0; intChar < intMessageLength; intChar++) {
    // Get information about char
    char = llGetSubString(prmChat, intChar, intChar);
    strNew = strNew + garble_char(char);
  }

  // Set name to speaker
  string object_name = llGetObjectName();
  llSetObjectName(llGetDisplayName(llGetOwner())); // TODO: Get Name
  llWhisper(0, strNew);
  llSetObjectName(object_name);
}

default
{
  state_entry() {
    init();
  }

  on_rez(integer prmStart) {
    init();
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function;
    string value;

    if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
      debug(prmText);
      return;
    }
    value = llJsonGetValue(prmText, ["value"]);

    if (function == "set_restraints") { set_restraints(value); }
  }

  listen(integer prmChannel, string prmName, key senderID, string prmMessage) {
    if (prmChannel == CHANNEL_GAGCHAT) {
      convert_speech(prmMessage);
    } else if (prmChannel == CHANNEL_GAGEMOTE) {
      convert_emote(prmMessage);
    }
  }

}
