#include "includes/color_lib.lsl"
#include "includes/general_tools.lsl"
#include "includes/gui_tools.lsl"
#include "includes/leg_tools.lsl"
#include "includes/texture_lib.lsl"
#include "includes/user_lib.lsl"
#include "includes/dictionary/texture_rope_dictionary.lsl"

integer _rpMode = FALSE;
integer _featSkip = FALSE;

// GUI screens
integer GUI_HOME = 0;
integer GUI_STYLE = 100;
integer GUI_TEXTURE = 101;
integer GUI_COLOR = 111;

// Status
string legsBound = "free";  // 0: FREE; 1: BOUND; 2: HELPLESS

string _villain;

string _currentRestraints;
string _restraintLib;

string get_self() {
  if (_self != "") return _self;

  _self = llJsonSetValue(_self, ["name"], "Rope");
  _self = llJsonSetValue(_self, ["part"], "leg");
  _self = llJsonSetValue(_self, ["type"], "rope");
  _self = llJsonSetValue(_self, ["hasColor"], "1");
  return _self;
}

string get_current_restraints() {
  if (_currentRestraints) {
    return _currentRestraints;
  }

  _currentRestraints = llJsonSetValue(_currentRestraints, ["wrist"], JSON_NULL);
  _currentRestraints = llJsonSetValue(_currentRestraints, ["elbow"], JSON_NULL);
  _currentRestraints = llJsonSetValue(_currentRestraints, ["torso"], JSON_NULL);

  _currentRestraints = llJsonSetValue(_currentRestraints, ["ankle"], JSON_NULL);
  _currentRestraints = llJsonSetValue(_currentRestraints, ["knee"], JSON_NULL);
  _currentRestraints = llJsonSetValue(_currentRestraints, ["immobilizer"], JSON_NULL);
  _currentRestraints = llJsonSetValue(_currentRestraints, ["crotch"], JSON_NULL);
  return _currentRestraints;
}

init() {
  if (!is_set(_currentColors)) {
    set_color(COLOR_WHITE, "rope");
  }
  if (!is_set(_currentTextures)) {
    set_texture(TEXTURE_ROPE_BRAID, "rope");
  }
}

init_gui(key prmID, integer prmScreen) {
  _guiUserKey = prmID;

  if (_guiListener) { llListenRemove(_guiListener); }
  _guiChannel = (integer)llFrand(-9998) - 1;
  _guiListener = llListen(_guiChannel, "", _guiUserKey, "");
  gui(prmScreen);
}

gui(integer prmScreen) {
  // Reset Busy Clock
  llSetTimerEvent(GUI_TIMEOUT);

  string btn10 = " ";   string btn11 = " ";   string btn12 = " ";
  string btn7 = " ";   string btn8 = " ";   string btn9 = " ";
  string btn4 = " ";   string btn5 = " ";   string btn6 = " ";
  string btn1 = "<<Back>>"; string btn2 = "<<Done>>"; string btn3 = " ";

  list mpButtons;
  _guiText = " ";

  // GUI: Main
  if (prmScreen == GUI_HOME) {
    btn3 = "<<Style>>";

    if (!is_set(llJsonGetValue(_currentRestraints, ["ankle"])) && !is_set(llJsonGetValue(_currentRestraints, ["immobilizer"]))) {
      mpButtons += "Ankle";
    } else if (!is_set(llJsonGetValue(_currentRestraints, ["immobilizer"])) && is_set(llJsonGetValue(_currentRestraints, ["ankle"]))) {
      mpButtons += "Free Ankle";
    }

    if (!is_set(llJsonGetValue(_currentRestraints, ["knee"])) && !is_set(llJsonGetValue(_currentRestraints, ["immobilizer"]))) {
      mpButtons += "Knee";
    } else if (!is_set(llJsonGetValue(_currentRestraints, ["immobilizer"])) && is_set(llJsonGetValue(_currentRestraints, ["knee"]))) {
      mpButtons += "Free Knee";
    }

    if (llJsonGetValue(_currentRestraints, ["immobilizer"]) == JSON_NULL
      && llJsonGetValue(_currentRestraints, ["ankle"]) == "ankle_rope"
      && (llJsonGetValue(_currentRestraints, ["torso"]) == "box_rope" || llJsonGetValue(_currentRestraints, ["wrist"]) == "back_rope")
    ) {
      mpButtons += "Hog";
    }

    if ((has_feat(_villain, "Rigger+") || _rpMode || _featSkip)
      && !is_set(llJsonGetValue(_currentRestraints, ["immobilizer"]))
      && "knee_rope" == llJsonGetValue(_currentRestraints, ["knee"])
    ) {
      mpButtons += "Ball";
    }

    if (is_set(llJsonGetValue(_currentRestraints, ["immobilizer"]))) {
      mpButtons += "Release";
    }

    if ((has_feat(_villain, "Sadist") || _rpMode || _featSkip)
      && !is_set(llJsonGetValue(_currentRestraints, ["crotch"]))
    ) {
      mpButtons += "Crotch";
    } else if (is_set(llJsonGetValue(_currentRestraints, ["crotch"]))) {
      mpButtons += "Untie Crotch";
    }

    mpButtons = multipage_gui(mpButtons, 2, _guiPage);
  }

  // GUI: Colorize
  else if (prmScreen == GUI_STYLE) {
    _guiText = "Choose what you want to style.";
    mpButtons = multipage_gui(["Color", "Texture"], 2, _guiPage);
  }
  else if (prmScreen == GUI_COLOR) {
    _guiText = "Choose a color the ropes.";
    mpButtons = multipage_gui(_colors, 3, _guiPage);
  }
  else if (prmScreen == GUI_TEXTURE) {
    _guiText = "Choose a texture for the ropes.";
    mpButtons = multipage_gui(_textures, 3, _guiPage);
  }

  if (prmScreen != _guiScreen) { _guiScreenLast = _guiScreen; }
  _guiScreen = prmScreen;

  _guiButtons = [btn1, btn2, btn3];
  if (btn4+btn5+btn6 != "   ") { _guiButtons += [btn4, btn5, btn6]; }
  if (btn7+btn8+btn9 != "   ") { _guiButtons += [btn7, btn8, btn9]; }
  if (btn10+btn11+btn12 != "   ") { _guiButtons += [btn10, btn11, btn12]; }

  // Load MP Buttons - hopefully the lengths were configured correctly!
  if (llGetListLength(mpButtons)) { _guiButtons += mpButtons; }

  llDialog(_guiUserKey, _guiText, _guiButtons, _guiChannel);
}

// ===== Main Functions =====
string define_restraint(string prmName) {
  string restraint;
  list liPoseStandard = ["stand", "kneel", "sit", "sitLeft", "sitRight", "groundFront", "groundLeft", "groundRight", "groundBack"];

  // Type-specific values
  restraint = llJsonSetValue(restraint, ["name"], prmName);
  restraint = llJsonSetValue(restraint, ["canCut"], "1");
  restraint = llJsonSetValue(restraint, ["canEscape"], "1");
  restraint = llJsonSetValue(restraint, ["canTether"], "0");
  restraint = llJsonSetValue(restraint, ["canUseItem"], "1");
  restraint = llJsonSetValue(restraint, ["type"], llJsonGetValue(get_self(), ["type"]));

  integer complexity;
  integer integrity;
  integer tightness;

  if (prmName == "Ankle") {
    complexity = 3;
    integrity = 5;
    tightness = 6;

    restraint = llJsonSetValue(restraint, ["uid"], "ankle_rope");
    restraint = llJsonSetValue(restraint, ["slot"], "ankle");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, liPoseStandard));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, ["leg_rope_ankle"]));
    restraint = llJsonSetValue(restraint, ["canTether"], "1");
  } else if (prmName == "Knee") {
    complexity = 3;
    integrity = 5;
    tightness = 6;

    restraint = llJsonSetValue(restraint, ["uid"], "knee_rope");
    restraint = llJsonSetValue(restraint, ["slot"], "knee");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, liPoseStandard));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, ["leg_rope_knee"]));
  } else if (prmName == "Hog") {
    complexity = 2;
    integrity = 5;
    tightness = 10;

    restraint = llJsonSetValue(restraint, ["uid"], "hog_rope");
    restraint = llJsonSetValue(restraint, ["slot"], "immobilizer");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, ["hogFront", "hogLeft", "hogRight"]));
  } else if (prmName == "Ball") {
    complexity = 4;
    integrity = 5;
    tightness = 15;

    restraint = llJsonSetValue(restraint, ["uid"], "ball_rope");
    restraint = llJsonSetValue(restraint, ["slot"], "immobilizer");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, ["ballLeft", "ballRight"]));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, ["leg_rope_ball"]));
    restraint = llJsonSetValue(restraint, ["preventAttach", JSON_APPEND], "leg_rope_knee");
  } else if (prmName == "Crotch") {
    complexity = 2;
    integrity = 5;
    tightness = 20;

    restraint = llJsonSetValue(restraint, ["uid"], "crotch_rope");
    restraint = llJsonSetValue(restraint, ["slot"], "crotch");
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, ["leg_rope_crotch"]));

    if (has_feat(_villain, "Sadist")) { complexity = complexity + 2; }
  }

  if (has_feat(_villain, "Rigger")) { integrity = integrity+5; }
  if (has_feat(_villain, "Rigger+")) { complexity++; }

  restraint = llJsonSetValue(restraint, ["complexity"], (string)complexity);
  restraint = llJsonSetValue(restraint, ["integrity"], (string)integrity);
  restraint = llJsonSetValue(restraint, ["tightness"], (string)tightness);

  return restraint;
}

send_availability_info() {
  simple_request("add_available_restraint", get_self());
}

set_legs_bound(string prmBound) {
  legsBound = prmBound;
}

// ===== Event Controls =====
execute_function(string prmFunction, string prmJson) {
  string value = llJsonGetValue(prmJson, ["value"]);
  if (JSON_INVALID == value) {
    //return;  // TODO: Rewrite all linked calls to send in JSON
  }

  if (prmFunction == "set_gender") { set_gender(value); }
  else if (prmFunction == "set_restraints") {
    _currentRestraints = llJsonGetValue(value, ["slots"]);
    set_restraints(value);
  }
  else if (prmFunction == "get_available_restraints") { send_availability_info(); }
  else if (prmFunction == "set_rpMode") { _rpMode = (integer)value; }
  else if (prmFunction == "set_featSkip") { _featSkip = (integer)value; }
  else if (prmFunction == "set_villain") { _villain = value; }
  else if (prmFunction == "request_style") {
      if (llJsonGetValue(value, ["attachment"]) != llJsonGetValue(get_self(), ["part"])) { return; }
      if (llJsonGetValue(value, ["name"]) != "rope") { return; }
    string component = llJsonGetValue(value, ["component"]);
    if ("" == component) { component = "rope"; }

    set_color((vector)llJsonGetValue(_currentColors, [component]), component);
    set_texture(llJsonGetValue(_currentTextures, [component]), component);
  }
  else if (prmFunction == "gui_leg_rope") {
    key userkey = (key)llJsonGetValue(prmJson, ["userkey"]);
    integer screen = 0;
    if ((integer)llJsonGetValue(prmJson, ["restorescreen"]) && _guiScreenLast) { screen = _guiScreenLast;}
    init_gui(userkey, screen);
  } else if (prmFunction == "reset_gui") {
    exit("");
  }
  else if (prmFunction == "add_restraint_by_type") {
    if (llJsonGetValue(value, ["type"]) != llJsonGetValue(get_self(), ["type"])) {
      return; // Incorrect type
    }

    if (llJsonGetValue(value, ["part"]) != llJsonGetValue(get_self(), ["part"])) {
      return; // Incorrect type
    }

    string restraintSet;
    restraintSet = llJsonSetValue(restraintSet, ["type"], llJsonGetValue(get_self(), ["part"]));
    restraintSet = llJsonSetValue(restraintSet, ["restraint"], define_restraint(llJsonGetValue(value, ["restraint"])));
    simple_request("add_restraint", restraintSet);
    return;
  }
}

default
{
  state_entry() {
    init();
  }

  listen(integer prmChannel, string prmName, key prmID, string prmText) {
    if (prmChannel = _guiChannel) {
      if (prmText == "<<Done>>") { exit("done"); return; }
      else if (prmText == " ") { gui(_guiScreen); return; }
      else if (prmText == "<<Back>>") {
        if (_guiScreen == GUI_STYLE) { gui(0); return; }
        if (_guiScreen != GUI_HOME) { gui(_guiScreenLast); return;}
        gui_request("gui_bind", TRUE, _guiUserKey, 0);
        return;
      }
      else if (prmText == "<<Back>>") {
        if (_guiScreen != 0) { gui(_guiScreenLast); return;}
        gui_request("gui_bind", TRUE, _guiUserKey, 0);
        return;
      }
      else if (prmText == "Next >>") { _guiPage ++; gui(_guiScreen); return; }
      else if (prmText == "<< Previous") { _guiPage --; gui(_guiScreen); return; }

      if (prmText == "Release") {
        simple_request("remove_restraint", llJsonGetValue(get_self(), ["part"]));
        _resumeFunction = "set_restraints";
        return;
      } else if (prmText == "Free Ankle") {
        simple_request("remove_slot", "ankle");
        _resumeFunction = "set_restraints";
        return;
      } else if (prmText == "Free Knee") {
        simple_request("remove_slot", "knee");
        _resumeFunction = "set_restraints";
        return;
      } else if (prmText == "Untie Crotch") {
        simple_request("remove_slot", "crotch");
        _resumeFunction = "set_restraints";
        return;
      }
      if (_guiScreen == GUI_HOME) {
        if (prmText == "<<Style>>") {
          gui(GUI_STYLE);
          return;
        } else {
          string restraintSet;
          restraintSet = llJsonSetValue(restraintSet, ["type"], llJsonGetValue(get_self(), ["part"]));
          restraintSet = llJsonSetValue(restraintSet, ["restraint"], define_restraint(prmText));
          simple_request("add_restraint", restraintSet);
          _resumeFunction = "set_restraints";
          return;
        }
      } else if (_guiScreen == GUI_STYLE) {
        if ("Color" == prmText) { gui(GUI_COLOR); }
        else if ("Texture" == prmText) { gui(GUI_TEXTURE); }
      } else if (_guiScreen == GUI_COLOR) {
        set_color_by_name(prmText, "rope");
      } else if (_guiScreen == GUI_TEXTURE) {
        set_texture_by_name(prmText, "rope");
      }

      gui(_guiScreen);
      return;
    }
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function;
    string value;

    if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
      debug(prmText);
      return;
    }

    execute_function(function, prmText);

    if (function == _resumeFunction) {
      _resumeFunction = "";
      init_gui(_guiUserKey, _guiScreen);
    }
  }

  timer() {
    exit("timeout");
  }
}
