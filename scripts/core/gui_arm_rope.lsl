#include "includes/arm_tools.lsl"
#include "includes/color_lib.lsl"
#include "includes/general_tools.lsl"
#include "includes/gui_tools.lsl"
#include "includes/restraint_tools.lsl"
#include "includes/texture_lib.lsl"
#include "includes/user_lib.lsl"
#include "includes/dictionary/texture_rope_dictionary.lsl"

integer _rpMode = FALSE;
integer _featSkip = FALSE;

integer GUI_HOME = 0;
integer GUI_STYLE = 100;
integer GUI_COLOR = 101;
integer GUI_TEXTURE = 111;

// Status
string armsBound = "free";  // 0: FREE; 1: BOUND; 2: HELPLESS

string _villain;

string _currentRestraints;
string _restraintLib;

string get_self() {
  if (_self != "") return _self;

  _self = llJsonSetValue(_self, ["name"], "Rope");
  _self = llJsonSetValue(_self, ["part"], "arm");
  _self = llJsonSetValue(_self, ["type"], "rope");
  _self = llJsonSetValue(_self, ["hasColor"], "1");
  return _self;
}

string get_current_restraints() {
  if (_currentRestraints) {
    return _currentRestraints;
  }

  _currentRestraints = llJsonSetValue(_currentRestraints, ["wrist"], JSON_NULL);
  _currentRestraints = llJsonSetValue(_currentRestraints, ["elbow"], JSON_NULL);
  _currentRestraints = llJsonSetValue(_currentRestraints, ["torso"], JSON_NULL);
  return _currentRestraints;
}

init() {
  if (!is_set(_currentColors)) {
    set_color(COLOR_WHITE, "rope");
  }
  if (!is_set(_currentTextures)) {
    set_texture(TEXTURE_ROPE_BRAID, "rope");
  }
}

init_gui(key prmID, integer prmScreen) {
  _guiUserKey = prmID;

  if (_guiListener) { llListenRemove(_guiListener); }
  _guiChannel = (integer)llFrand(-9998) - 1;
  _guiListener = llListen(_guiChannel, "", _guiUserKey, "");
  gui(prmScreen);
}

gui(integer prmScreen) {
  // Reset Busy Clock
  llSetTimerEvent(GUI_TIMEOUT);

  string btn10 = " ";   string btn11 = " ";   string btn12 = " ";
  string btn7 = " ";   string btn8 = " ";   string btn9 = " ";
  string btn4 = " ";   string btn5 = " ";   string btn6 = " ";
  string btn1 = "<<Back>>"; string btn2 = "<<Done>>"; string btn3 = " ";

  list mpButtons;
  _guiText = " ";

  // GUI: Main
  if (prmScreen == 0) {
    btn3 = "<<Style>>";

    if (llJsonGetValue(get_current_restraints(), ["wrist"]) != JSON_NULL
      || llJsonGetValue(_currentRestraints, ["elbow"]) != JSON_NULL
      || llJsonGetValue(_currentRestraints, ["torso"]) != JSON_NULL
    ) {
      mpButtons += "Release";
    }

    if (llJsonGetValue(_currentRestraints, ["wrist"]) == JSON_NULL && llJsonGetValue(_currentRestraints, ["torso"]) == JSON_NULL) {
      if (llJsonGetValue(_currentRestraints, ["elbow"]) == JSON_NULL) { mpButtons += "Front"; }
      mpButtons += "Back";
    }

    if (llJsonGetValue(_currentRestraints, ["elbow"]) == JSON_NULL
      && llJsonGetValue(_currentRestraints, ["torso"]) == JSON_NULL
      && (llSubStringIndex(llJsonGetValue(_currentRestraints, ["wrist"]), "back") != -1 && "back_cuff" != llJsonGetValue(_currentRestraints, ["wrist"]))
    ) {
      mpButtons += "Elbow";
    }

    if (llJsonGetValue(_currentRestraints, ["torso"]) == JSON_NULL && llJsonGetValue(_currentRestraints, ["elbow"]) == JSON_NULL && llJsonGetValue(_currentRestraints, ["wrist"]) == JSON_NULL) {
      mpButtons += "Sides";
    } else if (llJsonGetValue(_currentRestraints, ["torso"]) == JSON_NULL) {
      mpButtons += "Harness";
    }

    if ((has_feat(_villain, "Rigger") || _rpMode || _featSkip)
      && llJsonGetValue(_currentRestraints, ["elbow"]) == JSON_NULL
      && llJsonGetValue(_currentRestraints, ["torso"]) == JSON_NULL
      && llJsonGetValue(_currentRestraints, ["wrist"]) == JSON_NULL
    ) {
      mpButtons += "Box";
    }

    if ((has_feat(_villain, "Rigger+") || _rpMode || _featSkip)
      && llJsonGetValue(_currentRestraints, ["elbow"]) == JSON_NULL
      && llJsonGetValue(_currentRestraints, ["torso"]) == JSON_NULL
      && llJsonGetValue(_currentRestraints, ["wrist"]) == JSON_NULL
    ) {
      mpButtons += "Hammer";
    }

    mpButtons = multipage_gui(mpButtons, 2, _guiPage);
  }

  // GUI: Colorize
  else if (prmScreen == GUI_STYLE) {
    _guiText = "Choose what you want to style.";
    mpButtons = multipage_gui(["Color", "Texture"], 2, _guiPage);
  }
  else if (prmScreen == GUI_COLOR) {
    _guiText = "Choose a color the ropes.";
    mpButtons = multipage_gui(_colors, 3, _guiPage);
  }
  else if (prmScreen == GUI_TEXTURE) {
    _guiText = "Choose a texture for the ropes.";
    mpButtons = multipage_gui(_textures, 3, _guiPage);
  }

  if (prmScreen != _guiScreen) { _guiScreenLast = _guiScreen; }
  _guiScreen = prmScreen;

  _guiButtons = [btn1, btn2, btn3];
  if (btn4+btn5+btn6 != "   ") { _guiButtons += [btn4, btn5, btn6]; }
  if (btn7+btn8+btn9 != "   ") { _guiButtons += [btn7, btn8, btn9]; }
  if (btn10+btn11+btn12 != "   ") { _guiButtons += [btn10, btn11, btn12]; }

  // Load MP Buttons - hopefully the lengths were configured correctly!
  if (llGetListLength(mpButtons)) { _guiButtons += mpButtons; }

  llDialog(_guiUserKey, _guiText, _guiButtons, _guiChannel);
}

// ===== Main Functions =====
string define_restraint(string prmName) {
  string restraint;

  // Type-specific values
  restraint = llJsonSetValue(restraint, ["name"], prmName);
  restraint = llJsonSetValue(restraint, ["canCut"], "1");
  restraint = llJsonSetValue(restraint, ["canEscape"], "1");
  restraint = llJsonSetValue(restraint, ["canTether"], "1");
  restraint = llJsonSetValue(restraint, ["canUseItem"], "1");
  restraint = llJsonSetValue(restraint, ["type"], "rope");

  integer complexity;
  integer integrity;
  integer tightness;

  if (prmName == "Sides") {
    complexity = 2;
    integrity = 5;
    tightness = 5;

    restraint = llJsonSetValue(restraint, ["uid"], "sides_rope");
    restraint = llJsonSetValue(restraint, ["slot"], "torso");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, ["sides"]));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, ["arm_rope_sides"]));
  } else if (prmName == "Front") {
    complexity = 3;
    integrity = 5;
    tightness = 4;

    restraint = llJsonSetValue(restraint, ["uid"], "front_rope");
    restraint = llJsonSetValue(restraint, ["slot"], "wrist");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, ["front"]));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, ["arm_rope_front_wrist"]));
  } else if (prmName == "Back") {
    complexity = 3;
    integrity = 5;
    tightness = 5;

    string pose = "back";
    list liAttachments = ["arm_rope_back_wrist"];
    if (llJsonGetValue(get_current_restraints(), ["elbow"]) != JSON_NULL) {
      pose = "backTight";
      liAttachments = ["arm_rope_backTight_wrist"];
    }

    restraint = llJsonSetValue(restraint, ["uid"], "back_rope");
    restraint = llJsonSetValue(restraint, ["slot"], "wrist");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, [pose]));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, liAttachments));
  } else if (prmName == "Elbow") {
    complexity = 3;
    integrity = 5;
    tightness = 8;

    restraint = llJsonSetValue(restraint, ["uid"], "elbow_rope");
    restraint = llJsonSetValue(restraint, ["slot"], "elbow");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, ["backTight"]));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, ["arm_rope_backTight_elbow"]));
  } else if (prmName == "Harness") {
    complexity = 3;
    integrity = 5;
    tightness = 6;

    list liAttachments;
    list liPoses;
    string wristRestraintId = llJsonGetValue(get_current_restraints(), ["wrist"]);
    if (llJsonGetValue(get_current_restraints(), ["elbow"]) != JSON_NULL
      || wristRestraintId == "back_cuff"
      || wristRestraintId == "back_zip"
    ) {
      liAttachments += "arm_rope_backTight_harness";
    } else if (llSubStringIndex(wristRestraintId, "back") != -1) {
      liAttachments += "arm_rope_back_harness";
    } else if (llSubStringIndex(wristRestraintId, "front") != -1) {
      liAttachments += "arm_rope_front_harness";
    }

    restraint = llJsonSetValue(restraint, ["uid"], "harness_rope");
    restraint = llJsonSetValue(restraint, ["slot"], "torso");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, liPoses));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, liAttachments));
  } else if (prmName == "Box") {
    complexity = 5;
    integrity = 10;
    tightness = 20;

    restraint = llJsonSetValue(restraint, ["uid"], "box_rope");
    restraint = llJsonSetValue(restraint, ["slot"], "torso");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, ["box"]));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, ["arm_rope_box_wrist", "arm_rope_box_harness"]));
  } else if (prmName == "Hammer") {
    complexity = 4;
    integrity = 15;
    tightness = 25;

    restraint = llJsonSetValue(restraint, ["uid"], "hammer_rope");
    restraint = llJsonSetValue(restraint, ["slot"], "torso");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, ["hammer"]));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, ["arm_rope_hammer"]));
  }

  if (has_feat(_villain, "Rigger")) { integrity = integrity+5; }
  if (has_feat(_villain, "Rigger+")) { complexity++; }

  restraint = llJsonSetValue(restraint, ["complexity"], (string)complexity);
  restraint = llJsonSetValue(restraint, ["integrity"], (string)integrity);
  restraint = llJsonSetValue(restraint, ["tightness"], (string)tightness);

  return restraint;
}

send_availability_info() {
  simple_request("add_available_restraint", get_self());
}

set_arms_bound(string prmArmsBound) {
  armsBound = prmArmsBound;
}

// ===== Event Controls =====
execute_function(string prmFunction, string prmJson) {
  string value = llJsonGetValue(prmJson, ["value"]);
  if (JSON_INVALID == value) {
    //return;  // TODO: Rewrite all linked calls to send in JSON
  }

  if (prmFunction == "set_gender") { set_gender(value); }
  else if (prmFunction == "set_restraints") {
    _currentRestraints = llJsonGetValue(value, ["slots"]);
    set_restraints(value);
  }
  else if (prmFunction == "get_available_restraints") { send_availability_info(); }
  else if (prmFunction == "set_rpMode") { _rpMode = (integer)value; }
  else if (prmFunction == "set_featSkip") { _featSkip = (integer)value; }
  else if (prmFunction == "set_villain") { _villain = value; }
  else if (prmFunction == "request_style") {
    if (llJsonGetValue(value, ["attachment"]) != llJsonGetValue(get_self(), ["part"])) { return; }
    if (llJsonGetValue(value, ["name"]) != "rope") { return; }
    string component = llJsonGetValue(value, ["component"]);
    if ("" == component) { component = "rope"; }

    set_color((vector)llJsonGetValue(_currentColors, [component]), component);
    set_texture(llJsonGetValue(_currentTextures, [component]), component);
  }
  else if (prmFunction == "gui_arm_rope") {
    key userkey = (key)llJsonGetValue(prmJson, ["userkey"]);
    integer screen = 0;
    if ((integer)llJsonGetValue(prmJson, ["restorescreen"]) && _guiScreenLast) { screen = _guiScreenLast;}
    init_gui(userkey, screen);
  } else if (prmFunction == "reset_gui") {
    exit("");
  }
  else if (prmFunction == "add_restraint_by_type") {
    if (llJsonGetValue(value, ["type"]) != llJsonGetValue(get_self(), ["type"])) {
      return; // Incorrect type
    }

    if (llJsonGetValue(value, ["part"]) != llJsonGetValue(get_self(), ["part"])) {
      return; // Incorrect type
    }

    string restraintSet;
    restraintSet = llJsonSetValue(restraintSet, ["type"], llJsonGetValue(get_self(), ["part"]));
    restraintSet = llJsonSetValue(restraintSet, ["restraint"], define_restraint(llJsonGetValue(value, ["restraint"])));
    simple_request("add_restraint", restraintSet);
    return;
  }
}

default
{
  state_entry() {
    init();
  }

  listen(integer prmChannel, string prmName, key prmID, string prmText) {
    if (prmChannel = _guiChannel) {
      if (prmText == "<<Done>>") { exit("done"); return; }
      else if (prmText == " ") { gui(_guiScreen); return; }
      else if (prmText == "<<Back>>") {
        if (_guiScreen == GUI_STYLE) { gui(0); return; }
        if (_guiScreen != 0) { gui(_guiScreenLast); return;}
        gui_request("gui_bind", TRUE, _guiUserKey, 0);
        return;
      }
      else if (prmText == "Next >>") { _guiPage ++; gui(_guiScreen); return; }
      else if (prmText == "<< Previous") { _guiPage --; gui(_guiScreen); return; }

      if (prmText == "Release") {
        simple_request("remove_restraint", llJsonGetValue(get_self(), ["part"]));
        _resumeFunction = "set_restraints";
        return;
      }

      if (_guiScreen == 0) {
        if (prmText == "<<Style>>") {
          gui(GUI_STYLE);
          return;
        } else {
          string restraintSet;
          restraintSet = llJsonSetValue(restraintSet, ["type"], llJsonGetValue(get_self(), ["part"]));
          restraintSet = llJsonSetValue(restraintSet, ["restraint"], define_restraint(prmText));
          simple_request("add_restraint", restraintSet);
          _resumeFunction = "set_restraints";
          return;
        }
      } else if (_guiScreen == GUI_STYLE) {
        if ("Color" == prmText) { gui(GUI_COLOR); }
        else if ("Texture" == prmText) { gui(GUI_TEXTURE); }
      } else if (_guiScreen == GUI_COLOR) {
        set_color_by_name(prmText, "rope");
      } else if (_guiScreen == GUI_TEXTURE) {
        set_texture_by_name(prmText, "rope");
      }

      gui(_guiScreen);
      return;
    }
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function;
    string value;

    if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
      debug(prmText);
      return;
    }

    execute_function(function, prmText);

    if (function == _resumeFunction) {
      _resumeFunction = "";
      init_gui(_guiUserKey, _guiScreen);
    }
  }

  timer() {
    exit("timeout");
  }
}