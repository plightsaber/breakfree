#include "includes/pose_lib.lsl"
#include "includes/restraint_tools.lsl"

string _self;  // JSON object
list _legPoses;
string _legPose;

// Tether Variables
key _armTetherID;
key _legTetherID;

float _armTetherLength;
float _legTetherLength;

init() {
  llRequestPermissions(llGetOwner(), PERMISSION_TAKE_CONTROLS | PERMISSION_TRACK_CAMERA);
}

set_restraints(string prmJson) {
  _restraints = prmJson;

  if (!is_arm_bound()) {
    _armTetherID = NULL_KEY;
  }

  if (!is_leg_bound()) {
    _legTetherID = NULL_KEY;
  }

  refresh_sensors();
}

set_leg_pose(string uid, integer sendUpdate) {
  llRequestPermissions(llGetOwner(), PERMISSION_TRIGGER_ANIMATION | PERMISSION_OVERRIDE_ANIMATIONS);
  if (!is_set(uid)) {
    return; // Just keep on truckin'
  }

  if (uid == "free" || uid == "external") {
    _legPose = "";
    llRequestPermissions(llGetOwner(), PERMISSION_TAKE_CONTROLS | PERMISSION_TRACK_CAMERA);
    llReleaseControls();
    return;
  }

  string pose = llJsonGetValue(get_poses(), ["leg", uid]);
  if (!is_set(pose)) {
    return;
  }

  llRequestPermissions(llGetOwner(), PERMISSION_TAKE_CONTROLS | PERMISSION_TRACK_CAMERA);
  llTakeControls(CONTROL_FWD | CONTROL_BACK | CONTROL_RIGHT | CONTROL_LEFT | CONTROL_UP | CONTROL_DOWN, TRUE, FALSE);
  _legPose = uid;

  if (sendUpdate) {
    simple_request("set_leg_pose", _legPose);
  }
}

set_leg_poses(string prmPoses) {
  _legPoses = llJson2List(prmPoses);
  if (llListFindList(_legPoses, [_legPose]) == -1) {
    set_leg_pose(llList2String(_legPoses, 0), FALSE);
  }
}

float get_speedBack() {
  return (float)llJsonGetValue(get_poses(), ["leg", _legPose, "speedBack"])/10;
}

float get_speedFwd() {
  return (float)llJsonGetValue(get_poses(), ["leg", _legPose, "speedFwd"])/10;
}

refresh_sensors() {
  if (is_set(_armTetherID)) {
    llSensorRepeat("", _armTetherID, AGENT|SCRIPTED, _armTetherLength, PI, 0.5);
  } else if (is_set(_legTetherID)) {
    llSensorRepeat("", _legTetherID, AGENT|SCRIPTED, _legTetherLength, PI, 0.5);
  } else {
    llStopMoveToTarget();
    llSensorRemove();
  }
}

tether_to(string prmJson) {
  if (llJsonGetValue(prmJson, ["attachment"]) == "arm") {
    _armTetherID = (key)llJsonGetValue(prmJson, ["targetID"]);
    _armTetherLength = (integer)llJsonGetValue(prmJson, ["length"]);
  } else if (llJsonGetValue(prmJson, ["attachment"]) == "leg") {
    _legTetherID = (key)llJsonGetValue(prmJson, ["targetID"]);
    _legTetherLength = (integer)llJsonGetValue(prmJson, ["length"]);
  }

  refresh_sensors();
}

update_avi_tether_pos(integer force) {
  if (is_set(_armTetherID)) {
    vector armTetherPos = llList2Vector(llGetObjectDetails(_armTetherID, [OBJECT_POS]), 0);
    integer armTetherDistance = llAbs(llFloor(llVecDist(armTetherPos, llGetPos())));
    if (force || armTetherDistance > _armTetherLength) {
      llMoveToTarget(armTetherPos, 0.5);
    }
    else { llStopMoveToTarget(); }
  } else if (is_set(_legTetherID)) {
    vector legTetherPos = llList2Vector(llGetObjectDetails(_legTetherID, [OBJECT_POS]), 0);
    integer legTetherDistance = llAbs(llFloor(llVecDist(legTetherPos, llGetPos())));
    if (force || legTetherDistance > _legTetherLength) {
      llMoveToTarget(legTetherPos, 0.5);
    }
    else { llStopMoveToTarget(); }
  }
}

execute_function(string function, string prmJson) {
  string value = llJsonGetValue(prmJson, ["value"]);

  if (function == "set_leg_pose") { set_leg_pose(value, FALSE); }
  else if (function == "set_restraints") { set_restraints(value); }
  else if (function == "set_poses") { set_leg_poses(llJsonGetValue(value, ["leg"])); }
  else if (function == "tether_to") { tether_to(value); }
  else if (function == "tetherPull") { update_avi_tether_pos(TRUE); }
}

default
{
  control(key prmAviID, integer prmLevel, integer prmEdge) {
    integer press = prmLevel & prmEdge;
    integer release = ~prmLevel & prmEdge;
    integer hold = prmLevel & ~prmEdge;

    // General Movement
    if (hold == CONTROL_FWD) {
      llSetVelocity(<get_speedFwd(),0,0>, TRUE);
    }

    // Position Changes
    if (press == CONTROL_UP) {
      set_leg_pose(llJsonGetValue(get_poses(), ["leg", _legPose, "poseUp"]), TRUE);
    }
    if (press == CONTROL_DOWN) {
      set_leg_pose(llJsonGetValue(get_poses(), ["leg", _legPose, "poseDown"]), TRUE);
    }
    if (press == CONTROL_RIGHT) {
      set_leg_pose(llJsonGetValue(get_poses(), ["leg", _legPose, "poseRight"]), TRUE);
    }
    if (press == CONTROL_LEFT) {
      set_leg_pose(llJsonGetValue(get_poses(), ["leg", _legPose, "poseLeft"]), TRUE);
    }

    // Animations
    if (press == CONTROL_FWD) { simple_request("animate_mover", "animation_walk_forward"); }

    if (release) { simple_request("animate_mover", "stop"); }
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function;
    string value;

    if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
      debug(prmText);
      return;
    }
    execute_function(function, prmText);
  }

  no_sensor() {
    update_avi_tether_pos(FALSE);
  }
}
