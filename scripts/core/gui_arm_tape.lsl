#include "includes/arm_tools.lsl"
#include "includes/color_lib.lsl"
#include "includes/general_tools.lsl"
#include "includes/gui_tools.lsl"
#include "includes/restraint_tools.lsl"
#include "includes/texture_lib.lsl"
#include "includes/user_lib.lsl"
#include "includes/dictionary/texture_tape_dictionary.lsl"

integer _rpMode = FALSE;
integer _featSkip = FALSE;

integer GUI_HOME = 0;
integer GUI_STYLE = 100;
integer GUI_COLOR = 100;
//integer GUI_TEXTURE = 111;

// Status
string armsBound = "free";  // 0: FREE; 1: BOUND; 2: HELPLESS
string _villain;

string _currentRestraints;
string _restraintLib;

string get_self() {
  if (_self != "") return _self;

  _self = llJsonSetValue(_self, ["name"], "Tape");
  _self = llJsonSetValue(_self, ["part"], "arm");
  _self = llJsonSetValue(_self, ["type"], "tape");
  _self = llJsonSetValue(_self, ["hasColor"], "1");
  return _self;
}

string get_current_restraints() {
  if (_currentRestraints) {
    return _currentRestraints;
  }

  _currentRestraints = llJsonSetValue(_currentRestraints, ["wrist"], JSON_NULL);
  _currentRestraints = llJsonSetValue(_currentRestraints, ["elbow"], JSON_NULL);
  _currentRestraints = llJsonSetValue(_currentRestraints, ["torso"], JSON_NULL);
  return _currentRestraints;
}

init() {
  if (!is_set(_currentColors)) {
    set_color(COLOR_SILVER, "tape");
  }
  if (!is_set(_currentTextures)) {
    set_texture(TEXTURE_DUCT, "tape");
  }
}

init_gui(key prmID, integer prmScreen) {
  _guiUserKey = prmID;

  if (_guiListener) { llListenRemove(_guiListener); }
  _guiChannel = (integer)llFrand(-9998) - 1;
  _guiListener = llListen(_guiChannel, "", _guiUserKey, "");
  gui(prmScreen);
}

gui(integer prmScreen) {
  // Reset Busy Clock
  llSetTimerEvent(GUI_TIMEOUT);

  string btn10 = " ";   string btn11 = " ";   string btn12 = " ";
  string btn7 = " ";   string btn8 = " ";   string btn9 = " ";
  string btn4 = " ";   string btn5 = " ";   string btn6 = " ";
  string btn1 = "<<Back>>"; string btn2 = "<<Done>>"; string btn3 = " ";

  list mpButtons;
  _guiText = " ";

  // GUI: Main
  if (prmScreen == GUI_HOME) {
    btn3 = "<<Style>>";

    if (is_set(llJsonGetValue(get_current_restraints(), ["wrist"]))
      || is_set(llJsonGetValue(_currentRestraints, ["elbow"]))
      || is_set(llJsonGetValue(_currentRestraints, ["torso"]))
    ) {
      mpButtons += "Release";
    }

    if (!is_set(llJsonGetValue(_currentRestraints, ["wrist"])) && !is_set(llJsonGetValue(_currentRestraints, ["torso"]))) {
      if (!is_set(llJsonGetValue(_currentRestraints, ["elbow"]))) {
        mpButtons += "Front";
      }
      mpButtons += "Back";
    }

    if (!is_set(llJsonGetValue(_currentRestraints, ["elbow"]))
      && !is_set(llJsonGetValue(_currentRestraints, ["torso"]))
      && (llSubStringIndex(llJsonGetValue(_currentRestraints, ["wrist"]), "back") != -1 && "back_cuff" != llJsonGetValue(_currentRestraints, ["wrist"]))
    ) {
      mpButtons += "Elbow";
    }

    if (!is_set(llJsonGetValue(_currentRestraints, ["torso"])) && !is_set(llJsonGetValue(_currentRestraints, ["elbow"])) && !is_set(llJsonGetValue(_currentRestraints, ["wrist"]))) {
      mpButtons += "Sides";
    } else if (!is_set(llJsonGetValue(_currentRestraints, ["torso"]))) {
      mpButtons += "Harness";
    }

    if ((has_feat(_villain, "Anubis") || _rpMode || _featSkip)
      && llJsonGetValue(_currentRestraints, ["elbow"]) == JSON_NULL
      && llJsonGetValue(_currentRestraints, ["torso"]) == JSON_NULL
      && llJsonGetValue(_currentRestraints, ["wrist"]) == JSON_NULL
    ) {
      mpButtons += "Box";
    }

    mpButtons = multipage_gui(mpButtons, 2, _guiPage);
  }

  // GUI: Colorize
  else if (prmScreen == GUI_COLOR) {
    _guiText = "Choose a color for the arm tape.";
    mpButtons = multipage_gui(_colors, 3, _guiPage);
  }

  if (prmScreen != _guiScreen) { _guiScreenLast = _guiScreen; }
  _guiScreen = prmScreen;

  _guiButtons = [btn1, btn2, btn3];
  if (btn4+btn5+btn6 != "   ") { _guiButtons += [btn4, btn5, btn6]; }
  if (btn7+btn8+btn9 != "   ") { _guiButtons += [btn7, btn8, btn9]; }
  if (btn10+btn11+btn12 != "   ") { _guiButtons += [btn10, btn11, btn12]; }

  // Load MP Buttons - hopefully the lengths were configured correctly!
  if (llGetListLength(mpButtons)) { _guiButtons += mpButtons; }

  llDialog(_guiUserKey, _guiText, _guiButtons, _guiChannel);
}

// ===== Main Functions =====
string define_restraint(string prmName) {
  string restraint;

  // Type-specific values
  restraint = llJsonSetValue(restraint, ["name"], prmName);
  restraint = llJsonSetValue(restraint, ["canCut"], "1");
  restraint = llJsonSetValue(restraint, ["canEscape"], "1");
  restraint = llJsonSetValue(restraint, ["canTether"], "0");
  restraint = llJsonSetValue(restraint, ["canUseItem"], "1");
  restraint = llJsonSetValue(restraint, ["type"], "tape");

  integer complexity = 1;
  integer integrity;
  integer tightness;

  if (prmName == "Sides") {
    integrity = 20;
    tightness = 5;

    restraint = llJsonSetValue(restraint, ["uid"], "sides_tape");
    restraint = llJsonSetValue(restraint, ["slot"], "torso");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, ["sides"]));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, ["arm_tape_sides"]));
  } else if (prmName == "Front") {
    integrity = 20;
    tightness = 4;

    restraint = llJsonSetValue(restraint, ["uid"], "front_tape");
    restraint = llJsonSetValue(restraint, ["slot"], "wrist");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, ["front"]));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, ["arm_tape_front_wrist"]));
  } else if (prmName == "Back") {
    integrity = 20;
    tightness = 4;

    string pose = "back";
    list liAttachments = ["arm_tape_back_wrist"];
    if (llJsonGetValue(get_current_restraints(), ["elbow"]) != JSON_NULL) {
      pose = "backTight";
      liAttachments = ["arm_tape_backTight_wrist"];
    }

    restraint = llJsonSetValue(restraint, ["uid"], "back_tape");
    restraint = llJsonSetValue(restraint, ["slot"], "wrist");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, [pose]));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, liAttachments));
  } else if (prmName == "Elbow") {
    integrity = 25;
    tightness = 8;

    restraint = llJsonSetValue(restraint, ["uid"], "elbow_tape");
    restraint = llJsonSetValue(restraint, ["slot"], "elbow");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, ["backTight"]));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, ["arm_tape_backTight_elbow"]));
  } else if (prmName == "Harness") {
    integrity = 30;
    tightness = 6;

    list liAttachments;
    list liPoses;
    string wristRestraintId = llJsonGetValue(get_current_restraints(), ["wrist"]);
    if (llJsonGetValue(get_current_restraints(), ["elbow"]) != JSON_NULL
      || wristRestraintId == "back_cuff"
      || wristRestraintId == "back_zip"
    ) {
      liAttachments += "arm_tape_backTight_harness";
    } else if (llSubStringIndex(wristRestraintId, "back") != -1) {
      liAttachments += "arm_tape_back_harness";
    } else if (llSubStringIndex(wristRestraintId, "front") != -1) {
      liAttachments += "arm_tape_front_harness";
    }

    restraint = llJsonSetValue(restraint, ["uid"], "harness_tape");
    restraint = llJsonSetValue(restraint, ["slot"], "torso");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, liPoses));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, liAttachments));
  } else if (prmName == "Box") {
    complexity = 2;
    integrity = 30;
    tightness = 15;

    restraint = llJsonSetValue(restraint, ["uid"], "box_tape");
    restraint = llJsonSetValue(restraint, ["slot"], "torso");
    restraint = llJsonSetValue(restraint, ["poses"], llList2Json(JSON_ARRAY, ["box"]));
    restraint = llJsonSetValue(restraint, ["attachments"], llList2Json(JSON_ARRAY, ["arm_tape_box_wrist", "arm_tape_box_harness"]));
  }

  if (has_feat(_villain, "Anubis")) { tightness = tightness + 2; }
  if (has_feat(_villain, "Anubis+")) { tightness = tightness + 2; }

  restraint = llJsonSetValue(restraint, ["complexity"], (string)complexity);
  restraint = llJsonSetValue(restraint, ["integrity"], (string)integrity);
  restraint = llJsonSetValue(restraint, ["tightness"], (string)tightness);

  return restraint;
}

send_availability_info() {
  simple_request("add_available_restraint", get_self());
}

set_arms_bound(string prmArmsBound) {
  armsBound = prmArmsBound;
}

// ===== Event Controls =====
execute_function(string prmFunction, string prmJson) {
  string value = llJsonGetValue(prmJson, ["value"]);
  if (JSON_INVALID == value) {
    //return;  // TODO: Rewrite all linked calls to send in JSON
  }

  if (prmFunction == "set__gender") { set_gender(value); }
  else if (prmFunction == "set_restraints") {
    _currentRestraints = llJsonGetValue(value, ["slots"]);
    set_restraints(value);
  }
  else if (prmFunction == "get_available_restraints") { send_availability_info(); }
  else if (prmFunction == "set_rpMode") { _rpMode = (integer)value; }
  else if (prmFunction == "set_featSkip") { _featSkip = (integer)value; }
  else if (prmFunction == "set_villain") { _villain = value; }
  else if (prmFunction == "request_style") {
    if (llJsonGetValue(value, ["attachment"]) != llJsonGetValue(get_self(), ["part"])) { return; }
    if (llJsonGetValue(value, ["name"]) != "tape") { return; }
    string component = llJsonGetValue(value, ["component"]);
    if ("" == component) { component = "tape"; }
    set_color((vector)llJsonGetValue(_currentColors, [component]), component);
    set_texture(llJsonGetValue(_currentTextures, [component]), component);
  }
  else if (prmFunction == "gui_arm_tape") {
    key userkey = (key)llJsonGetValue(prmJson, ["userkey"]);
    integer screen = 0;
    if ((integer)llJsonGetValue(prmJson, ["restorescreen"]) && _guiScreenLast) { screen = _guiScreenLast;}
    init_gui(userkey, screen);
  } else if (prmFunction == "reset_gui") {
    exit("");
  }
  else if (prmFunction == "add_restraint_by_type") {
    if (llJsonGetValue(value, ["type"]) != llJsonGetValue(get_self(), ["type"])) {
      return; // Incorrect type
    }

    if (llJsonGetValue(value, ["part"]) != llJsonGetValue(get_self(), ["part"])) {
      return; // Incorrect type
    }

    string restraintSet;
    restraintSet = llJsonSetValue(restraintSet, ["type"], llJsonGetValue(get_self(), ["part"]));
    restraintSet = llJsonSetValue(restraintSet, ["restraint"], define_restraint(llJsonGetValue(value, ["restraint"])));
    simple_request("add_restraint", restraintSet);
    return;
  }
}

default
{
  state_entry() {
    init();
  }

  listen(integer prmChannel, string prmName, key prmID, string prmText) {
    if (prmChannel = _guiChannel) {
      if (prmText == "<<Done>>") { exit("done"); return; }
      else if (prmText == " ") { gui(_guiScreen); return; }
      else if (prmText == "<<Back>>") {
        if (_guiScreen != GUI_HOME) { gui(_guiScreenLast); return;}
        gui_request("gui_bind", TRUE, _guiUserKey, 0);
        return;
      }
      else if (prmText == "Next >>") { _guiPage ++; gui(_guiScreen); return; }
      else if (prmText == "<< Previous") { _guiPage --; gui(_guiScreen); return; }

      if (prmText == "Release") {
        simple_request("remove_restraint", llJsonGetValue(get_self(), ["part"]));
        _resumeFunction = "set_restraints";
        return;
      }

      if (_guiScreen == GUI_HOME) {
        if (prmText == "<<Style>>") {
          gui(GUI_STYLE);
          return;
        } else {
          string restraintSet;
          restraintSet = llJsonSetValue(restraintSet, ["type"], llJsonGetValue(get_self(), ["part"]));
          restraintSet = llJsonSetValue(restraintSet, ["restraint"], define_restraint(prmText));
          simple_request("add_restraint", restraintSet);
          _resumeFunction = "set_restraints";
          return;
        }
      } else if (_guiScreen == GUI_COLOR) {
        set_color_by_name(prmText, "tape");
        gui(_guiScreen);
        return;
      }
    }
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function;
    string value;

    if ((function = llJsonGetValue(prmText, ["function"])) == JSON_INVALID) {
      debug(prmText);
      return;
    }

    execute_function(function, prmText);

    if (function == _resumeFunction) {
      _resumeFunction = "";
      init_gui(_guiUserKey, _guiScreen);
    }
  }

  timer() {
    exit("timeout");
  }

}
