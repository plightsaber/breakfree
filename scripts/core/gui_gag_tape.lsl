#include "includes/color_lib.lsl"
#include "includes/gag_tools.lsl"
#include "includes/general_tools.lsl"
#include "includes/gui_tools.lsl"
#include "includes/texture_lib.lsl"
#include "includes/user_lib.lsl"
#include "includes/dictionary/texture_tape_dictionary.lsl"

integer _rpMode = FALSE;
integer _featSkip = FALSE;

integer GUI_HOME = 0;
integer GUI_STYLE = 100;
integer GUI_COLOR_STUFF = 101;
integer GUI_COLOR_TAPE = 102;
integer GUI_TEXTURE = 111;

string _villain;

string get_self() {
  if (_self != "") return _self;

  _self = llJsonSetValue(_self, ["name"], "Tape");
  _self = llJsonSetValue(_self, ["part"], "gag");
  _self = llJsonSetValue(_self, ["type"], "tape");
  _self = llJsonSetValue(_self, ["hasColor"], "1");
  return _self;
}

init() {
  if (!is_set(_currentColors)) {
    set_color(COLOR_SILVER, "tape");
  }
  if (!is_set(_currentTextures)) {
    set_texture(TEXTURE_DUCT, "tape");
  }
}

init_gui(key prmID, integer prmScreen) {
  _guiUserKey = prmID;

  if (_guiListener) { llListenRemove(_guiListener); }
  _guiChannel = (integer)llFrand(-9998) - 1;
  _guiListener = llListen(_guiChannel, "", _guiUserKey, "");
  gui(prmScreen);
}

send_availability_info() {
  simple_request("add_available_restraint", get_self());
}

gui(integer prmScreen) {
  // Reset Busy Clock
  llSetTimerEvent(GUI_TIMEOUT);

  string btn10 = " ";   string btn11 = " ";   string btn12 = " ";
  string btn7 = " ";   string btn8 = " ";   string btn9 = " ";
  string btn4 = " ";   string btn5 = " ";   string btn6 = " ";
  string btn1 = "<<Back>>"; string btn2 = "<<Done>>"; string btn3 = " ";

  list mpButtons;
  _guiText = " ";

  // GUI: Main
  if (prmScreen == GUI_HOME) {
    get_current_restraints();
    btn3 = "<<Style>>";
    if (llJsonGetValue(_currentRestraints, ["gag1"]) != JSON_NULL
      || llJsonGetValue(_currentRestraints, ["gag2"]) != JSON_NULL
      || llJsonGetValue(_currentRestraints, ["gag3"]) != JSON_NULL
      || llJsonGetValue(_currentRestraints, ["gag4"]) != JSON_NULL
    ) {
      mpButtons += "Ungag";
    }

    if (llJsonGetValue(_currentRestraints, ["gag1"]) == JSON_NULL
      && llJsonGetValue(_currentRestraints, ["gag2"]) == JSON_NULL
      && llJsonGetValue(_currentRestraints, ["gag3"]) == JSON_NULL
      && llJsonGetValue(_currentRestraints, ["gag4"]) == JSON_NULL
    ) {
      mpButtons += "Stuff";
    }

    if (llJsonGetValue(_currentRestraints, ["gag2"]) == JSON_NULL
      && llJsonGetValue(_currentRestraints, ["gag3"]) == JSON_NULL
      && llJsonGetValue(_currentRestraints, ["gag4"]) == JSON_NULL
    ) {
      mpButtons += "Simple";
    }

    if (llJsonGetValue(_currentRestraints, ["gag2"]) != JSON_NULL
      && llJsonGetValue(_currentRestraints, ["gag3"]) == JSON_NULL
      && llJsonGetValue(_currentRestraints, ["gag4"]) == JSON_NULL
    ) {
      mpButtons += "Heavy";
    }

    mpButtons = multipage_gui(mpButtons, 2, _guiPage);
  }
  // GUI: Colorize
  else if (prmScreen == GUI_STYLE) {
    _guiText = "Choose what you want to style.";
    mpButtons = multipage_gui(["Tape", "Stuffing"], 2, _guiPage);
  }
  else if (prmScreen == GUI_COLOR_TAPE) {
    _guiText = "Choose a color for the tape.";
    mpButtons = multipage_gui(_colors, 3, _guiPage);
  }
  else if (prmScreen == GUI_COLOR_STUFF) {
    _guiText = "Choose a color for the stuffing.";
    mpButtons = multipage_gui(_colors, 3, _guiPage);
  }
  /*
  else if (prmScreen == 111) {
    _guiText = "Choose a texture for the gag.";
    mpButtons = multipage_gui(textures, 3, _guiPage);
  }
  */

  if (prmScreen != _guiScreen) { _guiScreenLast = _guiScreen; }
  _guiScreen = prmScreen;

  _guiButtons = [btn1, btn2, btn3];
  if (btn4+btn5+btn6 != "   ") { _guiButtons += [btn4, btn5, btn6]; }
  if (btn7+btn8+btn9 != "   ") { _guiButtons += [btn7, btn8, btn9]; }
  if (btn10+btn11+btn12 != "   ") { _guiButtons += [btn10, btn11, btn12]; }

  // Load MP Buttons - hopefully the lengths were configured correctly!
  if (llGetListLength(mpButtons)) { _guiButtons += mpButtons; }

  llDialog(_guiUserKey, _guiText, _guiButtons, _guiChannel);
}

// ===== Main Functions =====
string define_restraint(string prmName) {
  string gag;

  // Type-specific values
  gag = llJsonSetValue(gag, ["name"], prmName);
  gag = llJsonSetValue(gag, ["type"], llJsonGetValue(get_self(), ["type"]));
  gag = llJsonSetValue(gag, ["canEscape"], "1");
  gag = llJsonSetValue(gag, ["canCut"], "1");

  integer complexity;
  integer integrity;
  integer tightness;

  if (prmName == "Stuff") {
    complexity = 1;
    integrity = 2;
    tightness = 3;

    gag = llJsonSetValue(gag, ["uid"], "stuff_cloth");
    gag = llJsonSetValue(gag, ["slot"], "gag1");
    gag = llJsonSetValue(gag, ["canCut"], "0");
    gag = llJsonSetValue(gag, ["mouthOpen"], "1");
    gag = llJsonSetValue(gag, ["attachments", JSON_APPEND], "gag_cloth_stuff");
  } else if (prmName == "Simple") {
    complexity = 1;
    integrity = 10;
    tightness = 5;

    gag = llJsonSetValue(gag, ["uid"], "simple_tape");
    gag = llJsonSetValue(gag, ["speechSealed"], "1");
    gag = llJsonSetValue(gag, ["slot"], "gag2");
    if (_mouthOpen) { gag = llJsonSetValue(gag, ["attachments", JSON_APPEND], "gag_tape_stuffed"); }
    else { gag = llJsonSetValue(gag, ["attachments", JSON_APPEND], "gag_tape_simple"); }
  } else if (prmName == "Heavy") {
    complexity = 2;
    integrity = 10;
    tightness = 5;

    gag = llJsonSetValue(gag, ["uid"], "heavy_tape");
    gag = llJsonSetValue(gag, ["speechSealed"], "1");
    gag = llJsonSetValue(gag, ["slot"], "gag3");
    gag = llJsonSetValue(gag, ["complexity"], "2");
    gag = llJsonSetValue(gag, ["integrity"], "10");
    gag = llJsonSetValue(gag, ["tightness"], "5");
    gag = llJsonSetValue(gag, ["attachments", JSON_APPEND], "gag_tape_heavy");
    gag = llJsonSetValue(gag, ["preventAttach", JSON_APPEND], "gag_tape_simple");
    gag = llJsonSetValue(gag, ["preventAttach", JSON_APPEND], "gag_tape_stuffed");
  }

  if (has_feat(_villain, "Anubis")) { tightness = tightness + 2; }
  if (has_feat(_villain, "Anubis+")) { tightness = tightness + 2; }
  if (has_feat(_villain, "Gag Snob")) { integrity = integrity + 5; }
  if (has_feat(_villain, "Gag Snob+")) { complexity++; }

  gag = llJsonSetValue(gag, ["complexity"], (string)complexity);
  gag = llJsonSetValue(gag, ["integrity"], (string)integrity);
  gag = llJsonSetValue(gag, ["tightness"], (string)tightness);

  return gag;
}

execute_function(string prmFunction, string prmJson) {
  string value = llJsonGetValue(prmJson, ["value"]);
  if (JSON_INVALID == value) {
    //return;  // TODO: Rewrite all linked calls to send in JSON
  }

  if (prmFunction == "set_gender") { set_gender(value); }
  else if (prmFunction == "set_restraints") {
    _currentRestraints = llJsonGetValue(value, ["slots"]);
    _mouthOpen = llJsonGetValue(value, ["mouthOpen"]) == "1";
  }
  else if (prmFunction == "get_available_restraints") { send_availability_info(); }
  else if (prmFunction == "set_rpMode") { _rpMode = (integer)value; }
  else if (prmFunction == "set_featSkip") { _featSkip = (integer)value; }
  else if (prmFunction == "set_villain") { _villain = value; }
  else if (prmFunction == "request_style") {
    if (llJsonGetValue(value, ["attachment"]) != llJsonGetValue(get_self(), ["part"])) { return; }
    if (llJsonGetValue(value, ["name"]) != "tape") { return; }
    string component = llJsonGetValue(value, ["component"]);
    if ("" == component) { component = "tape"; }
    set_color((vector)llJsonGetValue(_currentColors, [component]), component);
    set_texture(llJsonGetValue(_currentTextures, [component]), component);
  }
  else if (prmFunction == "set_color") {
    if (llJsonGetValue(value, ["attachment"]) != "gag") { return; }
    if (!is_set(llJsonGetValue(value, ["component"]))) { return; }
    _currentColors = llJsonSetValue(_currentColors, [llJsonGetValue(value, ["component"])], llJsonGetValue(value, ["color"]));
  }
  else if (prmFunction == "gui_gag_tape") {
    key userkey = (key)llJsonGetValue(prmJson, ["userkey"]);
    integer screen = 0;
    if ((integer)llJsonGetValue(prmJson, ["restorescreen"]) && _guiScreenLast) { screen = _guiScreenLast;}
    init_gui(userkey, screen);
  } else if (prmFunction == "reset_gui") {
    exit("");
  }
  else if (prmFunction == "add_restraint_by_type") {
    if (llJsonGetValue(value, ["type"]) != llJsonGetValue(get_self(), ["type"])) {
      return; // Incorrect type
    }

    if (llJsonGetValue(value, ["part"]) != llJsonGetValue(get_self(), ["part"])) {
      return; // Incorrect type
    }

    string restraintSet;
    restraintSet = llJsonSetValue(restraintSet, ["type"], llJsonGetValue(get_self(), ["part"]));
    restraintSet = llJsonSetValue(restraintSet, ["restraint"], define_restraint(llJsonGetValue(value, ["restraint"])));
    simple_request("add_restraint", restraintSet);
    return;
  }
}

// ===== Event Controls =====
default
{
  state_entry() {
    init();
  }

  listen(integer prmChannel, string prmName, key prmID, string prmText) {
    if (prmChannel = _guiChannel) {
      if (prmText == "<<Done>>") { exit("done"); return; }
      else if (prmText == " ") { gui(_guiScreen); return; }
      else if (prmText == "<<Back>>") {
        if (_guiScreen == 100) { gui(0); return; }
        else if (_guiScreen != 0) { gui(_guiScreenLast); return;}
        gui_request("gui_bind", TRUE, _guiUserKey, 0);
        return;
      }
      else if (prmText == "Next >>") { _guiPage ++; gui(_guiScreen); return; }
      else if (prmText == "<< Previous") { _guiPage --; gui(_guiScreen); return; }

      if (prmText == "Ungag") {
        simple_request("remove_restraint", "gag");
        _resumeFunction = "set_restraints";
        return;
      }

      if (_guiScreen == GUI_HOME) {
        if (prmText == "<<Style>>") {
          gui(100);
        } else {
          string restraint;
          restraint = llJsonSetValue(restraint, ["type"], llJsonGetValue(get_self(), ["part"]));
          restraint = llJsonSetValue(restraint, ["restraint"], define_restraint(prmText));
          simple_request("add_restraint", restraint);
          _resumeFunction = "set_restraints";
        }
        return;
      } else if (_guiScreen == GUI_STYLE) {
        if ("Tape" == prmText) { gui(GUI_COLOR_TAPE); }
        else if ("Stuffing" == prmText) { gui(GUI_COLOR_STUFF); }
      } else if (_guiScreen == GUI_COLOR_TAPE) {
        set_color_by_name(prmText, "tape");
      } else if (_guiScreen == GUI_COLOR_STUFF) {
        set_color_by_name(prmText, "stuff");
      }
      gui(_guiScreen);
      return;
    }
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function = llJsonGetValue(prmText, ["function"]);
    if (JSON_INVALID == function) {
      debug(prmText);
      return;
    }

    execute_function(function, prmText);

    if (function == _resumeFunction) {
      _resumeFunction = "";
      init_gui(_guiUserKey, _guiScreen);
    }
  }

  timer() {
    exit("timeout");
  }
}
