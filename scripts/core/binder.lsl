#include "includes/contrib_lib.lsl"
#include "includes/general_tools.lsl"
#include "includes/restraint_tools.lsl"

string _slots;  // List of UIDs ONLY for currently applied restraints
string _villain;

init() {
  _slots = llJsonSetValue(_slots, ["wrist"], JSON_NULL);
  _slots = llJsonSetValue(_slots, ["elbow"], JSON_NULL);
  _slots = llJsonSetValue(_slots, ["torso"], JSON_NULL);
  _slots = llJsonSetValue(_slots, ["ankle"], JSON_NULL);
  _slots = llJsonSetValue(_slots, ["knee"], JSON_NULL);
  _slots = llJsonSetValue(_slots, ["immobilizer"], JSON_NULL);
  _slots = llJsonSetValue(_slots, ["gag1"], JSON_NULL);
  _slots = llJsonSetValue(_slots, ["gag2"], JSON_NULL);
  _slots = llJsonSetValue(_slots, ["gag3"], JSON_NULL);
  _slots = llJsonSetValue(_slots, ["gag4"], JSON_NULL);

  _slots = llJsonSetValue(_slots, ["crotch"], JSON_NULL);

  override_restraint(_restraints);
}

add_restraint(string prmJson) {
  string restraint = llJsonGetValue(prmJson, ["restraint"]);
  string slot = llJsonGetValue(restraint, ["slot"]);

  _restraints = llJsonSetValue(_restraints, [slot], restraint);
  _slots = llJsonSetValue(_slots, [slot], llJsonGetValue(restraint, ["uid"]));

  // You don't get experience for tying yourself up
  if (llGetOwner() != llJsonGetValue(_villain, ["key"])) {
    string request;

    integer tightness = (integer)llJsonGetValue(restraint, ["tightness"]);
    integer integrity = (integer)llJsonGetValue(restraint, ["integrity"]);
    integer complexity = (integer)llJsonGetValue(restraint, ["complexity"]);
    integer expEarned = ((tightness+integrity)*complexity/2);

    api_request(llJsonGetValue(_villain, ["key"]), llGetOwner(), "add_exp", (string)expEarned);
  }
  deploy_restraints();
}

deploy_restraints() {
  reset_poses();
  simple_request("set_attachments", llList2Json(JSON_ARRAY, get_attachments()));
  rebuild_metadata();
  rebuild_security();
}

list get_attachments() {
  list bindFolders = get_restraint_list(_restraints, "attachments");
  list preventFolders = get_restraint_list(_restraints, "preventAttach");

  // Use bent knee restraints for certain positions
  string kneeRestraint = llJsonGetValue(_restraints, ["knee"]);
  string immobilizer = llJsonGetValue(_restraints, ["immobilizer", "uid"]);
  if (is_set(kneeRestraint) && is_set(immobilizer) &&
    (llSubStringIndex(immobilizer, "hog") != -1 || llSubStringIndex(immobilizer, "ball") != -1 || llSubStringIndex(immobilizer, "kneel") != -1)
  ) {
    // Add/Remove attachment by pattern.  You hopefully named everything to proper convention!
    string attachName = "leg_" + llJsonGetValue(kneeRestraint, ["type"]) + "_knee";
    preventFolders += attachName;
    bindFolders += attachName + "Bent";
  }

  // Hogtie connectors
  if ("hog_rope" == immobilizer) {
    if (is_set(llJsonGetValue(_restraints, ["elbow"]))) { bindFolders += ["leg_rope_hogBackTight"]; }
    else if (llJsonGetValue(_restraints, ["torso", "uid"]) == "box_rope") { bindFolders += ["leg_rope_hogHarness"]; }
    else { bindFolders += ["leg_rope_hogBack"]; }
  }

  // Change meshes for back => backTight pose
  if (is_set(llJsonGetValue(_restraints, ["elbow"]))) {
    string wrist = llJsonGetValue(_restraints, ["wrist", "uid"]);
    if (wrist == "back_rope") {
      bindFolders += "arm_rope_backTight_wrist";
      preventFolders += "arm_rope_back_wrist";
    } else if (wrist == "back_tape") {
      bindFolders += "arm_tape_backTight_wrist";
      preventFolders += "arm_tape_back_wrist";
    }
  }

  return ListXnotY(bindFolders, preventFolders);
}

override_restraint(string prmJson) {
  _restraints = prmJson;
  deploy_restraints();
}

remove_slot(string prmSlot) {
  string restraint = llJsonGetValue(_restraints, [prmSlot]);

  _restraints = llJsonSetValue(_restraints, [prmSlot], JSON_NULL);
  _slots = llJsonSetValue(_slots, [prmSlot], JSON_NULL);

  // Remove hog_rope if connecting arm restraint removed
  if (llJsonGetValue(_slots, ["immobilizer"]) == "hog_rope"
    && (prmSlot == "wrist" || llJsonGetValue(restraint, ["uid"]) == "box_rope")
  ) {
    remove_slot("immobilizer");
  }
}

remove_restraint(string prmType) {
  string removedRestraint = get_top_restraint(prmType);
  if (JSON_NULL == removedRestraint) {
    debug("No restraints to remove.");
    return;
  }

  string slot = llJsonGetValue(removedRestraint, ["slot"]);

  // Removal rules are about to get complicated.  This is a problem for future Myshel!
  integer isEscape = FALSE; // TODO: Only apply alternate order for escapes.
  if (isEscape && prmType == "leg") {
    if (is_set(llJsonGetValue(_restraints, ["immobilizer"]))) { slot = "immobilizer"; }
    else if (is_set(llJsonGetValue(_restraints, ["ankle"]))) { slot = "ankle"; }
    else if (is_set(llJsonGetValue(_restraints, ["knee"]))) { slot = "knee"; }
  }

  remove_slot(slot);
  deploy_restraints();
}

reset_poses() {
  string poses;
  string poseRequest;

  // Arm poses
  poses = llJsonGetValue(get_top_restraint("arm"), ["poses"]);
  if (!is_set(poses)) {
    poses = llJsonSetValue(poses, [JSON_APPEND], "free");
  }
  poseRequest = llJsonSetValue(poseRequest, ["arm"], poses);

  // Leg poses
  poses = llJsonGetValue(get_top_restraint("leg"), ["poses"]);
  if (!is_set(poses)) {
    poses = llJsonSetValue(poses, [JSON_APPEND], "free");
  }
  poseRequest = llJsonSetValue(poseRequest, ["leg"], poses);

  simple_request("set_poses", poseRequest);
}

release_restraint(string prmType) {
  if (prmType == "arm") {
    remove_slot("torso");
    remove_slot("elbow");
    remove_slot("wrist");
  } else if (prmType == "leg") {
    remove_slot("immobilizer");
    remove_slot("knee");
    remove_slot("ankle");
    remove_slot("crotch");
  } else if (prmType == "gag") {
    remove_slot("gag1");
    remove_slot("gag2");
    remove_slot("gag3");
    remove_slot("gag4");
  } else if (prmType == "hand") {
    remove_slot("hand");
  } else if (prmType == "blindfold") {
    remove_slot("blindfold");
  }

  deploy_restraints();
}

rebuild_security()
{
  string security;
  security = llJsonSetValue(security, ["arm"], get_security_details("arm"));
  security = llJsonSetValue(security, ["leg"], get_security_details("leg"));
  security = llJsonSetValue(security, ["gag"], get_security_details("gag"));
  security = llJsonSetValue(security, ["blindfold"], get_security_details("blindfold"));
  security = llJsonSetValue(security, ["crotch"], get_security_details("crotch"));
  security = llJsonSetValue(security, ["hand"], get_security_details("hand"));

  simple_request("set_security", security);
}

rebuild_metadata() {
  string metadata;
  metadata = llJsonSetValue(metadata, ["slots"], _slots);

  integer isArmsBound = is_set(llJsonGetValue(_restraints, ["wrist"]))
    || is_set(llJsonGetValue(_restraints, ["elbow"]))
    || is_set(llJsonGetValue(_restraints, ["torso"]));
  metadata = llJsonSetValue(metadata, ["armBound"], (string)isArmsBound);

  integer isArmsBoundExternal = is_set(llJsonGetValue(_restraints, ["armExternal"]));
  metadata = llJsonSetValue(metadata, ["armBoundExternal"], (string)isArmsBoundExternal);

  integer isLegsBound = is_set(llJsonGetValue(_restraints, ["ankle"]))
    || is_set(llJsonGetValue(_restraints, ["knee"]))
    || is_set(llJsonGetValue(_restraints, ["immobilizer"]));
  metadata = llJsonSetValue(metadata, ["legBound"], (string)isLegsBound);

  integer is_gagged = is_set(llJsonGetValue(_restraints, ["gag1"]))
    || is_set(llJsonGetValue(_restraints, ["gag2"]))
    || is_set(llJsonGetValue(_restraints, ["gag3"]))
    || is_set(llJsonGetValue(_restraints, ["gag4"]));
  metadata = llJsonSetValue(metadata, ["gagged"], (string)is_gagged);

  if (is_gagged) {
    metadata = llJsonSetValue(metadata, ["mouthOpen"], (string)search_restraint("gag", "mouthOpen", "1"));
    metadata = llJsonSetValue(metadata, ["speechGarbled"], (string)search_restraint("gag", "speechGarbled", "1"));
    metadata = llJsonSetValue(metadata, ["speechMuffled"], (string)search_restraint("gag", "speechMuffled", "1"));
    metadata = llJsonSetValue(metadata, ["speechSealed"], (string)search_restraint("gag", "speechSealed", "1"));
  } else {
    metadata = llJsonSetValue(metadata, ["mouthOpen"], "0");
    metadata = llJsonSetValue(metadata, ["speechGarbled"], "0");
    metadata = llJsonSetValue(metadata, ["speechMuffled"], "0");
    metadata = llJsonSetValue(metadata, ["speechSealed"], "0");
  }

  metadata = llJsonSetValue(metadata, ["armTetherable"], (string)search_restraint("arm", "canTether", "1"));
  metadata = llJsonSetValue(metadata, ["legTetherable"], (string)search_restraint("leg", "canTether", "1"));

  metadata = llJsonSetValue(metadata, ["armBoundExternal"], (string)search_restraint("arm", "type", "external"));
  metadata = llJsonSetValue(metadata, ["animations"], llList2Json(JSON_ARRAY, get_restraint_list(_restraints, "animations")));

  simple_request("set_restraints", metadata);
}

string get_security_details(string prmType) {
  string details = "{'complexity':0,'integrity':0,'tightness':0}";

  // Tightness: Add all restraint totals for tightness
  // Complexity: Get just the top level
  // Integrity: Get just the top level.

  // Get details from top restraint
  string topRestraint = get_top_restraint(prmType);
  details = llJsonSetValue(details, ["complexity"], llJsonGetValue(topRestraint, ["complexity"]));
  details = llJsonSetValue(details, ["integrity"], llJsonGetValue(topRestraint, ["integrity"]));

  // Get cumulative details
  list slots = get_search_slots(prmType);

  integer index;
  integer tightness;
  for (index = 0; index < llGetListLength(slots); index++) {
    string slot = llList2String(slots, index);
    tightness += (integer)llJsonGetValue(_restraints, [slot, "tightness"]);
  }
  details = llJsonSetValue(details, ["tightness"], (string)tightness);

  // Other properties
  details = llJsonSetValue(details, ["canCrop"], llJsonGetValue(topRestraint, ["canCrop"]));
  details = llJsonSetValue(details, ["canCut"], llJsonGetValue(topRestraint, ["canCut"]));
  details = llJsonSetValue(details, ["canEscape"], llJsonGetValue(topRestraint, ["canEscape"]));
  details = llJsonSetValue(details, ["canPick"], llJsonGetValue(topRestraint, ["canPick"]));

  return details;
}

integer is_mouth_open() {
  integer index;
  list liGags = llJson2List(llJsonGetValue(_restraints,["gag"]));
  for (index = 0; index < llGetListLength(liGags); ++index) {
    if ("1" == llJsonGetValue(llList2String(liGags, index), ["mouthOpen"])) {
      return TRUE;
    }
  }
  return FALSE;
}

default
{
  state_entry() {
    init();
  }

  link_message(integer prmLink, integer prmValue, string prmText, key prmID) {
    string function = llJsonGetValue(prmText, ["function"]);
    if (JSON_INVALID == function) {
      return;
    }

    string value = llJsonGetValue(prmText, ["value"]);
    if (JSON_INVALID == value) {
      return;
    }

    if ("add_restraint" == function) add_restraint(value);
    else if ("remove_restraint" == function) remove_restraint(value);
    else if ("remove_slot" == function) { remove_slot(value); deploy_restraints(); return; }
    else if ("release_restraint" == function) release_restraint(value);
    else if ("overrideRestraint" == function) override_restraint(value);
    else if ("set_villain" == function) { _villain = value; }
  }

}
