#include "includes/gui_tools.lsl"
integer _busy = FALSE;

list CORE = ["BreakFree", "README", "CHANGELOG"];
list TOOLS = ["BreakFree - Bobby Pin", "BreakFree - Bolt Cropper", "BreakFree - Knife"];
list HUDS = ["BreakFree - Villain HUD"];
list SCRIPTS = ["&BreakFree", "&BFServer", "_escape_tool"];

init_gui(key prmUserKey, integer prmScreen) {
  _guiUserKey = prmUserKey;

  if (_guiListener) { llListenRemove(_guiListener); }
  _guiChannel = (integer)llFrand(-9998) - 1;
  _guiListener = llListen(_guiChannel, "", _guiUserKey, "");
  gui(prmScreen);
}

string get_folder_path(string inventoryName) {
  integer space =  llSubStringIndex(inventoryName, " ");
  string newFolder = inventoryName;
  if (space != -1) {
    newFolder = llGetSubString(inventoryName, 0, space - 1); // Index includes the space.  Subtract it.
  }

  return "#RLV/~BreakFree/" + newFolder;
}

gui(integer prmScreen) {
  llDialog(
    _guiUserKey,
    "Current version: BreakFree 5.2.0\nREQUIRES RLV VIEWER\n\nWhat parts of BreakFree do you need?",
    ["Huds", "Scripts", "Tools", "Core", "Attachments", " ", "Everything!"],
    _guiChannel
  );
}

give_core(key prmUserKey) {
  llGiveInventoryList(prmUserKey, "#RLV/~BreakFree/_core", CORE);
}

give_huds(key prmUserKey) {
  llGiveInventoryList(prmUserKey, "#RLV/~BreakFree/_huds", HUDS);
}

give_tools(key prmUserKey) {
  llGiveInventoryList(prmUserKey, "#RLV/~BreakFree/_tools", TOOLS);
}

give_scripts(key prmUserKey) {
  llGiveInventoryList(prmUserKey, "#RLV/~BreakFree/_scripts", SCRIPTS);
}

give_restraints(key prmUserKey) {
  if (_busy) {
    llRegionSayTo(prmUserKey, 0, "The restraint box is busy right now.  Please try again in a minute.");
  }

  _busy = TRUE;
  llRegionSayTo(prmUserKey, 0, "There are a lot of folders.  Please make sure to accept all of them.");

  integer inventoryCount = llGetInventoryNumber(INVENTORY_OBJECT);
  integer index;
  for (index; index < inventoryCount; index++) {
    string inventoryName = llGetInventoryName(INVENTORY_OBJECT, index);
      // Seperate non-restraints
      if (-1 == llListFindList(CORE, [inventoryName])
        && -1 == llListFindList(HUDS, [inventoryName])
        && -1 == llListFindList(TOOLS, [inventoryName]))
      {
        llGiveInventoryList(prmUserKey, get_folder_path(inventoryName), [inventoryName]);
      }
  }

  _busy = FALSE;
}

default
{
  touch_start(integer prmDetected) {
    key userKey = llDetectedKey(0);
    init_gui(userKey, 0);
  }

  listen(integer prmChannel, string prmName, key prmKey, string prmText) {
    if (prmChannel = _guiChannel) {
      if (prmText == "Everything!") {
        give_core(prmKey);
        give_huds(prmKey);
        give_restraints(prmKey);
        give_tools(prmKey);
        give_scripts(prmKey);
      }

      if (prmText == "Core") {
        give_core(prmKey);
      }

      if (prmText == "Attachments") {
        give_restraints(prmKey);
      }

      if (prmText == "Tools") {
        give_tools(prmKey);
      }

      if (prmText == "Huds") {
        give_huds(prmKey);
      }

      if (prmText == "Scripts") {
        give_scripts(prmKey);
      }

    }
  }
}