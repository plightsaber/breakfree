#ifndef USER_LIB_LSL
#define USER_LIB_LSL

string _gender = "female";

string get_default_user(key prmUserKey) {
  string user = "";
  // Basic Info
  user = llJsonSetValue(user, ["key"], prmUserKey);
  user = llJsonSetValue(user, ["name"], llGetDisplayName(prmUserKey));
  user = llJsonSetValue(user, ["gender"], get_gender_from_key(prmUserKey));

  // Skills
  list feats;
  user = llJsonSetValue(user, ["feats"], llList2Json(JSON_ARRAY, feats));

  // Bound Status
  user = llJsonSetValue(user, ["armBound"], "0");
  user = llJsonSetValue(user, ["handBound"], "0");
  user = llJsonSetValue(user, ["blade"], "0");

  return user;
}

string get_gender_from_key(key prmUserID) {
  list details = llGetObjectDetails(prmUserID, [OBJECT_BODY_SHAPE_TYPE]);
  if (details == []) return "female";

  float gender = llList2Float(details, 0);
  if (gender < 0.0)  return "object";
  if (gender > 0.5)  return "male";

  return "female";
}

string get_name() {
  return llGetDisplayName(llGetOwner());
}

string get_owner_pronoun(string prmPlaceholder) {
  if ("female" == _gender) {
    return prmPlaceholder;
  } else {
    if ("her" == prmPlaceholder) {
      return "his";
    }
  }

  return "";
}

set_gender(string prmGender) {
  _gender = prmGender;
}

integer has_feat(string user, string feat) {
  string feats = llJsonGetValue(user, ["feats"]);
  list liFeats = llJson2List(feats);
  return llListFindList(liFeats, [feat]) > -1;
}
#endif // USER_LIB_LSL
