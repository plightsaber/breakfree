#ifndef GUI_TOOLS_LSL
#define GUI_TOOLS_LSL
#include "includes/general_tools.lsl"
integer CHANNEL_ATTACHMENT = -9999277;
integer GUI_TIMEOUT = 60;

integer _guiListener;
integer _guiPage = 0;

key _guiUserKey;
list _guiButtons;
integer _guiChannel;
integer _guiScreen;
integer _guiScreenLast;
string _guiText;

exit(string prmReason) {
  llListenRemove(_guiListener);
  llSetTimerEvent(0.0);
  if (prmReason) { simple_request("reset_gui", prmReason); }
}

gui_request(string prmGUI, integer prmRestore, key prmUserID, integer prmScreen) {
  string guiRequest = "";
  guiRequest = llJsonSetValue(guiRequest, ["function"], prmGUI);
  guiRequest = llJsonSetValue(guiRequest, ["restorescreen"], (string)prmRestore);
  guiRequest = llJsonSetValue(guiRequest, ["userkey"], (string)prmUserID);
  guiRequest = llJsonSetValue(guiRequest, ["value"], (string)prmScreen);
  llMessageLinked(LINK_THIS, 0, guiRequest, NULL_KEY);
  exit("");
}

list multipage_gui(list prmButtons, integer prmRows, integer prmPage) {
    list mpGui = [];
    integer buttonCount = llGetListLength(prmButtons);
    integer availableRows = prmRows;

    if (buttonCount >= 3 * prmRows) {
      // Devote a row to navigation buttons
      availableRows--;
      mpGui += ["<< Previous", " ", "Next >>"];

      _guiPage = prmPage;
  }

  // Set page index
  _guiPage = prmPage;
  if (prmPage < 0) {
    _guiPage = llFloor(buttonCount / (3 * availableRows));
  } else if (prmPage * availableRows * 3 >= buttonCount) {
    _guiPage = 0;
  }

    integer mpIndex = _guiPage * availableRows * 3;
    for (mpIndex; mpIndex < buttonCount; mpIndex++) {
      mpGui += llList2String(prmButtons, mpIndex);
      if (llGetListLength(mpGui) == prmRows * 3) {
        return mpGui;
    }
    }

    return mpGui;
}

simple_attached_request(string prmFunction,string prmValue){
  string request = "";
  (request = llJsonSetValue(request,["function"],prmFunction));
  (request = llJsonSetValue(request,["value"],prmValue));
  llRegionSayTo(llGetOwner(),CHANNEL_ATTACHMENT,request);
}
#endif // GUI_TOOLS_LSL
