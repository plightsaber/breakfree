#ifndef COLOR_DICTIONARY_LSL
#define COLOR_DICTIONARY_LSL
vector COLOR_WHITE = <0.8, 0.8, 0.8>;
vector COLOR_PUREWHITE = <1.0, 1.0, 1.0>;
vector COLOR_LBROWN = <0.725, 0.580, 0.455>;
vector COLOR_BROWN = <0.495, 0.372, 0.298>;
vector COLOR_SILVER = <0.5,0.5,0.5>;
vector COLOR_BLACK = <0.1, 0.1, 0.1>;
vector COLOR_BLUE = <0.0, 0.25, 0.5>;
vector COLOR_GREEN = <0.0, 0.4, 0.2>;
vector COLOR_RED = <0.75, 0.0, 0.0>;
vector COLOR_PINK = <1.0, 0.5, 0.5>;
vector COLOR_YELLOW = <0.88, 0.68, 0.15>;
vector COLOR_PURPLE = <0.5, 0.0, 0.5>;

list _colors =  ["White", "Pure White", "Light Brown", "Brown", "Silver", "Black", "Blue", "Green", "Red", "Pink", "Yellow", "Purple"];
list _colorVals = [COLOR_WHITE, COLOR_PUREWHITE, COLOR_LBROWN, COLOR_BROWN, COLOR_SILVER, COLOR_BLACK, COLOR_BLUE, COLOR_GREEN, COLOR_RED, COLOR_PINK, COLOR_YELLOW, COLOR_PURPLE];
#endif // COLOR_DICTIONARY_LSL
