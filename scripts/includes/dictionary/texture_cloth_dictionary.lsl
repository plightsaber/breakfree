#ifndef TEXTURE_CLOTH_DICTIONARY_LSL
#define TEXTURE_CLOTH_DICTIONARY_LSL
string TEXTURE_BANDANA = "texBandana";
string TEXTURE_LINEN = "texLinen";

list _textures = ["Smooth", "Linen", "Bandana"];
list _textureVals = [TEXTURE_BLANK, TEXTURE_LINEN, TEXTURE_BANDANA];
#endif // TEXTURE_CLOTH_DICTIONARY_LSL
