#ifndef TEXTURE_ROPE_DICTIONARY_LSL
#define TEXTURE_ROPE_DICTIONARY_LSL
string TEXTURE_ROPE_BRAID = "texRopeBraid";
string TEXTURE_ROPE_TWIST = "texRopeTwist";

list _textures = ["Smooth", "Braid", "Twist"];
list _textureVals = [TEXTURE_BLANK, TEXTURE_ROPE_BRAID, TEXTURE_ROPE_TWIST];
#endif // TEXTURE_ROPE_DICTIONARY_LSL
