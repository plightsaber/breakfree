#ifndef TEXTURE_TAPE_DICTIONARY_LSL
#define TEXTURE_TAPE_DICTIONARY_LSL
string TEXTURE_DUCT = "texDuct";

list _textures = ["Smooth", "Duct"];
list _textureVals = [TEXTURE_BLANK, TEXTURE_DUCT];
#endif // TEXTURE_TAPE_DICTIONARY_LSL
