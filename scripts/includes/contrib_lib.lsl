#ifndef CONTRIB_LIB_LSL
#define CONTRIB_LIB_LSL
//Return elements in X list that are not in Y list
list ListXnotY(list lx, list ly) {
  list lz = [];
  integer i = llGetListLength(lx);
  while(i--)
  if ( !~llListFindList(ly,llList2List(lx,i,i)) )
      lz += llList2List(lx,i,i);
  return lz;
}

// Return source string to Title Case
string ToTitle(string src){
  list words = llParseString2List(llToLower(src),[],[".",";","?","!","\""," ","\n"]);
  integer ll = llGetListLength(words);
  integer lc = (-1);
  string word = "";
  while (((++lc) < ll)) {
    string cap = llToUpper(llGetSubString((word = llList2String(words,lc)),0,0));
    (words = llListReplaceList(words,[(cap + llDeleteSubString(word,0,0))],lc,lc));
  }
  return llDumpList2String(words,"");
}
#endif // CONTRIB_LIB_LSL
