
#ifndef RESTRAINT_TOOLS_LSL
#define RESTRAINT_TOOLS_LSL
#include "includes/general_tools.lsl"

integer _slot; // Current slot of restraint.
string _restraints;

list get_restraint_list(string prmRestraint, string prmList) {
  list restraintList = [];
  list slots = llJson2List(prmRestraint);

  integer index = 0;
  for (index = 0; index < llGetListLength(slots); ++index) {
    string strList = llJsonGetValue(llList2String(slots, index), [prmList]);
    if (JSON_INVALID != strList) {
      list liValues = llJson2List(strList);
      restraintList += liValues;
    }
  }
  return restraintList;
}

integer is_arm_bound() {
  return is_set(llJsonGetValue(_restraints, ["armBound"]))
    || is_set(llJsonGetValue(_restraints, ["armBoundExternal"]));
}

integer is_leg_bound() {
  return is_set(llJsonGetValue(_restraints, ["legBound"]));
}

integer is_gagged() {
  return is_set(llJsonGetValue(_restraints, ["gagged"]));
}

integer is_other_bound() {
  return is_set(llJsonGetValue(_restraints, ["slots", "crotch"]))
    || is_set(llJsonGetValue(_restraints, ["slots", "blindfold"]))
    || is_set(llJsonGetValue(_restraints, ["slots", "hand"]));
}

integer is_bound() {
  return is_arm_bound() || is_leg_bound() || is_gagged() || is_other_bound();
}

integer search_restraint(string prmType, string prmParameter, string prmValue) {
  list searchSlots = get_search_slots(prmType);

  integer index;
  for (index = 0; index < llGetListLength(searchSlots); ++index) {
    if (llJsonGetValue(_restraints, [llList2String(searchSlots, index), prmParameter]) == prmValue) {
      return TRUE;
    }
  }

  return FALSE;
}

string get_top_restraint(string prmType) {
  if (prmType == "arm") {
    if (is_set(llJsonGetValue(_restraints, ["torso"]))) { return llJsonGetValue(_restraints, ["torso"]); }
    else if (is_set(llJsonGetValue(_restraints, ["elbow"]))) { return llJsonGetValue(_restraints, ["elbow"]); }
    else if (is_set(llJsonGetValue(_restraints, ["wrist"]))) { return llJsonGetValue(_restraints, ["wrist"]); }
  } else if (prmType == "leg") {
    if (is_set(llJsonGetValue(_restraints, ["immobilizer"]))) { return llJsonGetValue(_restraints, ["immobilizer"]); }
    else if (is_set(llJsonGetValue(_restraints, ["knee"]))) { return llJsonGetValue(_restraints, ["knee"]); }
    else if (is_set(llJsonGetValue(_restraints, ["ankle"]))) { return llJsonGetValue(_restraints, ["ankle"]); }
  } else if (prmType == "gag") {
    if (is_set(llJsonGetValue(_restraints, ["gag4"]))) { return llJsonGetValue(_restraints, ["gag4"]); }
    else if (is_set(llJsonGetValue(_restraints, ["gag3"]))) { return llJsonGetValue(_restraints, ["gag3"]); }
    else if (is_set(llJsonGetValue(_restraints, ["gag2"]))) { return llJsonGetValue(_restraints, ["gag2"]); }
    else if (is_set(llJsonGetValue(_restraints, ["gag1"]))) { return llJsonGetValue(_restraints, ["gag1"]); }
  } else if (is_set(llJsonGetValue(_restraints, [prmType]))) {
    return llJsonGetValue(_restraints, [prmType]); // Return special slot
  }

  return JSON_NULL;
}

list get_search_slots(string prmType) {
  if (prmType == "arm") {
    return ["wrist", "elbow", "torso", "armExternal"];
  } else if (prmType == "leg") {
    return ["ankle", "knee", "immobilizer"];
  } else if (prmType == "gag") {
    return ["gag1", "gag2", "gag3", "gag4"];
  }

  return [prmType]; // Assume special slot - return self
}
#endif // RESTRAINT_TOOLS_LSL
