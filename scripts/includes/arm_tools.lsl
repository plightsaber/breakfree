#ifndef ARM_TOOLS_LSL
#define ARM_TOOLS_LSL
#include "includes/restraint_tools.lsl"
string _self;
string _resumeFunction;

set_restraints(string prmJson) {
  _slot = 0;
  _restraints = prmJson;
  string armRestraints = llJsonGetValue(prmJson, ["arm"]);
  if (JSON_NULL == armRestraints) return;

  list liArmRestraints = llJson2List(armRestraints);
  string armRestraint = llList2String(liArmRestraints, -1);

  _slot = (integer)llJsonGetValue(armRestraint, ["slot"]);
}
#endif // ARM_TOOLS_LSL
