#ifndef BLINDFOLD_TOOLS_LSL
#define BLINDFOLD_TOOLS_LSL
#include "includes/restraint_tools.lsl"
string _self;
string _currentRestraints;
string _resumeFunction;

string get_current_restraints() {
  if (_currentRestraints) {
    return _currentRestraints;
  }

  _currentRestraints = llJsonSetValue(_currentRestraints, ["blindfold"], JSON_NULL);
  return _currentRestraints;
}

set_restraints(string prmJson) {
  _slot = 0;
  _restraints = prmJson;
  string blindfolds = llJsonGetValue(prmJson, ["blindfold"]);
  if (JSON_NULL == blindfolds) return;

  list liBlindfolds = llJson2List(blindfolds);
  string armRestraint = llList2String(liBlindfolds, -1);

  _slot = (integer)llJsonGetValue(blindfolds, ["slot"]);
}
#endif // BLINDFOLD_TOOLS_LSL
