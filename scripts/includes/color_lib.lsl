#ifndef COLOR_LIB_LSL
#define COLOR_LIB_LSL
#include "includes/dictionary/color_dictionary.lsl"
string _currentColors;

set_color(vector prmColor, string prmComponent) {
  _currentColors = llJsonSetValue(_currentColors, [prmComponent], (string)prmColor);

  string request = "";
  request = llJsonSetValue(request, ["color"], (string)prmColor);
  request = llJsonSetValue(request, ["attachment"], llJsonGetValue(get_self(), ["part"]));
  request = llJsonSetValue(request, ["component"], prmComponent);
  request = llJsonSetValue(request, ["userKey"], (string)llGetOwner());

  simple_attached_request("set_color", request);
  simple_request("set_color", request);
}

set_color_by_name(string prmColorName, string prmComponent) {
  integer colorIndex = llListFindList(_colors, [prmColorName]);
  set_color(llList2Vector(_colorVals, colorIndex), prmComponent);
}
#endif // COLOR_LIB_LSL
