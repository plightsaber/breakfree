#ifndef LEG_TOOLS_LSL
#define LEG_TOOLS_LSL
#include "includes/restraint_tools.lsl"
string _self;
string _resumeFunction;

set_restraints(string prmJson) {
  _slot = 0;
  _restraints = prmJson;
  string legRestraints = llJsonGetValue(prmJson, ["leg"]);
  if (JSON_NULL == legRestraints) return;

  list liLegRestraints = llJson2List(legRestraints);
  string legRestraint = llList2String(liLegRestraints, -1);

  _slot = (integer)llJsonGetValue(legRestraint, ["slot"]);
}
#endif // LEG_TOOLS_LSL
