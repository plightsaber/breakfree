#ifndef GENERAL_TOOLS_LSL
#define GENERAL_TOOLS_LSL
integer CHANNEL_API = -9999274;
integer DEBUGGING = FALSE;

debug(string prmOutput) {
  if (DEBUGGING) llOwnerSay(prmOutput);
}

api_request(key prmToKey, key prmFromKey, string prmFunction, string prmValue) {
  string request;
  request = llJsonSetValue(request, ["function"], prmFunction);
  request = llJsonSetValue(request, ["fromKey"], prmFromKey);
  request = llJsonSetValue(request, ["toKey"], prmToKey);
  request = llJsonSetValue(request, ["value"], prmValue);
  llRegionSayTo(prmToKey, CHANNEL_API, request);
}

simple_request(string prmFunction, string prmValue) {
  string request = "";
  request = llJsonSetValue(request, ["function"], prmFunction);
  request = llJsonSetValue(request, ["value"], prmValue);
  llMessageLinked(LINK_THIS, 0, request, NULL_KEY);
}

integer is_set(string prmJsonValue) {
  return (prmJsonValue != "" && prmJsonValue != JSON_NULL && prmJsonValue != JSON_INVALID && prmJsonValue != "0" && prmJsonValue != NULL_KEY);
}

integer roll(integer prmDice, integer prmSides) {
  integer result = 0;
  integer i;
  for (i = 0; i < prmDice; i++) {
    result += llCeil(llFrand(prmSides));
  }
  return result;
}
#endif // GENERAL_TOOLS_LSL
