#ifndef TEXTURE_LIB_LSL
#define TEXTURE_LIB_LSL
string _currentTextures;

set_texture(string prmTexture, string prmComponent) {
  _currentTextures = llJsonSetValue(_currentTextures, [prmComponent], prmTexture);

  string request = "";
  request = llJsonSetValue(request, ["attachment"], llJsonGetValue(get_self(), ["part"]));
  request = llJsonSetValue(request, ["component"], prmComponent);
  request = llJsonSetValue(request, ["texture"], prmTexture);
  request = llJsonSetValue(request, ["userKey"], (string)llGetOwner());

  simple_attached_request("set_texture", request);
  simple_request("set_texture", request);
}

set_texture_by_name(string prmTextureName, string prmComponent) {
  integer textureIndex = llListFindList(_textures, [prmTextureName]);
  set_texture(llList2String(_textureVals, textureIndex), prmComponent);
}
#endif // TEXTURE_LIB_LSL
