#ifndef GAG_TOOLS_LSL
#define GAG_TOOLS_LSL
string _self;
string _resumeFunction;

string _currentRestraints;
integer _mouthOpen;

string get_current_restraints() {
  if (_currentRestraints) {
    return _currentRestraints;
  }

  _currentRestraints = llJsonSetValue(_currentRestraints, ["gag1"], JSON_NULL);
  _currentRestraints = llJsonSetValue(_currentRestraints, ["gag2"], JSON_NULL);
  _currentRestraints = llJsonSetValue(_currentRestraints, ["gag3"], JSON_NULL);
  _currentRestraints = llJsonSetValue(_currentRestraints, ["gag4"], JSON_NULL);
  return _currentRestraints;
}
#endif // GAG_TOOLS_LSL
