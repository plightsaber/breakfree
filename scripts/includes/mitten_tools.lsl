#ifndef MITTEN_TOOLS_LSL
#define MITTEN_TOOLS_LSL
#include "includes/restraint_tools.lsl"
string _self;
string _currentRestraints;
string _resumeFunction;

string get_current_restraints() {
  if (_currentRestraints) {
    return _currentRestraints;
  }

  _currentRestraints = llJsonSetValue(_currentRestraints, ["hand"], JSON_NULL);
  return _currentRestraints;
}

set_restraints(string prmJson) {
  _slot = 0;
  _restraints = prmJson;
  string mittens = llJsonGetValue(prmJson, ["hand"]);
  if (JSON_NULL == mittens) return;

  list liMittens = llJson2List(mittens);
  string armRestraint = llList2String(liMittens, -1);

  _slot = (integer)llJsonGetValue(mittens, ["slot"]);
}
#endif // MITTEN_TOOLS_LSL
