#ifndef ESCAPE_TEMPLATE_LSL
#define ESCAPE_TEMPLATE_LSL
#include "includes/general_tools.lsl"
#include "includes/restraint_tools.lsl"

string _progress;
string _puzzles;

get_escape_data(string prmRestraint) {
  if (prmRestraint != RESTRAINT_TYPE) {
    return;
  }

  // Always reset current tightness progress on request
  _progress = llJsonSetValue(_progress, ["tightness", "progress"], "0");

  string request;
  request = llJsonSetValue(request, ["progress"], _progress);
  request = llJsonSetValue(request, ["puzzles"], _puzzles);
  simple_request("set_active_escape_data", request);
}

set_restraints(string prmRestraints) {
  list slots = get_search_slots(RESTRAINT_TYPE);
  integer index;

  for (index = 0; index < llGetListLength(slots); ++index) {
    string slot = llList2String(slots, index);
    if (llJsonGetValue(_restraints, ["slots", slot]) != llJsonGetValue(prmRestraints, ["slots", slot])) {
      // Refresh puzzles and progress on restraint change
      _progress = JSON_NULL;
      _puzzles = JSON_NULL;
      index = llGetListLength(slots); // Break the loop
    }
  }

  _restraints = prmRestraints;
}

execute_function(string prmFunction, string prmJson) {
  string value = llJsonGetValue(prmJson, ["value"]);

  if ("get_escape_data" == prmFunction) {
    get_escape_data(value);
  } else if ("set_progress" == prmFunction) {
    if (llJsonGetValue(value, ["restraint"]) != RESTRAINT_TYPE) {
      return;
    }
    _progress = llJsonGetValue(value, ["progress"]);
  } else if ("set_puzzles" == prmFunction) {
    if (llJsonGetValue(value, ["restraint"]) != RESTRAINT_TYPE) {
      return;
    }
    _puzzles = llJsonGetValue(value, ["puzzles"]);
  } else if ("set_restraints" == prmFunction) {
    set_restraints(value);
    return;
  }
}

default
{
  link_message(integer sender_num, integer num, string str, key id) {
    string function;
    string value;

    if ((function = llJsonGetValue(str, ["function"])) == JSON_INVALID) {
      debug(str);
      return;
    }
    execute_function(function, str);
  }
}
#endif // ESCAPE_TEMPLATE_LSL
