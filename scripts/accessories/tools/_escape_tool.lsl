#include "includes/general_tools.lsl"
#include "includes/gui_tools.lsl"
#include "includes/user_lib.lsl"

// ===== Constants and Variables ===== (Thanks Bioshock <_<)
string TOOL_TYPE = "blade"; // blade | cropper | pick
integer TOUCH_MAX_DISTANCE = 1;
float TOUCH_TIMEOUT = 3.0;

integer _apiListener;

key _configQueryKey;
key _toucherKey;
string _guiUser;

list _boundUserKeys;
list _boundUserNames;

string _currentState = "default";

init() {
  _configQueryKey = llGetNotecardLine(".config", 0); // Load config.
}

init_touch(key userKey) {
  if (_apiListener) { llListenRemove(_apiListener); }
  _apiListener = llListen(CHANNEL_API, "", NULL_KEY, "");

  _toucherKey = userKey;

  _boundUserKeys = [];
  _boundUserNames = [];

  // Request User Object
  string request = "";
  request = llJsonSetValue(request, ["function"], "get_touch_info");
  request = llJsonSetValue(request, ["fromKey"], llGetKey());
  request = llJsonSetValue(request, ["toKey"], _toucherKey);

  _currentState = "get_touch_info";
  llRegionSayTo(_toucherKey, CHANNEL_API, request);
  llSetTimerEvent(TOUCH_TIMEOUT);
}

init_gui(key prmID) {
  _guiUserKey = prmID;

  if (_guiListener) { llListenRemove(_guiListener); }
  _guiChannel = (integer)llFrand(-9998) - 1;
  _guiListener = llListen(_guiChannel, "", _guiUserKey, "");
  gui();
}

gui() {
  _currentState = "GUI";

  // Reset Busy Clock
  llSetTimerEvent(GUI_TIMEOUT);

  list mpButtons = ["Remove", "<<Done>>", "Refresh"];
  _guiText = "What do you want to do with this?";
  _guiButtons = multipage_gui(mpButtons + _boundUserNames, 4, _guiPage);
  llDialog(_guiUserKey, _guiText, _guiButtons, _guiChannel);
}

ping_bound() {
  _currentState = "ping_bound";
  string request;
  request = llJsonSetValue(request, ["function"], "ping_bound");
  request = llJsonSetValue(request, ["key"], llGetKey());

  llWhisper(CHANNEL_API, request);
  llSetTimerEvent(TOUCH_TIMEOUT);
}


send_touch(key userKey) {
  _toucherKey = NULL_KEY;
  string value = _guiUser;
  value = llJsonSetValue(value, [TOOL_TYPE], "1");

  string request;
  request = llJsonSetValue(request, ["function"], "touch_user");
  request = llJsonSetValue(request, ["fromKey"], llJsonGetValue(value, ["key"]));
  request = llJsonSetValue(request, ["toKey"], userKey);
  request = llJsonSetValue(request, ["value"], value);
  llRegionSayTo(userKey, CHANNEL_API, request);

  _currentState = "default";
  llSetTimerEvent(0.0);
}

// ===== Events =====
default
{
  state_entry() {
    init();
  }

  on_rez(integer start_param) {
    init();
  }

  dataserver(key queryID, string configData) {
    if (queryID == _configQueryKey) {
      TOOL_TYPE = llJsonGetValue(configData, ["type"]);
    }
  }

  listen(integer channel, string name, key id, string message) {
    if ("GUI" == _currentState && channel == _guiChannel) {
      if (message == "Next >>") { _guiPage ++; gui(); return; }
      else if (message == "<< Previous") { _guiPage --; gui(); return; }
      else if (message == " ") { gui(); return; }
      else if (message == "<<Done>>") {
        _currentState = "default";
        llSetTimerEvent(0.0);
        return;
      }
      else if (message == "Refresh") {
        init_touch(_guiUserKey);
        return;
      }
      else if (message == "Remove") {
        llDie();

        // If that didn't work - we must be an attachment
        llRequestPermissions(llGetOwner(), PERMISSION_ATTACH );
        llDetachFromAvatar();
        return;
      }

      integer index = llListFindList(_boundUserNames, [message]);
      send_touch(llList2Key(_boundUserKeys, index));
    } else if ("ping_bound" == _currentState) {
      string function = llJsonGetValue(message, ["function"]);
      _boundUserKeys += llJsonGetValue(message, ["userKey"]);
      _boundUserNames += llGetDisplayName(llJsonGetValue(message, ["userKey"]));
      return;
    } else if ("get_touch_info" == _currentState) {
      _guiUser = llJsonGetValue(message, ["value"]);
      ping_bound();
      return;
    }
  }

  timer() {
    if ("get_touch_info" == _currentState) {
      _guiUser = get_default_user(_toucherKey);
      ping_bound();
      return;
    } else if ("ping_bound" == _currentState) {
      init_gui(_toucherKey);
      return;
    } else if ("GUI" == _currentState) {
      _currentState = "default";
      exit("");
      return;
    }

    // We shouldn't get here - but clean up just in case.
    debug("WARNING! timer entered in invalid state: " + _currentState);
    llSetTimerEvent(0.0);
    _currentState = "default";
  }

  touch_start(integer num_detected) {
    key toucherKey = llDetectedKey(0);

    if (_currentState != "default") {
      if (toucherKey == _guiUserKey) {
        init_gui(toucherKey);
        return;
      }
      llRegionSayTo(toucherKey, 0, "This item is currently being handled by " + llGetDisplayName(_guiUserKey) + ".");
      return;
    }

    init_touch(toucherKey);
  }
}
